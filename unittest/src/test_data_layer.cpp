#include "../framework/unittest.hh"
#include "util/data_layer.hpp"
#include "util/os_util.hh"

FIXTURE_BEGIN(test_data_layer)

// Machine generated code

#include "util/data_layer.hpp"
#include <cstdint>
#include <string>

struct MyTable {
  MyTable() {
    obj_ptr_ = kratos::util::DbQuery::create_from_string("MyTable_JSON_CONF", get_config(), "", false);
  }
  template <typename T>
  auto save(std::shared_ptr<T> prx) -> void {
    obj_ptr_->save(prx);
  }
  template <typename T>
  auto overwrite(std::shared_ptr<T> prx) -> void {
    obj_ptr_->overwrite(prx);
  }
  template <typename T>
  auto remove(std::shared_ptr<T> prx) -> void {
    obj_ptr_->remove(prx);
  }
  auto dispose() -> void {
    obj_ptr_->dispose();
  }
  auto is_dirty() -> bool {
    return obj_ptr_->is_dirty();
  }
  static auto create(bool read_only = false) -> std::shared_ptr<MyTable> {
    auto* ptr = new MyTable(read_only, false);
    return std::shared_ptr<MyTable>(ptr);
  }
  static auto create_new(const std::int32_t& index)  -> std::shared_ptr<MyTable> {
    auto* ptr = new MyTable(false, true);
    ptr->mut().set(0, index);
    return std::shared_ptr<MyTable>(ptr);
  }
  template <typename T>
  static auto load(std::shared_ptr<T> prx, const std::int32_t& index)  -> std::shared_ptr<MyTable> {
    auto* ptr = new MyTable(false, true);
    ptr->mut().set(0, index);
    ptr->overwrite(prx);
    return std::shared_ptr<MyTable>(ptr);
  }
  static auto index_name() -> std::string {
    return "MyTable";
  }
  static auto index_type() -> std::string {
    return "";
  }
  static auto expiration() -> std::uint32_t {
	return 0;
  }
  auto read_only() -> void {
    obj_ptr_->set_read_only(true);
  }
  auto mut() -> kratos::util::DbObject& { return *obj_ptr_; }
  void set_i32_value(const std::int32_t& v) { obj_ptr_->set(0, v); }
  const std::int32_t& get_i32_value() { return obj_ptr_->get<std::int32_t>(0); }
  void set_s_value(const std::string& v) { obj_ptr_->set(1, v); }
  const std::string& get_s_value() { return obj_ptr_->get<std::string>(1); }
private:
  friend class kratos::util::DbQuery;
  MyTable(bool read_only, bool new_obj) {
    obj_ptr_ = kratos::util::DbQuery::create_from_string("MyTable_JSON_CONF", get_config(), "", read_only);
    obj_ptr_->set_new_object(new_obj);
  }
  MyTable(const std::string& index, bool read_only, bool new_obj) {
    obj_ptr_ = kratos::util::DbQuery::create_from_string("MyTable_JSON_CONF", get_config(), index, read_only);
    obj_ptr_->set_new_object(new_obj);
  }
  auto get_obj_ptr() -> kratos::util::DbObjectPtr { return obj_ptr_; }
  static auto get_type_name() -> const std::string& {
    static std::string type_name = "MyTable";
    return type_name;
  }
  kratos::util::DbObjectPtr obj_ptr_;
  static auto get_config() -> const std::string& {
    static std::string JSON_STR = R"({
            "type": "MyTable",
            "values" : [
            {
                "name" : "i32_value",
                "is_obj_id" : true,
                "type" : "int",
                "default" : "1"
            },
            {
                "name" : "s_value",
                "type" : "string",
                "default" : "hello world"
            }
            ]
        })";
    return JSON_STR;
  }
};

CASE(TestLoad1) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      const char *data =
          R"({
          "values" : [
              {
                  "name" : "field_a",
                  "value" : "abc"
              }
          ]
      })";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };

  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  auto obj_ptr = query.load(service, "config.json", "1");
  ASSERT_TRUE(obj_ptr->get<std::string>("field_a") == "abc");
  ASSERT_TRUE(obj_ptr->get<std::string>(0) == "abc");
  obj_ptr->set("field_a", std::string("efg"));
  ASSERT_TRUE(obj_ptr->is_dirty());
  ASSERT_TRUE(obj_ptr->get<std::string>(0) == "efg");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestLoad2) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      // 没有字段名
      const char *data =
          R"({
          "values" : [
              {
                  "value" : "abc"
              }
          ]
      })";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };

  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  auto obj_ptr = query.load(service, "config.json", "1");
  ASSERT_TRUE(obj_ptr->get<std::string>("field_a") == "abc");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestLoad3) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      const char *data =
          R"(
        [
          {
          "values" : [
              {
                  "name" : "field_a",
                  "value" : "abc"
              }
          ]
         }
       ]
      )";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };

  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  auto obj_vec = query.filter(service, "config.json");
  ASSERT_TRUE(obj_vec.size() == 1);
  ASSERT_TRUE(obj_vec[0]->get<std::string>("field_a") == "abc");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestLoad4) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      const char *data =
          R"({
          "values" : [
              {
                  "value" : "100"
              }
          ]
      })";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 1; }
  };
  struct MyDbObject {
    void set_i(int i32) { obj_ptr_->set(0, i32); }
    int get_i() { return obj_ptr_->get<int>(0); }

    MyDbObject() {
      obj_ptr_ = kratos::util::DbQuery::create_from_string(
          "MyDbObject_JSON_CONF", get_config());
    }

    auto get_obj_ptr() -> kratos::util::DbObjectPtr { return obj_ptr_; }

    static auto get_config() -> const std::string & {
      static std::string JSON_STR = R"(
      {
       "type": "MyDbObject",
       "values" : [
           {
               "name" : "i",
               "type" : "int",
               "default" : "0",
               "is_obj_id" : true
           }
       ]
      }
      )";
      return JSON_STR;
    }

    static auto get_type_name() -> const std::string & {
      static std::string type_name = "MyDbObject";
      return type_name;
    }

  private:
    kratos::util::DbObjectPtr obj_ptr_;
  };
  auto service = std::make_shared<FakeDataService>();
  auto obj_ptr = kratos::util::DbQuery::load<MyDbObject>(service, "1");
  ASSERT_TRUE(obj_ptr->get_i() == 100);
}

CASE(TestOverwrite1) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool create(const std::string &) { return true; }
    bool save(const std::string &) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      const char *data =
          R"({
          "values" : [
              {
                  "name" : "field_a",
                  "value" : "abc"
              }
          ]
      })";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };

  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  auto obj_ptr = query.create("config.json");
  obj_ptr->overwrite(service);
  ASSERT_TRUE(obj_ptr->get<std::string>("field_a") == "abc");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestCreate1) {
  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto obj_ptr = query.create("config.json");
  // 默认值
  ASSERT_TRUE(obj_ptr->get<std::string>("field_a") == "hello");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestCreate2) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(query.create(service, "config.json"));
  kratos::util::get_db_manager().cleanup();
}

CASE(TestCreate3) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 1; }
  };
  struct MyDbObject {
    void set_i(int i32) { obj_ptr_->set(0, i32); }
    int get_i() { return obj_ptr_->get<int>(0); }

    MyDbObject() {
      obj_ptr_ = kratos::util::DbQuery::create_from_string(
          "MyDbObject_JSON_CONF", get_config());
    }

    auto get_obj_ptr() -> kratos::util::DbObjectPtr { return obj_ptr_; }

    static auto get_config() -> const std::string & {
      static std::string JSON_STR = R"(
      {
       "type": "MyDbObject",
       "values" : [
           {
               "name" : "i",
               "type" : "int",
               "default" : "0",
               "is_obj_id" : true
           }
       ],
       "expiration" : 0
      }
      )";
      return JSON_STR;
    }

    static auto get_type_name() -> const std::string & {
      static std::string type_name = "MyDbObject";
      return type_name;
    }

  private:
    kratos::util::DbObjectPtr obj_ptr_;
  };
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(kratos::util::DbQuery::create<MyDbObject>(service));
}

CASE(TestCreateObject1) {
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "unknown",
             "default" : "hello",
             "is_obj_id" : true
         }
     ],
     "expiration" : 0
  }
  )";
  kratos::util::DbQuery query;
  ASSERT_EXCEPT(query.create_from_string("wrong_config.json", config_str));
  kratos::util::get_db_manager().cleanup();
}

CASE(TestCreateObject2) {
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         },
         {
             "name" : "field_b",
             "type" : "int",
             "default" : 1
         },
         {
             "name" : "field_c",
             "type" : "uint",
             "default" : 1
         },
         {
             "name" : "field_d",
             "type" : "int64",
             "default" : 1
         },
         {
             "name" : "field_e",
             "type" : "uint64",
             "default" : 1
         },
         {
             "name" : "field_f",
             "type" : "float",
             "default" : 1.0
         },
         {
             "name" : "field_g",
             "type" : "double",
             "default" : 1.0
         }
     ]
  }
  )";
  kratos::util::DbQuery query;
  auto obj_ptr = query.create_from_string("test_config", config_str);
  ASSERT_TRUE(obj_ptr);
  ASSERT_TRUE(obj_ptr->get_field_size() == 7);
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  auto service = std::make_shared<FakeDataService>();
  obj_ptr->save_all(service);
  kratos::util::get_db_manager().cleanup();
}

CASE(TestDispose1) {
  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  {
    auto obj_ptr = query.create("config.json");
    obj_ptr->dispose();
  }
  kratos::util::get_db_manager().cleanup();
}

CASE(TestSave1) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool saved{false};
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) {
      saved = !str.empty();
      return true;
    }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto obj_ptr = query.create("config.json");
  ASSERT_TRUE(!obj_ptr->is_dirty());
  obj_ptr->set(0, std::string("123"));
  ASSERT_TRUE(obj_ptr->is_dirty());
  auto service = std::make_shared<FakeDataService>();
  obj_ptr->save(service);
  ASSERT_TRUE(!obj_ptr->is_dirty());
  ASSERT_TRUE(service->saved);
  kratos::util::get_db_manager().cleanup();
}

CASE(TestFilter1) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &s) {
      std::cout << s << std::endl;
      const char *data =
          R"([
          {
            "values" : [
                {
                    "name" : "field_a",
                    "value" : "abc"
                }
            ]
          }
          ])";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  using namespace kratos::util;
  DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(
      !query.filter(service, "config.json", "field_a != 'world'").empty());
  kratos::util::get_db_manager().cleanup();
}

CASE(TestFilter2) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &s) {
      std::cout << s << std::endl;
      const char *data =
          R"([
          {
            "index" : "1",
            "values" : [
                {
                    "name" : "field_a",
                    "value" : "abc",
                    xxx
                }
            ]
          }
          ])";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  using namespace kratos::util;
  DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(
      query.filter(service, "config.json", "field_a != 'world'").empty());
  query.filter(service, "config.json", "field_a != 'world'");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestFilter3) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &s) {
      std::cout << s << std::endl;
      const char *data =
          R"([
          {
            "values" : [
                {
                    "name" : "field_a",
                    "value" : "abc"
                }
            ]
          }
          ])";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello"
             xxx
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("wrong_config1.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  using namespace kratos::util;
  DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(query.filter(service, "wrong_config1.json", "field_a != 'world'")
                  .empty());
  kratos::util::get_db_manager().cleanup();
}

CASE(TestFilter4) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      const char *data =
          R"(
        [
          {
          "values" : [
              {
                  "name" : "i",
                  "value" : "100"
              }
          ]
         }
       ]
      )";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 1; }
  };
  struct MyDbObject {
    void set_i(int i32) { obj_ptr_->set(0, i32); }
    int get_i() { return obj_ptr_->get<int>(0); }

    MyDbObject() {
      obj_ptr_ = kratos::util::DbQuery::create_from_string(
          "MyDbObject_JSON_CONF", get_config());
    }

    auto get_obj_ptr() -> kratos::util::DbObjectPtr { return obj_ptr_; }

    static auto get_config() -> const std::string & {
      static std::string JSON_STR = R"(
      {
       "type": "MyDbObject",
       "values" : [
           {
               "name" : "i",
               "type" : "int",
               "default" : "0",
               "is_obj_id" : true
           }
       ]
      }
      )";
      return JSON_STR;
    }

    static auto get_type_name() -> const std::string & {
      static std::string type_name = "MyDbObject";
      return type_name;
    }

  private:
    kratos::util::DbObjectPtr obj_ptr_;
  };
  auto service = std::make_shared<FakeDataService>();
  auto vec = kratos::util::DbQuery::filter<MyDbObject>(service);
  ASSERT_TRUE(vec[0]->get_i() == 100);
  kratos::util::get_db_manager().cleanup();
}

CASE(TestFilter5) {
  struct FakeDataService {
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &s) {
      std::cout << s << std::endl;
      const char *data =
          R"([
          {
            "values" : [
                {
                    "name" : "field_a",
                    "value" : "abc",
                    xxx
                }
            ]
          }
          ])";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "cmd" : "create",
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  using namespace kratos::util;
  DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(
      query.filter(service, "config.json", "field_a != 'world'").empty());
  query.filter(service, "config.json", "field_a != 'world'");
  kratos::util::get_db_manager().cleanup();
}

CASE(TestSaveAll1) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool saved{false};
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) {
      saved = !str.empty();
      return true;
    }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto obj_ptr = query.create("config.json");
  ASSERT_TRUE(!obj_ptr->is_dirty());
  obj_ptr->set(0, std::string("123"));
  ASSERT_TRUE(obj_ptr->is_dirty());
  auto service = std::make_shared<FakeDataService>();
  obj_ptr->save_all(service);
  ASSERT_TRUE(!obj_ptr->is_dirty());
  ASSERT_TRUE(service->saved);
  kratos::util::get_db_manager().cleanup();
}

CASE(TestLoadMulti1) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool saved{false};
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) {
      saved = !str.empty();
      return true;
    }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 0; }
  };
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  auto ret_vec = query.load<MyTable>(service, {"1", "2", "3"});
  ASSERT_TRUE(ret_vec.empty());
  kratos::util::get_db_manager().cleanup();
}

CASE(TestRemove1) {
  struct FakeDataService {
    bool saved{false};
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) {
      saved = !str.empty();
      return true;
    }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 1; }
  };
  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(1 == query.remove(service, "config.json", "field_a != 'world'"));
  kratos::util::get_db_manager().cleanup();
}

CASE(TestRemove2) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      return std::make_shared<std::string>();
    }
    std::size_t remove(const std::string &) { return 1; }
  };
  const char *config_str = R"(
  {
     "type": "Test",
     "values" : [
         {
             "name" : "field_a",
             "type" : "string",
             "default" : "hello",
             "is_obj_id" : true
         }
     ]
  }
  )";
  std::ofstream ofs;
  ofs.open("config.json", std::ios::trunc | std::ios::out);
  ofs << config_str;
  ofs.close();
  kratos::util::DbQuery query;
  auto service = std::make_shared<FakeDataService>();
  auto obj_ptr = query.create("config.json");
  ASSERT_TRUE(obj_ptr->remove(service) != 0);
  kratos::util::get_db_manager().cleanup();
}

CASE(TestRemove3) {
  struct FakeDataService {
    rpc::ProxyID getID() { return 1; }
    bool create(const std::string &) { return true; }
    bool save(const std::string &str) { return true; }
    std::shared_ptr<std::string> load(const std::string &) {
      const char *data =
          R"({
          "index" : "1",
          "values" : [
              {
                  "value" : "100"
              }
          ]
      })";
      return std::make_shared<std::string>(data);
    }
    std::size_t remove(const std::string &) { return 1; }
  };
  struct MyDbObject {
    void set_i(int i32) { obj_ptr_->set(0, i32); }
    int get_i() { return obj_ptr_->get<int>(0); }
    auto mut() -> kratos::util::DbObject & { return *obj_ptr_; }

  private:
    friend class kratos::util::DbQuery;
    MyDbObject() {
      obj_ptr_ = kratos::util::DbQuery::create_from_string(
          "MyDbObject_JSON_CONF", get_config());
    }
    auto get_obj_ptr() -> kratos::util::DbObjectPtr { return obj_ptr_; }

    static auto get_config() -> const std::string & {
      static std::string JSON_STR = R"(
      {
       "type": "MyDbObject",
       "values" : [
           {
               "name" : "i",
               "type" : "int",
               "default" : "0",
               "is_obj_id" : true
           }
       ]
      }
      )";
      return JSON_STR;
    }

    static auto get_type_name() -> const std::string & {
      static std::string type_name = "MyDbObject";
      return type_name;
    }

    kratos::util::DbObjectPtr obj_ptr_;
  };
  auto service = std::make_shared<FakeDataService>();
  ASSERT_TRUE(kratos::util::DbQuery::remove<MyDbObject>(service) == 1);
  kratos::util::get_db_manager().cleanup();
}

CASE(TestObject1) {
  auto obj_ptr = kratos::util::DbQuery::create<MyTable>();
  ASSERT_TRUE(obj_ptr->get_i32_value() == 1);
  ASSERT_TRUE(obj_ptr->mut().get<int>("i32_value") == 1);
}

TEARDOWN([]() {
  kratos::util::remove_file("config.json");
  kratos::util::remove_file("wrong_config.json");
  kratos::util::remove_file("wrong_config1.json");
})

FIXTURE_END(test_data_layer)
