#include "../framework/unittest.hh"
#include "box/service_box.hh"
#include "detail/command_impl.hh"
#include "detail/command_manager.hh"
#include "detail/http_base_impl.hh"
#include "root/coroutine/coroutine.h"
#include "util/os_util.hh"
#include "util/time_util.hh"
#include <fstream>

FIXTURE_BEGIN(test_command_manager)

using namespace kratos;

SETUP([]() {
  util::make_dir("service");
  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << R"(listener = {
            host = ("127.0.0.1:10001")
         }
         box_channel_recv_buffer_len = 1024
         box_name = "test_box"
         connect_other_box_timeout = 2000
         service_finder_connect_timeout = 2000
         logger_config_line = "console://;line=[%y%m%d-%H:%M:%S:%U];flush=true"
         open_coroutine = true
         command = {
           enable = true
           listen = "127.0.0.1:6889"
           root_page_path = ""
         })";
  ofs.close();
});

CASE(TestWaitFor1) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char *argv[] = {"test", "--config=config.cfg"};
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  bool done = false;
  cmd.wait_for("test", 1000, [&](const std::string &req) -> std::string {
    done = true;
    return "done";
  });
  bool done1 = false;
  http.do_request_async("127.0.0.1", 6889, "/", "GET", {},
                        "{\"command\": \"test\"}", 1000, 0,
                        [&](kratos::http::HttpCallPtr call, std::uint64_t) {
                          done1 = call.lock()->get_content() == "done";
                        });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 1000) {
      break;
    }
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(done1);
  http.stop();
  sb.stop();
}

CASE(TestWaitFor2) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char *argv[] = {"test", "--config=config.cfg"};
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager(), false);
  bool done = false;
  cmd.wait_for("test", 1000, [&](const std::string &req) -> std::string {
    done = true;
    throw std::runtime_error("");
  });
  bool done1 = false;
  http.do_request_async("127.0.0.1", 6889, "/", "GET", {},
                        "{\"command\": \"test\"}", 1000, 0,
                        [&](kratos::http::HttpCallPtr call, std::uint64_t) {
                          done1 = call.lock()->get_content() ==
                                  "{\"error\" : \"Internal error\"}";
                        });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 1000) {
      break;
    }
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(done1);
  http.stop();
  sb.stop();
}

CASE(TestWaitFor3) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char* argv[] = { "test", "--config=config.cfg" };
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  bool done = false;
  cmd.wait_for("test", 1000, [&](const std::string &req) -> std::string {
    done = true;
    coro_yield();
    return "";
  });
  bool done1 = false;
  http.do_request_async(
      "127.0.0.1", 6889, "/", "GET", {}, "{\"command\": \"test\"}", 1000, 0,
      [&](kratos::http::HttpCallPtr call, std::uint64_t) { done1 = true; });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 1000) {
      break;
    }
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(!done1);
  http.stop();
  sb.stop();
}

CASE(TestWaitFor4) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char* argv[] = { "test", "--config=config.cfg" };
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  auto result = cmd.wait_for(
      "", 0, [&](const std::string &req) -> std::string { return ""; });
  http.stop();
  ASSERT_FALSE(result);
  sb.stop();
}

CASE(TestWaitFor5) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char* argv[] = { "test", "--config=config.cfg" };
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  bool done = false;
  cmd.wait_for("test", 1000, [&](const std::string &req) -> std::string {
    done = true;
    return "";
  });
  bool done1 = false;
  http.do_request_async(
      "127.0.0.1", 6889, "/", "GET", {}, "{\"command\": sdjkfjsdkfjdkfj}", 1000,
      0, [&](kratos::http::HttpCallPtr call, std::uint64_t) { done1 = true; });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 1000) {
      break;
    }
  }
  ASSERT_TRUE(!done);
  ASSERT_TRUE(done1);
  http.stop();
  sb.stop();
}

CASE(TestWaitFor6) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char* argv[] = { "test", "--config=config.cfg" };
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  bool done1 = false;
  http.do_request_async(
      "127.0.0.1", 6889, "/", "GET", {}, "{\"command\": \"sdjkfjsdkfjdkfj\"}",
      1000, 0,
      [&](kratos::http::HttpCallPtr call, std::uint64_t) { done1 = true; });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 1000) {
      break;
    }
  }
  ASSERT_TRUE(done1);
  http.stop();
  sb.stop();
}

CASE(TestWaitFor7) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char* argv[] = { "test", "--config=config.cfg" };
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  bool done = false;
  cmd.wait_for("test", 1000, [&](const std::string &req) -> std::string {
    done = true;
    coro_yield();
    return "";
  });
  bool done1 = false;
  http.do_request_async(
      "127.0.0.1", 6889, "/", "GET", {}, "{\"command\": \"test\"}", 500, 0,
      [&](kratos::http::HttpCallPtr call, std::uint64_t) { done1 = true; });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 1000) {
      break;
    }
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(!done1);
  http.stop();
  sb.stop();
}

CASE(TestWaitFor8) {
  service::ServiceBox sb;
  sb.enable_logger(false);
  const char* argv[] = { "test", "--config=config.cfg" };
  sb.start(2, argv);
  kratos::http::HttpBaseImpl http(nullptr);
  ASSERT_TRUE(http.start());
  kratos::service::CommandImpl cmd(sb.get_command_manager());
  bool done = false;
  cmd.wait_for("test", 1000, [&](const std::string &req) -> std::string {
    done = true;
    coro_yield();
    return "";
  });
  bool done1 = false;
  http.do_request_async(
      "127.0.0.1", 6889, "/", "GET", {}, "{\"command\": \"test\"}", 200, 0,
      [&](kratos::http::HttpCallPtr call, std::uint64_t) { done1 = true; });
  auto start = util::get_os_time_millionsecond();
  while (true) {
    auto ms = util::get_os_time_millionsecond();
    sb.update(ms);
    http.update(ms);
    if (ms - start > 2000) {
      break;
    }
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(!done1);
  http.stop();
  sb.stop();
}

TEARDOWN([]() {
  util::rm_empty_dir("service");
  kratos::util::remove_file("config.cfg");
});

FIXTURE_END(test_command_manager)
