#include "../framework/unittest.hh"
#include "detail/redis_impl.hh"
#include "load_balancer/load_balance.h"
#include "util/os_util.hh"

FIXTURE_BEGIN(test_load_balancer)

using namespace kratos::loadbalance;

class IPHASHNode : public ILBNode {
public:
  IPHASHNode() = default;
  IPHASHNode(const std::string &n) : name_(n) {}

public:
  virtual auto set_name(const std::string &name) -> void { name_ = name; }
  virtual auto get_name() -> const std::string & { return name_; }
  virtual auto set_weight(std::int32_t weight) -> void { weight_ = weight; }
  virtual auto get_weight() -> std::int32_t { return weight_; }
  virtual auto set_active_count(std::int32_t count) -> void {
    active_count_ = count;
  }
  virtual auto get_active_count() -> std::int32_t { return active_count_; }

private:
  std::string name_{""};
  std::int32_t weight_{0};
  std::int32_t active_count_{0};
};

class LANode : public ILBNode {
public:
  LANode() = default;
  LANode(const std::string &n, std::int32_t w) : name_(n), weight_(w) {}

public:
  virtual auto set_name(const std::string &name) -> void { name_ = name; }
  virtual auto get_name() -> const std::string & { return name_; }
  virtual auto set_weight(std::int32_t weight) -> void { weight_ = weight; }
  virtual auto get_weight() -> std::int32_t { return weight_; }
  virtual auto set_active_count(std::int32_t count) -> void {
    active_count_ = count;
  }
  virtual auto get_active_count() -> std::int32_t { return active_count_; }

private:
  std::string name_{""};
  std::int32_t weight_{0};
  std::int32_t active_count_{0};
};

class RRNode : public ILBNode {
public:
  RRNode() = default;
  RRNode(const std::string &n, std::int32_t w) : name_(n), weight_(w) {}

public:
  virtual auto set_name(const std::string &name) -> void { name_ = name; }
  virtual auto get_name() -> const std::string & { return name_; }
  virtual auto set_weight(std::int32_t weight) -> void { weight_ = weight; }
  virtual auto get_weight() -> std::int32_t { return weight_; }
  virtual auto set_active_count(std::int32_t count) -> void {
    active_count_ = count;
  }
  virtual auto get_active_count() -> std::int32_t { return active_count_; }

private:
  std::string name_{""};
  std::int32_t weight_{0};
  std::int32_t active_count_{0};
};

class SWRRNode : public ILBNode {
public:
  SWRRNode() = default;
  SWRRNode(const std::string &n, std::int32_t w) : name_(n), weight_(w) {}

public:
  virtual auto set_name(const std::string &name) -> void { name_ = name; }
  virtual auto get_name() -> const std::string & { return name_; }
  virtual auto set_weight(std::int32_t weight) -> void { weight_ = weight; }
  virtual auto get_weight() -> std::int32_t { return weight_; }
  virtual auto set_active_count(std::int32_t count) -> void {
    active_count_ = count;
  }
  virtual auto get_active_count() -> std::int32_t { return active_count_; }

private:
  std::string name_{""};
  std::int32_t weight_{0};
  std::int32_t active_count_{0};
};

class VNSWRRNode : public ILBNode {
public:
  VNSWRRNode() = default;
  VNSWRRNode(const std::string &n, std::int32_t w) : name_(n), weight_(w) {}

public:
  virtual auto set_name(const std::string &name) -> void { name_ = name; }
  virtual auto get_name() -> const std::string & { return name_; }
  virtual auto set_weight(std::int32_t weight) -> void { weight_ = weight; }
  virtual auto get_weight() -> std::int32_t { return weight_; }
  virtual auto set_active_count(std::int32_t count) -> void {
    active_count_ = count;
  }
  virtual auto get_active_count() -> std::int32_t { return active_count_; }

private:
  std::string name_{""};
  std::int32_t weight_{0};
  std::int32_t active_count_{0};
};

class WRRNode : public ILBNode {
public:
  WRRNode() = default;
  WRRNode(const std::string &n, std::int32_t w) : name_(n), weight_(w) {}

public:
  virtual auto set_name(const std::string &name) -> void { name_ = name; }
  virtual auto get_name() -> const std::string & { return name_; }
  virtual auto set_weight(std::int32_t weight) -> void { weight_ = weight; }
  virtual auto get_weight() -> std::int32_t { return weight_; }
  virtual auto set_active_count(std::int32_t count) -> void {
    active_count_ = count;
  }
  virtual auto get_active_count() -> std::int32_t { return active_count_; }

private:
  std::string name_{""};
  std::int32_t weight_{0};
  std::int32_t active_count_{0};
};

class Box {
public:
  static auto Instance() -> Box * {
    if (instance_ == nullptr) {
      instance_ = new Box;
    }
    return instance_;
  }

  auto set_balancer(std::unique_ptr<ILoadBalancer> balancer) -> void {
    balancer_ = std::move(balancer);
  }

  void add_some_node(std::int32_t count, BalancerMod mod) {
    for (std::int32_t i = 0; i < count; i++) {
      std::string name = "node" + std::to_string(i);
      std::shared_ptr<ILBNode> node = std::make_shared<IPHASHNode>(name);
      switch (mod) {
      case kratos::loadbalance::BalancerMod::None_Scheduling:
        break;
      case kratos::loadbalance::BalancerMod::Ip_Hash_Scheduling:
        break;
      case kratos::loadbalance::BalancerMod::Least_Active_Scheduling:
        node = std::make_shared<LANode>(
            name, kratos::util::get_random_uint32(0, 100));
        break;
      case kratos::loadbalance::BalancerMod::Round_Robin_Scheduling:
        node = std::make_shared<RRNode>(
            name, kratos::util::get_random_uint32(0, 100));
        break;
      case kratos::loadbalance::BalancerMod::Weighted_Round_Robin_Scheduling:
        node = std::make_shared<WRRNode>(
            name, kratos::util::get_random_uint32(0, 100));
        break;
      case kratos::loadbalance::BalancerMod::
          Smooth_Weighted_Round_Robin_Scheduling:
        node = std::make_shared<SWRRNode>(
            name, kratos::util::get_random_uint32(0, 100));
        break;
      case kratos::loadbalance::BalancerMod::
          Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling:
        node = std::make_shared<VNSWRRNode>(
            name, kratos::util::get_random_uint32(0, 100));
        break;
      default:
        break;
      }
      nodes_[name] = node;
      balancer_->add_lb_node(node);
    }
  }

  auto get_next(const std::string &ip = "") -> LoadBalanceNodeWeakPtr {
    return balancer_->get_next(ip);
  }

  auto print_node() {
    std::cout << std::endl;
    for (auto &it : nodes_) {
      std::cout << "name : " << it.first
                << " wight : " << it.second->get_weight() << std::endl;
    }
  }

  auto clear() {
    balancer_->clear();
    nodes_.clear();
  }

private:
  static Box *instance_;
  std::map<std::string, std::shared_ptr<ILBNode>> nodes_;
  std::unique_ptr<ILoadBalancer> balancer_;
};

Box *Box::instance_ = nullptr;

CASE(iphashavg) {
  Box::Instance()->set_balancer(
      BalancerFactory::create_balancer(BalancerMod::Ip_Hash_Scheduling));
  Box::Instance()->add_some_node(10, BalancerMod::Ip_Hash_Scheduling);
  std::map<std::string, std::int32_t> node_hit_count;
  for (std::int32_t i = 0; i <= 255; i++) {
    for (std::int32_t j = 0; j <= 255; j++) {
      std::string clinet_ip =
          "192.168." + std::to_string(i) + "." + std::to_string(j);
      auto node = Box::Instance()->get_next(clinet_ip);
      if (node.expired()) {
        std::cout << "出大问题！ 节点失效了！" << std::endl;
      }
      auto node_name = node.lock()->get_name();
      node_hit_count[node_name]++;
    }
  }
  std::cout << std::endl;
  for (auto &it : node_hit_count) {
    std::cout << "name : " << it.first << " hit count : " << it.second
              << std::endl;
  }
}

CASE(iphashclear) {
  Box::Instance()->clear();
  auto node = Box::Instance()->get_next("127.0.0.1");
  auto flag = node.expired();
  ASSERT_TRUE(flag);
}

CASE(iphashaddnode1) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Ip_Hash_Scheduling);
  auto node1 = std::make_shared<IPHASHNode>("node1");
  balancer->add_lb_node(node1);
  auto node = balancer->get_next("127.0.0.1");
  ASSERT_FALSE(node.expired());
  ASSERT_EQUAL(node.lock()->get_name(), "node1");
}

CASE(iphashaddnode2) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Ip_Hash_Scheduling);
  auto node1 = std::make_shared<IPHASHNode>("node1");
  balancer->add_lb_node(node1);
  node1.reset();
  auto node = balancer->get_next("127.0.0.1");
  ASSERT_TRUE(node.expired());
  node = balancer->get_next("127.0.0.1");
  ASSERT_TRUE(node.expired());
}

CASE(iphashaddnode3) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Ip_Hash_Scheduling);
  auto node1 = std::make_shared<IPHASHNode>("node1");
  node1.reset();
  auto flag = balancer->add_lb_node(node1);
  ASSERT_FALSE(flag);
}

CASE(laavg) {
  Box::Instance()->clear();
  Box::Instance()->set_balancer(
      BalancerFactory::create_balancer(BalancerMod::Least_Active_Scheduling));
  Box::Instance()->add_some_node(10, BalancerMod::Least_Active_Scheduling);
  std::map<std::string, std::int32_t> node_hit_count;
  for (std::int32_t i = 0; i <= 255; i++) {
    for (std::int32_t j = 0; j <= 255; j++) {
      auto node = Box::Instance()->get_next();
      if (node.expired()) {
        std::cout << "出大问题！ 节点失效了！" << std::endl;
      }
      auto node_name = node.lock()->get_name();
      node_hit_count[node_name]++;
      node.lock()->set_active_count(node_hit_count[node_name]);
    }
  }
  std::cout << std::endl;
  for (auto &it : node_hit_count) {
    std::cout << "name : " << it.first << " hit count : " << it.second
              << std::endl;
  }
}

CASE(laclear) {
  Box::Instance()->clear();
  auto node = Box::Instance()->get_next();
  auto flag = node.expired();
  ASSERT_TRUE(flag);
}

CASE(laaddnode1) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Least_Active_Scheduling);
  auto node1 = std::make_shared<LANode>("node1", 1);
  balancer->add_lb_node(node1);
  auto node = balancer->get_next();
  ASSERT_FALSE(node.expired());
  ASSERT_EQUAL(node.lock()->get_name(), "node1");
}

CASE(laaddnode2) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Least_Active_Scheduling);
  auto node1 = std::make_shared<LANode>("node1", 1);
  balancer->add_lb_node(node1);
  node1.reset();
  auto node = balancer->get_next();
  ASSERT_TRUE(node.expired());
  node = balancer->get_next();
  ASSERT_TRUE(node.expired());
}

CASE(laaddnode3) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Least_Active_Scheduling);
  auto node1 = std::make_shared<LANode>("node1", 1);
  node1.reset();
  auto flag = balancer->add_lb_node(node1);
  ASSERT_FALSE(flag);
}

CASE(roundrobinavg) {
  Box::Instance()->clear();
  Box::Instance()->set_balancer(
      BalancerFactory::create_balancer(BalancerMod::Round_Robin_Scheduling));
  Box::Instance()->add_some_node(10, BalancerMod::Round_Robin_Scheduling);
  std::map<std::string, std::int32_t> node_hit_count;
  for (std::int32_t i = 0; i <= 255; i++) {
    for (std::int32_t j = 0; j <= 255; j++) {
      auto node = Box::Instance()->get_next();
      if (node.expired()) {
        std::cout << "出大问题！ 节点失效了！" << std::endl;
      }
      auto node_name = node.lock()->get_name();
      node_hit_count[node_name]++;
      node.lock()->set_active_count(node_hit_count[node_name]);
    }
  }
  std::cout << std::endl;
  for (auto &it : node_hit_count) {
    std::cout << "name : " << it.first << " hit count : " << it.second
              << std::endl;
  }
}

CASE(roundrobindclear) {
  Box::Instance()->clear();
  auto node = Box::Instance()->get_next();
  auto flag = node.expired();
  ASSERT_TRUE(flag);
}

CASE(roundrobindaddnode1) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Round_Robin_Scheduling);
  auto node1 = std::make_shared<RRNode>("node1", 1);
  balancer->add_lb_node(node1);
  auto node = balancer->get_next();
  ASSERT_FALSE(node.expired());
  ASSERT_EQUAL(node.lock()->get_name(), "node1");
}

CASE(roundrobindaddnode2) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Round_Robin_Scheduling);
  auto node1 = std::make_shared<RRNode>("node1", 1);
  balancer->add_lb_node(node1);
  node1.reset();
  auto node = balancer->get_next();
  ASSERT_TRUE(node.expired());
  node = balancer->get_next();
  ASSERT_TRUE(node.expired());
}

CASE(roundrobindaddnode3) {
  auto balancer =
      BalancerFactory::create_balancer(BalancerMod::Round_Robin_Scheduling);
  auto node1 = std::make_shared<RRNode>("node1", 1);
  node1.reset();
  auto flag = balancer->add_lb_node(node1);
  ASSERT_FALSE(flag);
}

CASE(smoothroundrobinavg) {
  Box::Instance()->clear();
  Box::Instance()->set_balancer(BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling));
  Box::Instance()->add_some_node(
      10, BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  std::map<std::string, std::int32_t> node_hit_count;
  for (std::int32_t i = 0; i <= 255; i++) {
    for (std::int32_t j = 0; j <= 255; j++) {
      auto node = Box::Instance()->get_next();
      if (node.expired()) {
        std::cout << "出大问题！ 节点失效了！" << std::endl;
        return;
      }
      auto node_name = node.lock()->get_name();
      node_hit_count[node_name]++;
      node.lock()->set_active_count(node_hit_count[node_name]);
    }
  }
  Box::Instance()->print_node();
  std::cout << std::endl;
  for (auto &it : node_hit_count) {
    std::cout << "name : " << it.first << " hit count : " << it.second
              << std::endl;
  }
}

CASE(smoothroundrobinlogic1) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<SWRRNode>("node1", 5);
  balancer->add_lb_node(node1);
  auto node2 = std::make_shared<SWRRNode>("node2", 1);
  balancer->add_lb_node(node2);
  auto node3 = std::make_shared<SWRRNode>("node3", 1);
  balancer->add_lb_node(node3);
  /*
  For edge case weights like { 5, 1, 1 } we now produce { a, a, b, a, c, a, a }
  */
  auto node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node3");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
}

CASE(smoothroundrobinlogic2) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<SWRRNode>("node1", 5);
  balancer->add_lb_node(node1);
  auto node2 = std::make_shared<SWRRNode>("node2", 1);
  balancer->add_lb_node(node2);
  auto node3 = std::make_shared<SWRRNode>("node3", 1);
  balancer->add_lb_node(node3);
  std::int32_t x = 0;
  std::cout << std::endl;
  for (std::int32_t i = 0; i < 80; i++) {
    auto node = balancer->get_next();
    std::cout << node.lock()->get_name() << " ";
    x++;
    if (x % 7 == 0 && x <= 21) {
      std::cout << "\n";
    }
    if (x % 8 == 0 && x > 21) {
      std::cout << "\n";
    }
    if (x == 21) {
      node1.get()->set_weight(6);
    }
  }
  std::cout << std::endl;
}

CASE(smoothroundrobinclear) {
  Box::Instance()->clear();
  auto node = Box::Instance()->get_next();
  auto flag = node.expired();
  ASSERT_TRUE(flag);
}

CASE(smoothroundrobinaddnode1) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<SWRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  auto node = balancer->get_next();
  ASSERT_FALSE(node.expired());
  ASSERT_EQUAL(node.lock()->get_name(), "node1");
}

CASE(smoothroundrobinaddnode2) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<SWRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  node1.reset();
  auto node = balancer->get_next();
  ASSERT_TRUE(node.expired());
  node = balancer->get_next();
  ASSERT_TRUE(node.expired());
}

CASE(smoothroundrobinaddnode3) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<SWRRNode>("node1", 1);
  node1.reset();
  auto flag = balancer->add_lb_node(node1);
  ASSERT_FALSE(flag);
}

CASE(smoothroundrobinaddnode4) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<SWRRNode>("node1", 4);
  balancer->add_lb_node(node1);
  auto node2 = std::make_shared<SWRRNode>("node2", 1);
  balancer->add_lb_node(node2);
  auto node3 = std::make_shared<SWRRNode>("node3", 1);
  balancer->add_lb_node(node3);
  /*
  For edge case weights like { 5, 1, 1 } we now produce { a, a, b, a, c, a, a }
  */
  node1.get()->set_weight(5);
  auto node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node3");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
}

CASE(vnsmoothroundrobinavg) {
  Box::Instance()->clear();
  Box::Instance()->set_balancer(BalancerFactory::create_balancer(
      BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling));
  Box::Instance()->add_some_node(
      10, BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling);
  std::map<std::string, std::int32_t> node_hit_count;
  for (std::int32_t i = 0; i <= 255; i++) {
    for (std::int32_t j = 0; j <= 255; j++) {
      auto node = Box::Instance()->get_next();
      if (node.expired()) {
        std::cout << "出大问题！ 节点失效了！" << std::endl;
      }
      auto node_name = node.lock()->get_name();
      node_hit_count[node_name]++;
      node.lock()->set_active_count(node_hit_count[node_name]);
    }
  }
  Box::Instance()->print_node();
  std::cout << std::endl;
  for (auto &it : node_hit_count) {
    std::cout << "name : " << it.first << " hit count : " << it.second
              << std::endl;
  }
}

CASE(vnsmoothroundrobinclear) {
  Box::Instance()->clear();
  auto node = Box::Instance()->get_next();
  auto flag = node.expired();
  ASSERT_TRUE(flag);
}

CASE(vnsmoothroundrobinaddnode1) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<VNSWRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  auto node = balancer->get_next();
  ASSERT_FALSE(node.expired());
  ASSERT_EQUAL(node.lock()->get_name(), "node1");
}

CASE(vnsmoothroundrobinaddnode2) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<VNSWRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  node1.reset();
  auto node = balancer->get_next();
  ASSERT_TRUE(node.expired());
  node = balancer->get_next();
  ASSERT_TRUE(node.expired());
}

CASE(vnsmoothroundrobinaddnode3) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<VNSWRRNode>("node1", 1);
  node1.reset();
  auto flag = balancer->add_lb_node(node1);
  ASSERT_FALSE(flag);
}

CASE(vnsmoothroundrobinaddnode4) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<VNSWRRNode>("node1", 4);
  balancer->add_lb_node(node1);
  auto node2 = std::make_shared<VNSWRRNode>("node2", 1);
  balancer->add_lb_node(node2);
  auto node3 = std::make_shared<VNSWRRNode>("node3", 1);
  balancer->add_lb_node(node3);
  /*
  For edge case weights like { 5, 1, 1 } we now produce { a, a, b, a, c, a, a }
  */
  node1.get()->set_weight(5);
  balancer->reset_balancer();

  auto node = balancer->get_next();
  ASSERT_FALSE(node.expired());
}

CASE(weightroundrobinavg) {
  Box::Instance()->clear();
  Box::Instance()->set_balancer(BalancerFactory::create_balancer(
      BalancerMod::Weighted_Round_Robin_Scheduling));
  Box::Instance()->add_some_node(10,
                                 BalancerMod::Weighted_Round_Robin_Scheduling);
  std::map<std::string, std::int32_t> node_hit_count;
  for (std::int32_t i = 0; i <= 255; i++) {
    for (std::int32_t j = 0; j <= 255; j++) {
      auto node = Box::Instance()->get_next();
      if (node.expired()) {
        std::cout << "出大问题！ 节点失效了！" << std::endl;
      }
      auto node_name = node.lock()->get_name();
      node_hit_count[node_name]++;
      node.lock()->set_active_count(node_hit_count[node_name]);
    }
  }
  Box::Instance()->print_node();
  std::cout << std::endl;
  for (auto &it : node_hit_count) {
    std::cout << "name : " << it.first << " hit count : " << it.second
              << std::endl;
  }
}

CASE(weightroundrobinlogic1) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<WRRNode>("node1", 4);
  balancer->add_lb_node(node1);
  auto node2 = std::make_shared<WRRNode>("node2", 3);
  balancer->add_lb_node(node2);
  auto node3 = std::make_shared<WRRNode>("node3", 2);
  balancer->add_lb_node(node3);
  /*
  For example, the real servers, A, B and C, have the weights, 4, 3, 2
  respectively, a scheduling sequence will be AABABCABC in a scheduling
  */
  auto node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node3");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node3");
  }
}

CASE(weightroundrobinclear) {
  Box::Instance()->clear();
  auto node = Box::Instance()->get_next();
  auto flag = node.expired();
  ASSERT_TRUE(flag);
}

CASE(weightroundrobinaddnode1) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<WRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  auto node = balancer->get_next();
  ASSERT_FALSE(node.expired());
  ASSERT_EQUAL(node.lock()->get_name(), "node1");
}

CASE(weightroundrobinaddnode2) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<WRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  node1.reset();
  auto node = balancer->get_next();
  ASSERT_TRUE(node.expired());
  node = balancer->get_next();
  ASSERT_TRUE(node.expired());
}

CASE(weightroundrobinaddnode3) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<WRRNode>("node1", 1);
  node1.reset();
  auto flag = balancer->add_lb_node(node1);
  ASSERT_FALSE(flag);
}

CASE(weightroundrobinreset) {
  auto balancer = BalancerFactory::create_balancer(
      BalancerMod::Weighted_Round_Robin_Scheduling);
  auto node1 = std::make_shared<WRRNode>("node1", 1);
  balancer->add_lb_node(node1);
  auto node2 = std::make_shared<WRRNode>("node2", 3);
  balancer->add_lb_node(node2);
  auto node3 = std::make_shared<WRRNode>("node3", 2);
  balancer->add_lb_node(node3);
  /*
  For example, the real servers, A, B and C, have the weights, 4, 3, 2
  respectively, a scheduling sequence will be AABABCABC in a scheduling
  */
  node1.get()->set_weight(4);
  balancer->reset_balancer();
  auto node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node3");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node1");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node2");
  }
  node = balancer->get_next();
  if (!node.expired()) {
    ASSERT_EQUAL(node.lock()->get_name(), "node3");
  }
}

FIXTURE_END(test_load_balancer)
