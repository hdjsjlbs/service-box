#include "../framework/unittest.hh"
#include "detail/box_argument_impl.hh"
#include "util/co_lock.hpp"

FIXTURE_BEGIN(test_co_lock)

CASE(TestLock1) {
  int i = 1;
  using namespace kratos::util;
  CoLock lock(1, &i);
  *lock.own() = 2;
  ASSERT_TRUE(i == 2);
}

CASE(TestLock2) {
  ASSERT_TRUE(kratos::util::co_lock_count() == 0);
}

CASE(TestLock3) {
  std::unordered_map<int, int> m;
  using namespace kratos::util;
  CoLock lock1(1, &m);
  CoLock lock2(2, &m);
  lock1.own();
  ASSERT_EXCEPT(lock2.own());
}

CASE(TestLock4) {
  auto i = std::make_shared<int>(1);
  using namespace kratos::util;
  CoLock lock(1, i);
  *lock.own() = 2;
  ASSERT_TRUE(*i == 2);
}

CASE(TestLock5) {
  auto i = std::make_shared<int>(1);
  using namespace kratos::util;
  CoLock lock(1, i);
  lock.own_ref() = 2;
  ASSERT_TRUE(*i == 2);
}

CASE(TestLock6) {
  int i = 1;
  using namespace kratos::util;
  CoLock lock(1, i);
  *lock.own() = 2;
  ASSERT_TRUE(i == 2);
}

FIXTURE_END(test_co_lock)
