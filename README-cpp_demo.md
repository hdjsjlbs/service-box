# C++ Demo工程示例

>  以一个简单的场景服务器作为示例的一个Demo，包含角色进入场景，角色移动和角色位置广播。用户可以通过这个工程，了解到一些c++基础组件的使用。

## 生成与编译

### Windows

> 如果已经执行过 [安装](README-install.md) 这边可以跳过前3步 从第4步开始

1.  获取 service-box 第三方依赖库 并且 编译service-box

```
cd build							// 进入到build目录
python build_local_env_win.py		// 执行build_local_env_win脚本拉去依赖库

方式1：
cd ../win-proj						// 进入到工程目录
"XXX\MSBuild" service-box.sln -target:service-box
"XXX\MSBuild" /P:Configuration=Release service-box.sln -target:service-box
其中XXX为本地的MSBuild的绝对路径
方式2：
可以打开 service-box.sln 进行 Debug 和 Release 版本的编译
```

2. 初始化repo环境

```
cd ../src/repo							// 进入得到 repo 目录
python init.py							// 执行init脚本
```

3. 初始化cpp rpc 依赖库

```
python repo.py -t cpp -i				// 初始化cpp依赖库
```

4. 添加demo相关的idl文件

```
python repo.py -t cpp -a example\demodata.idl			// 添加三个demo相关的idl
python repo.py -t cpp -a example\demoscene.idl
python repo.py -t cpp -a example\demosynchronize.idl
```

5. 编译相关服务

```
python repo.py -t cpp -b demodata						// 进行三个idl的编译
python repo.py -t cpp -b demoscene
python repo.py -t cpp -b demosynchronize
```

## 发布与证明

6. 发布相关服务

```
cd ../../publish/config/
修改box.json为：
{
	"binary_name" : "box",
	"bundle" : 
	[
		{
			"name" : "demoscene",
			"service" : "Scene"
		}
	],
	"proxy" : 
	[
		{
			"name": "demoscene",
			"proxy": "Scene"
		},
		{
			"name": "demosynchronize",
			"proxy": "Synchronize"
		}
	],
	"stage" : "alpha",
	"version" : "1.0.10"
}

cd ../bin								// 进入到发布脚本目录
python pub.py -c ../config/box.json		// 按照box.json的配置，进行发布
```

7. 运行服务

```
cd ../../tools

用管理员权限执行
第一步：
start_win_local_env.bat
	如果遇到启动失败，需要检查nginx是否已经启动，需要结束掉之前得nginx的父子进程，重新执行。
第二步:
start_win_box.bat
	1. 服务的日志会生成在 service-box/publish/bin/box/Debug/box_yyyymmdd.log.0
```

### Linux

> 如果已经执行过 [安装](README-install.md) 这边可以跳过前1步 从第2步开始

1. 获取 service-box 第三方依赖库 并且 编译service-box

```
cd build							// 进入到build目录
sh install_linux.sh					// 执行build_local_env_win脚本拉去依赖库
```

2. 添加demo相关的idl文件

```
cd ../src/repo											// 进入得到 repo 目录
python repo.py -t cpp -a example/demodata.idl			// 添加三个demo相关的idl
python repo.py -t cpp -a example/demoscene.idl
python repo.py -t cpp -a example/demosynchronize.idl
```

3. 编译相关服务

```
python repo.py -t cpp -b demodata						// 进行三个idl的编译
python repo.py -t cpp -b demoscene
python repo.py -t cpp -b demosynchronize
```

4. 发布相关服务

```
cd ../../publish/config/
修改box.json为：
{
	"binary_name" : "box",
	"bundle" : 
	[
		{
			"name" : "demoscene",
			"service" : "Scene"
		}
	],
	"proxy" : 
	[
		{
			"name": "demoscene",
			"proxy": "Scene"
		},
		{
			"name": "demosynchronize",
			"proxy": "Synchronize"
		}
	],
	"stage" : "alpha",
	"version" : "1.0.10"
}

cd ../bin								// 进入到发布脚本目录
python pub.py -c ../config/box.json		// 按照box.json的配置，进行发布
```

5. 启动服务

```
cd box									// 进入到工作目录
./box -c box.cfg						// 读取配置文件box.cfg 启动box
```

## 测试

> 目前推荐阅读 [一键生成mock使用](README-mock.md) 来进行简单的测试。