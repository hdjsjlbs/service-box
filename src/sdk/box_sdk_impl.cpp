﻿#include "box_sdk_impl.hh"
#include "box/box_channel.hh"
#include "box/box_network.hh"
#include "detail/box_config_impl.hh"
#include "detail/rpc_probe_impl.hh"
#include "klogger/interface/logger.h"
#include "klogger/interface/logger_factory.h"
#include "plugin_loader.hh"
#include "root/rpc_impl.h"
#include "root/rpc_logger.h"
#include "root/rpc_proxy.h"
#include "root/rpc_proxy_creator_interface.h"
#include "root/rpc_root.h"
#include "root/rpc_stub.h"
#include "sdk_frame.hh"
#include "util/os_util.hh"
#include "util/singleton.hh"
#include "util/string_util.hh"

#include <filesystem>
#include <iostream>
#include <list>
#include <memory>
#include <unordered_map>

#if defined(_WIN32) || defined(_WIN64)
#pragma comment(lib, "ws2_32.lib")
#endif

class SDKNetwork;
class SDKRpcLogger;
class ClassLoader;

namespace kratos {
namespace lang {
class Lang;
}
} // namespace kratos

// 管道链接状态
enum class ChannelState {
  NONE,
  DISCONNECTED, ///< 连接断开
  CONNECTING,   ///< 正在连接
  CONNECTED,    ///< 已连接
};

/**
 * SDK全局资源管理类.
 */
struct SDKRes {
  std::unique_ptr<rpc::RpcImpl> rpc;                     ///< RPC实例
  std::unique_ptr<SDKNetwork> network;                   ///< 网络实例
  std::unique_ptr<kratos::config::BoxConfigImpl> config; ///< 配置
  bool close_flag{false};                                ///< 关闭标志
  ChannelState channel_state{ChannelState::NONE};        ///< 管道连接标志
  rpc::TransportPtr trans; ///< 连接的通信管道

  using ProxyMap = std::unordered_map<std::string, rpc::ProxyPtr>;
  ProxyMap proxy_map;                                       ///< 代理实例表
  klogger::Logger *logger{nullptr};                         ///< 日志
  klogger::Appender *log_appender{nullptr};                 ///< 日志添加器
  std::unique_ptr<SDKRpcLogger> rpc_logger;                 ///< RPC日志
  std::unique_ptr<kratos::service::RpcProbeImpl> rpc_probe; ///< RPC探针

  using PluginMap = std::unordered_map<std::string, kratos::sdk::PluginLoader>;
  PluginMap plugin_map; ///< Plugin表
  using ServicePluginList = std::list<std::string>;
  ServicePluginList service_plugin_list; ///< 服务列表
};

/**
 * RPC框架日志实现.
 */
class SDKRpcLogger : public rpc::Logger {
  SDKRes *res_{nullptr};

public:
  /**
   * 构造
   */
  SDKRpcLogger(SDKRes *res) { res_ = res; }
  /**
   * 析构
   */
  virtual ~SDKRpcLogger() {}
  /**
   * 写入日志
   * \param str 日志行
   */
  virtual void write(const std::string &str) override {
    if (res_->log_appender) {
      res_->log_appender->write(klogger::Logger::WARNING, "%s", str.c_str());
    }
  }
};

/**
 * SDK网络通信类实现.
 */
class SDKNetwork : public kratos::service::BoxNetwork {
  BoxSDKImpl *sdk_impl_{nullptr};

public:
  /**
   * 构造.
   *
   */
  SDKNetwork(BoxSDKImpl *sdk_impl) { sdk_impl_ = sdk_impl; }
  /**
   * 析构.
   *
   */
  virtual ~SDKNetwork() {}

public:
  /**
   * 获取服务容器配置
   *
   * \return 服务容器配置
   */
  virtual auto get_config() -> kratos::config::BoxConfig & override {
    return *sdk_impl_->sdk_res_->config.get();
  }
  /**
   * 获取日志添加器.
   *
   * \return
   */
  virtual auto get_logger_appender() -> klogger::Appender * override {
    // 日志添加器
    return sdk_impl_->sdk_res_->log_appender;
  }

  /**
   * 获取本地化实例.
   *
   * \return 本地化实例
   */
  virtual auto get_lang() -> kratos::lang::Lang * override { return nullptr; }
  /**
   * 开启监听事件
   *
   * \param name 监听器名称
   * \param success 成功或失败
   * \param channel 监听管道
   */
  virtual void on_listen(
      const std::string & /*name*/, bool /*success*/,
      std::shared_ptr<kratos::service::BoxChannel> & /*channel*/) override {
    // 不需要实现
    return;
  }
  /**
   * 接受一个新的连接建立
   *
   * \param channel 新建立的管道
   */
  virtual void on_accept(
      std::shared_ptr<kratos::service::BoxChannel> & /*channel*/) override {
    // 不需要实现
    return;
  }
  /**
   * 连接到一个远程监听器
   *
   * \param name 连接器名称
   * \param success 成功或失败
   * \param channel 新建立的管道
   */
  virtual void
  on_connect(const std::string & /*name*/, bool success,
             std::shared_ptr<kratos::service::BoxChannel> &channel) override {
    if (!success) {
      if (sdk_impl_->get_on_connect_timedout_func()) {
        sdk_impl_->get_on_connect_timedout_func()(*sdk_impl_);
      }
      // 需要重连
      sdk_impl_->sdk_res_->channel_state = ChannelState::DISCONNECTED;
    } else {
      sdk_impl_->sdk_res_->channel_state = ChannelState::CONNECTED;
      sdk_impl_->sdk_res_->trans = channel;
      if (sdk_impl_->get_on_connected_evt_func()) {
        sdk_impl_->get_on_connected_evt_func()(*sdk_impl_);
      }
    }
    return;
  }
  /**
   * 关闭管道事件
   *
   * \param channel 关闭的管道
   */
  virtual void
  on_close(std::shared_ptr<kratos::service::BoxChannel> &channel) override {
    if (sdk_impl_->sdk_res_->close_flag) {
      return;
    }
    // 需要重连
    sdk_impl_->sdk_res_->channel_state = ChannelState::DISCONNECTED;
    sdk_impl_->sdk_res_->trans = nullptr;
    if (sdk_impl_->get_on_disconnected_evt_func()) {
      sdk_impl_->get_on_disconnected_evt_func()(*sdk_impl_);
    }
    return;
  }
  /**
   * 数据事件
   *
   * \param channel 接收到数据的管道
   */
  virtual void
  on_data(std::shared_ptr<kratos::service::BoxChannel> &channel) override {
    rpc::TransportPtr trans = channel;
    sdk_impl_->sdk_res_->rpc->onMessage(trans, nullptr);
  }
};

BoxSDKImpl::BoxSDKImpl() {}

BoxSDKImpl::~BoxSDKImpl() {}

auto BoxSDKImpl::initialize(const std::string &config_file,
                            std::string &error_str,
                            void *user_ptr) noexcept(true) -> bool {
  try {
    if (sdk_res_) {
      return false;
    }
    sdk_res_ = new SDKRes();
    if (!initialize_internal(config_file, error_str)) {
      return false;
    }
  } catch (std::exception &ex) {
    error_str = ex.what();
    return false;
  }
  usr_ptr_ = user_ptr;
  return true;
}

auto BoxSDKImpl::deinitialize() -> void {
  if (!sdk_res_) {
    return;
  }
  sdk_res_->close_flag = true;
  sdk_res_->proxy_map.clear();
  if (sdk_res_->network) {
    sdk_res_->network->stop();
  }
  // 清理RPC框架，包含proxy相关资源
  if (sdk_res_->rpc) {
    sdk_res_->rpc->deinitialize(true);
  }
  if (sdk_res_->log_appender) {
    sdk_res_->log_appender->destroy();
  }
  if (sdk_res_->logger) {
    sdk_res_->logger->destroy();
  }
  if (sdk_res_->rpc_probe) {
    sdk_res_->rpc_probe.reset();
  }
  sdk_res_->plugin_map.clear();
  for (const auto &uuid : sdk_res_->service_plugin_list) {
    rpc::unloadClassUnsafe(sdk_res_->rpc.get(), uuid);
  }
  usr_ptr_ = nullptr;
  delete sdk_res_;
  sdk_res_ = nullptr;
}

auto BoxSDKImpl::tick() -> void {
  if (!sdk_res_) {
    return;
  }
  // 网络主循环
  sdk_res_->network->update();
  // RPC框架主循环
  sdk_res_->rpc->update();
  if (sdk_res_->channel_state != ChannelState::NONE) {
    if (sdk_res_->channel_state == ChannelState::DISCONNECTED) {
      // 需要重连
      // 从配置内读取配置并启动连接
      sdk_res_->network->connect_to(
          "remote_sdk_point", "tcp",
          sdk_res_->config->get_string("sdk.remote_host"),
          sdk_res_->config->get_number<std::int32_t>("sdk.remote_port"),
          sdk_res_->config->get_number<std::int32_t>("sdk.conn_timeout"));
      sdk_res_->channel_state = ChannelState::CONNECTING;
    }
  }
}

auto BoxSDKImpl::is_connected() -> bool {
  if (!sdk_res_) {
    return false;
  }
  return (sdk_res_->channel_state == ChannelState::CONNECTED);
}

void BoxSDKImpl::on_connect_timedout(SDKEventFunction evt_func) {
  on_connect_timedout_evt_ = evt_func;
}

void BoxSDKImpl::on_connected(SDKEventFunction evt_func) {
  on_connected_evt_ = evt_func;
}

void BoxSDKImpl::write_log(const std::string &log) {
  if (!sdk_res_) {
    return;
  }
  if (sdk_res_->log_appender) {
    sdk_res_->log_appender->write(klogger::Logger::INFORMATION, "%s",
                                  log.c_str());
  }
}

auto BoxSDKImpl::get_usr_ptr() -> void * { return usr_ptr_; }

void BoxSDKImpl::on_disconnected(SDKEventFunction evt_func) {
  on_disconnectd_evt_ = evt_func;
}

void BoxSDKImpl::set_destroy_func(FrameDestroy func) { destroy_func_ = func; }

FrameDestroy BoxSDKImpl::get_destroy_func() { return destroy_func_; }

auto BoxSDKImpl::get_on_connected_evt_func() -> SDKEventFunction {
  return on_connected_evt_;
}

auto BoxSDKImpl::get_on_connect_timedout_func() -> SDKEventFunction {
  return on_connect_timedout_evt_;
}

auto BoxSDKImpl::get_on_disconnected_evt_func() -> SDKEventFunction {
  return on_disconnectd_evt_;
}

auto BoxSDKImpl::get_proxy_raw(const std::string &ref_str,
                               std::string &error_str) -> rpc::Proxy * {
  if (!sdk_res_) {
    return nullptr;
  }
  if (sdk_res_->channel_state != ChannelState::CONNECTED) {
    save_and_log("The channel of SDK disconnected", error_str);
    return nullptr;
  }
  auto it = sdk_res_->proxy_map.find(ref_str);
  if ((it != sdk_res_->proxy_map.end()) &&
      (sdk_res_->channel_state == ChannelState::CONNECTED)) {
    return it->second.get();
  } else {
    auto prx_ptr = sdk_res_->rpc->getProxyCreator()->getService(
        ref_str, sdk_res_->trans, true, sdk_res_->rpc.get());
    if (!prx_ptr) {
      save_and_log("Proxy not found[" + ref_str + "]", error_str);
      return nullptr;
    }
    // 更新代理实例表
    sdk_res_->proxy_map[ref_str] = prx_ptr;
    return dynamic_cast<rpc::Proxy *>(sdk_res_->proxy_map[ref_str].get());
  }
}

auto BoxSDKImpl::get_class_name_suffix() -> const std::string & {
  static const std::string CLASS_NAME_SUFFIX("Impl");
  return CLASS_NAME_SUFFIX;
}

auto BoxSDKImpl::get_class_name_prefix() -> const std::string & {
  static const std::string CLASS_NAME_PREFIX("class ");
  return CLASS_NAME_PREFIX;
}

auto BoxSDKImpl::initialize_internal(const std::string &config_file,
                                     std::string &error_str,
                                     void *user_ptr) noexcept(false) -> bool {
  if (!sdk_res_) {
    return false;
  }
  sdk_res_->rpc.reset(new rpc::RpcImpl());
  sdk_res_->network.reset(new SDKNetwork(this));
  sdk_res_->config.reset(new kratos::config::BoxConfigImpl(nullptr));
  // 加载配置文件
  if (!sdk_res_->config->load(config_file, error_str)) {
    std::cout << "Start failed[" + error_str << "]" << std::endl;
    return false;
  }
  if (sdk_res_->config->has_attribute("sdk.log_cfg")) {
    auto *logger = LoggerFactory::createLogger();
    if (!logger) {
      save_and_log("Fork logger instance failed", error_str);
      return false;
    }
    sdk_res_->logger = logger;
    sdk_res_->log_appender = logger->newAppender(
        "default", sdk_res_->config->get_string("sdk.log_cfg"));
  }
  if (!sdk_res_->config->has_attribute("sdk.remote_host")) {
    save_and_log("sdk.remote_host not found in config file", error_str);
    return false;
  }
  if (!sdk_res_->config->has_attribute("sdk.remote_port")) {
    save_and_log("sdk.remote_port not found in config file", error_str);
    return false;
  }
  if (!sdk_res_->config->has_attribute("sdk.conn_timeout")) {
    save_and_log("sdk.conn_timeout not found in config file", error_str);
    return false;
  }
  sdk_res_->rpc_logger.reset(new SDKRpcLogger(sdk_res_));
  if (sdk_res_->config->is_open_trace()) {
    sdk_res_->rpc_probe.reset(
        new kratos::service::RpcProbeImpl(sdk_res_->config.get()));
    sdk_res_->rpc->setRpcProbe(sdk_res_->rpc_probe.get());
    sdk_res_->rpc->openTrace(true);
  }
  sdk_res_->rpc->initialize(sdk_res_->rpc_logger.get(), false, false);
  if (!sdk_res_->network->start()) {
    save_and_log("Start SDK network failed", error_str);
    return false;
  }
  // 从配置内读取配置并启动连接
  if (!sdk_res_->network->connect_to(
          "remote_sdk_point", "tcp",
          sdk_res_->config->get_string("sdk.remote_host"),
          sdk_res_->config->get_number<std::int32_t>("sdk.remote_port"),
          sdk_res_->config->get_number<std::int32_t>("sdk.conn_timeout"))) {
    save_and_log("Connect to remote host failed, please check config file",
                 error_str);
    return false;
  }
  std::string plugin_dir = ".";
  if (sdk_res_->config->has_attribute("sdk.plugin_dir")) {
    plugin_dir = sdk_res_->config->get_string("sdk.plugin_dir");
  }
  load_all_plugin(plugin_dir);
  load_all_service_plugin(sdk_res_->rpc.get(), plugin_dir, user_ptr);
  return true;
}

#if defined(WIN32) || defined(WIN64) || defined(_WIN32)
auto BoxSDKImpl::add_service(rpc::BundleEntryFunc enter_func, 
                             rpc::BundleRegisterFunc reg_func, 
                             rpc::BundleInterfaceDescriptorFunc desc_func, 
                             rpc::BundleVersionFunc version_func,
                             rpc::BundleBuildFunc build_func) -> bool
{
    auto uuid = reg_func(nullptr);
    std::cout << "Adding service [" << uuid << "]...";
    rpc::IServiceInterface service{enter_func,reg_func,desc_func,version_func,build_func};
    if (rpc::addClass(sdk_res_->rpc.get(), std::to_string(uuid), &service))
    {
        sdk_res_->service_plugin_list.push_back(std::to_string(uuid));
        std::cout << "done" << std::endl;
        return true;
    }
    std::cout << "failed" << std::endl;
    return false;
}
#endif // defined(WIN32) || defined(WIN64) || defined(_WIN32)

BoxSDK *get_sdk_frame() {
  try {
    return new BoxSDKImpl();
  } catch (...) {
    return nullptr;
  }
}

void destroy_sdk_frame(BoxSDK *sdk_ptr) {
  if (!sdk_ptr) {
    return;
  }
  sdk_ptr->deinitialize();
  delete sdk_ptr;
}

/**
 * 保存错误描述并尝试写日志.
 *
 * \param error
 * \param error_str
 * \return
 */
auto BoxSDKImpl::save_and_log(const std::string &error, std::string &error_str)
    -> void {
  if (!sdk_res_) {
    return;
  }
  error_str = error;
  if (sdk_res_->log_appender) {
    sdk_res_->log_appender->write(klogger::Logger::FAILURE, "%s",
                                  error.c_str());
  }
}

/**
 * 从指定的目录下加载所有插件.
 *
 * \param plugin_dir 插件目录
 * \return
 */
auto BoxSDKImpl::load_all_plugin(const std::string &plugin_dir) noexcept(false)
    -> void {
  if (!sdk_res_) {
    return;
  }
  namespace fs = std::filesystem;
  std::vector<std::string> files;
  kratos::util::get_file_in_directory(plugin_dir, ".so", files);
  for (const auto &file : files) {
    // 通过命名方式控制，防止加载错误类型的SO
    if (kratos::util::endWith(fs::path(file).filename().string(),
                              "_proxy.so") ||
        kratos::util::endWith(fs::path(file).filename().string(),
                              "_module_sdk.so")) {
      std::cout << "Load module " << file << "...done" << std::endl;
      kratos::sdk::PluginLoader cl(file);
      // 注册plugin
      cl.do_register(sdk_res_->rpc.get());
      sdk_res_->plugin_map.emplace(file, std::move(cl));
    }
  }
}

/**
 * 从指定的目录下加载所有插件实现类.
 *
 * \param plugin_dir 插件目录
 * \return
 */
auto BoxSDKImpl::load_all_service_plugin(rpc::Rpc *rpc,
                                         const std::string &plugin_dir,
                                         void *user_ptr) noexcept(false)
    -> void {
  if (!sdk_res_) {
    return;
  }
  namespace fs = std::filesystem;
  std::vector<std::string> files;
  kratos::util::get_file_in_directory(plugin_dir, ".so", files);
  for (const auto &file : files) {
    // 通过命名方式控制，防止加载错误类型的SO
    if (kratos::util::endWith(fs::path(file).filename().string(), "_sdk.so") &&
        !kratos::util::endWith(fs::path(file).filename().string(),
                               "module_sdk.so")) {
      auto uuid = kratos::sdk::PluginLoader::get_uuid(file);
      std::cout << "Loading service " << file << "[" << uuid << "]...";
      if (rpc::loadClass(rpc, std::to_string(uuid), file, true,
                         (HostServiceContext *)user_ptr)) {
        sdk_res_->service_plugin_list.push_back(std::to_string(uuid));
        std::cout << "done" << std::endl;
      } else {
        std::cout << "failed" << std::endl;
      }
    }
  }
}
