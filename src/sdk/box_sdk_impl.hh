﻿#pragma once

#include "box_sdk.hh"

struct SDKRes;

/**
 * SDK框架实现类.
 */
class BoxSDKImpl : public BoxSDK {
  SDKEventFunction on_connected_evt_;
  SDKEventFunction on_connect_timedout_evt_;
  SDKEventFunction on_disconnectd_evt_;
  void *usr_ptr_{nullptr};
  FrameDestroy destroy_func_{nullptr};

public:
  SDKRes *sdk_res_{nullptr};

public:
  /**
   * 构造.
   *
   */
  BoxSDKImpl();
  /**
   * 析构.
   *
   */
  virtual ~BoxSDKImpl();
  /**
   * @see BoxSDK::initialize.
   */
  virtual auto initialize(const std::string &config_file,
                          std::string &error_str,
                          void *user_ptr = nullptr) noexcept(true)
      -> bool override;
  
  #if defined(WIN32) || defined(WIN64) || defined(_WIN32)
  virtual auto add_service(rpc::BundleEntryFunc enter_func, 
                           rpc::BundleRegisterFunc reg_func, 
                           rpc::BundleInterfaceDescriptorFunc desc_func, 
                           rpc::BundleVersionFunc version_func,
                           rpc::BundleBuildFunc build_func) -> bool override;
  #endif // #if defined(WIN32) || defined(WIN64) || defined(_WIN32)

  /**
   * @see BoxSDK::deinitialize.
   */
  virtual auto deinitialize() -> void override;
  /**
   * @see BoxSDK::tick.
   */
  virtual auto tick() -> void override;
  /**
   * @see BoxSDK::is_connected.
   */
  virtual auto is_connected() -> bool override;
  virtual void on_connect_timedout(SDKEventFunction evt_func) override;
  virtual void on_connected(SDKEventFunction evt_func) override;
  virtual void write_log(const std::string &log) override;
  virtual auto get_usr_ptr() -> void * override;
  /**
   * 与远程服务器断开连接.
   *
   * \param evt_func 回调函数
   */
  virtual void on_disconnected(SDKEventFunction evt_func) override;
  /**
   * 设置销毁函数
   */
  virtual void set_destroy_func(FrameDestroy func) override;
  /**
   * 获取销毁函数
   */
  virtual FrameDestroy get_destroy_func() override;

public:
  auto get_on_connected_evt_func() -> SDKEventFunction;
  auto get_on_connect_timedout_func() -> SDKEventFunction;
  auto get_on_disconnected_evt_func() -> SDKEventFunction;

protected:
  virtual auto get_proxy_raw(const std::string &ref_str, std::string &error_str)
      -> rpc::Proxy * override;
  virtual auto get_class_name_suffix() -> const std::string & override;
  virtual auto get_class_name_prefix() -> const std::string & override;

private:
  /**
   * 加载.
   *
   * \param config_file
   * \param error_str
   * \return
   */
  auto initialize_internal(const std::string &config_file,
                           std::string &error_str,
                           void *user_ptr = nullptr) noexcept(false) -> bool;
  /**
   * 保存错误描述并尝试写日志.
   *
   * \param error
   * \param error_str
   * \return
   */
  auto save_and_log(const std::string &error, std::string &error_str) -> void;
  /**
   * 从指定的目录下加载所有插件.
   *
   * \param plugin_dir 插件目录
   * \return
   */
  auto load_all_plugin(const std::string &plugin_dir) noexcept(false) -> void;
  /**
   * 从指定的目录下加载所有插件实现类.
   *
   * \param plugin_dir 插件目录
   * \return
   */
  auto load_all_service_plugin(rpc::Rpc *rpc, const std::string &plugin_dir,
                               void *user_ptr) noexcept(false) -> void;
};
