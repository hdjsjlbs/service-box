#include "time_util.hh"
#include <atomic>
#include <chrono>

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
#include <windows.h>
#else
#include <sys/time.h>
#endif

namespace kratos {
namespace util {
std::atomic_int64_t last_updating_millionsecond = 0;
std::atomic_int64_t begin_frame_millionsecond = 0;
std::atomic_int64_t current_frame_cost_millionsecond = 0;
std::atomic_int64_t lowest_cost_millionsecond = 0;
std::atomic_int64_t highest_cost_millionsecond = 0;
} // namespace util
} // namespace kratos

auto kratos::util::get_os_time_millionsecond() -> std::time_t {
  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch())
                .count();
  return ms;
}

auto kratos::util::get_last_timestamp_millionsecond() -> std::time_t {
  return last_updating_millionsecond;
}

auto kratos::util::set_last_timestamp_millionsecond(std::time_t now) -> void {
  last_updating_millionsecond = now;
}

auto kratos::util::get_lowest_cost_millionsecond() -> std::time_t {
  return lowest_cost_millionsecond;
}

auto kratos::util::get_highest_cost_millionsecond() -> std::time_t {
  return highest_cost_millionsecond;
}

auto kratos::util::set_frame_begin(std::time_t now) -> void {
  begin_frame_millionsecond = now;
}

auto kratos::util::set_frame_end(std::time_t now) -> void {
  auto gap = now - begin_frame_millionsecond;
  current_frame_cost_millionsecond = gap;
  if (!lowest_cost_millionsecond) {
    lowest_cost_millionsecond = gap;
  } else {
    if (gap < lowest_cost_millionsecond) {
      lowest_cost_millionsecond = gap;
    }
  }
  if (!highest_cost_millionsecond) {
    highest_cost_millionsecond = gap;
  } else {
    if (gap > highest_cost_millionsecond) {
      highest_cost_millionsecond = gap;
    }
  }
}

auto kratos::util::get_real_frame() -> int {
  auto temp_frame_cost_ms = current_frame_cost_millionsecond.load();
  if (!temp_frame_cost_ms) {
    return 1000;
  }
  return (int)(std::time_t(1000) / temp_frame_cost_ms);
}

auto kratos::util::get_current_cost_millionsecond() -> std::time_t {
  return current_frame_cost_millionsecond.load();
}

auto kratos::util::get_os_time_microsecond() -> std::time_t {
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
#define EPOCHFILETIME (116444736000000000UL)
  FILETIME ft;
  LARGE_INTEGER li;
  GetSystemTimeAsFileTime(&ft);
  li.LowPart = ft.dwLowDateTime;
  li.HighPart = ft.dwHighDateTime;
  return (li.QuadPart - EPOCHFILETIME) / 10;
#else
  struct timeval tv;
  uint64_t ms;
  gettimeofday(&tv, 0);
  ms = tv.tv_sec * 1000 * 1000;
  ms += tv.tv_usec;
  return ms;
#endif // _WIN32
}
