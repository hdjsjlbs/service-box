#pragma once

#define DB_EXPAND(x) x
#define DB_GET_MACRO(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13,   \
                     _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24,    \
                     _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35,    \
                     _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46,    \
                     _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57,    \
                     _58, _59, _60, _61, _62, _63, _64, NAME, ...)             \
  NAME
#define DB_PASTE(...)                                                          \
  DB_EXPAND(DB_GET_MACRO(                                                      \
      __VA_ARGS__, DB_PASTE64, DB_PASTE63, DB_PASTE62, DB_PASTE61, DB_PASTE60, \
      DB_PASTE59, DB_PASTE58, DB_PASTE57, DB_PASTE56, DB_PASTE55, DB_PASTE54,  \
      DB_PASTE53, DB_PASTE52, DB_PASTE51, DB_PASTE50, DB_PASTE49, DB_PASTE48,  \
      DB_PASTE47, DB_PASTE46, DB_PASTE45, DB_PASTE44, DB_PASTE43, DB_PASTE42,  \
      DB_PASTE41, DB_PASTE40, DB_PASTE39, DB_PASTE38, DB_PASTE37, DB_PASTE36,  \
      DB_PASTE35, DB_PASTE34, DB_PASTE33, DB_PASTE32, DB_PASTE31, DB_PASTE30,  \
      DB_PASTE29, DB_PASTE28, DB_PASTE27, DB_PASTE26, DB_PASTE25, DB_PASTE24,  \
      DB_PASTE23, DB_PASTE22, DB_PASTE21, DB_PASTE20, DB_PASTE19, DB_PASTE18,  \
      DB_PASTE17, DB_PASTE16, DB_PASTE15, DB_PASTE14, DB_PASTE13, DB_PASTE12,  \
      DB_PASTE11, DB_PASTE10, DB_PASTE9, DB_PASTE8, DB_PASTE7, DB_PASTE6,      \
      DB_PASTE5, DB_PASTE4, DB_PASTE3, DB_PASTE2, DB_PASTE1)(__VA_ARGS__))
#define DB_PASTE2(func, v1) func(v1)
#define DB_PASTE3(func, v1, v2) DB_PASTE2(func, v1) DB_PASTE2(func, v2)
#define DB_PASTE4(func, v1, v2, v3) DB_PASTE2(func, v1) DB_PASTE3(func, v2, v3)
#define DB_PASTE5(func, v1, v2, v3, v4)                                        \
  DB_PASTE2(func, v1) DB_PASTE4(func, v2, v3, v4)
#define DB_PASTE6(func, v1, v2, v3, v4, v5)                                    \
  DB_PASTE2(func, v1) DB_PASTE5(func, v2, v3, v4, v5)
#define DB_PASTE7(func, v1, v2, v3, v4, v5, v6)                                \
  DB_PASTE2(func, v1) DB_PASTE6(func, v2, v3, v4, v5, v6)
#define DB_PASTE8(func, v1, v2, v3, v4, v5, v6, v7)                            \
  DB_PASTE2(func, v1) DB_PASTE7(func, v2, v3, v4, v5, v6, v7)
#define DB_PASTE9(func, v1, v2, v3, v4, v5, v6, v7, v8)                        \
  DB_PASTE2(func, v1) DB_PASTE8(func, v2, v3, v4, v5, v6, v7, v8)
#define DB_PASTE10(func, v1, v2, v3, v4, v5, v6, v7, v8, v9)                   \
  DB_PASTE2(func, v1) DB_PASTE9(func, v2, v3, v4, v5, v6, v7, v8, v9)
#define DB_PASTE11(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10)              \
  DB_PASTE2(func, v1) DB_PASTE10(func, v2, v3, v4, v5, v6, v7, v8, v9, v10)
#define DB_PASTE12(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11)         \
  DB_PASTE2(func, v1) DB_PASTE11(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11)
#define DB_PASTE13(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12)    \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE12(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12)
#define DB_PASTE14(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13)                                                        \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE13(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13)
#define DB_PASTE15(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14)                                                   \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE14(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14)
#define DB_PASTE16(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15)                                              \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE15(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15)
#define DB_PASTE17(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16)                                         \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE16(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16)
#define DB_PASTE18(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17)                                    \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE17(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17)
#define DB_PASTE19(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18)                               \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE18(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18)
#define DB_PASTE20(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19)                          \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE19(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19)
#define DB_PASTE21(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20)                     \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE20(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20)
#define DB_PASTE22(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21)                \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE21(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21)
#define DB_PASTE23(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22)           \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE22(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22)
#define DB_PASTE24(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23)      \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE23(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23)
#define DB_PASTE25(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24) \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE24(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24)
#define DB_PASTE26(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25)                                                        \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE25(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25)
#define DB_PASTE27(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26)                                                   \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE26(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26)
#define DB_PASTE28(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27)                                              \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE27(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27)
#define DB_PASTE29(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28)                                         \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE28(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28)
#define DB_PASTE30(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29)                                    \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE29(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29)
#define DB_PASTE31(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30)                               \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE30(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30)
#define DB_PASTE32(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31)                          \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE31(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31)
#define DB_PASTE33(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32)                     \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE32(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32)
#define DB_PASTE34(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33)                \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE33(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33)
#define DB_PASTE35(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34)           \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE34(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34)
#define DB_PASTE36(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35)      \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE35(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35)
#define DB_PASTE37(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36) \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE36(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36)
#define DB_PASTE38(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37)                                                        \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE37(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37)
#define DB_PASTE39(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38)                                                   \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE38(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38)
#define DB_PASTE40(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39)                                              \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE39(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39)
#define DB_PASTE41(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40)                                         \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE40(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40)
#define DB_PASTE42(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41)                                    \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE41(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41)
#define DB_PASTE43(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42)                               \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE42(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42)
#define DB_PASTE44(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43)                          \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE43(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43)
#define DB_PASTE45(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44)                     \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE44(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44)
#define DB_PASTE46(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45)                \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE45(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45)
#define DB_PASTE47(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46)           \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE46(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46)
#define DB_PASTE48(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47)      \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE47(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47)
#define DB_PASTE49(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48) \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE48(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48)
#define DB_PASTE50(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49)                                                        \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE49(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49)
#define DB_PASTE51(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50)                                                   \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE50(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50)
#define DB_PASTE52(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51)                                              \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE51(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51)
#define DB_PASTE53(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52)                                         \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE52(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52)
#define DB_PASTE54(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53)                                    \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE53(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53)
#define DB_PASTE55(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54)                               \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE54(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54)
#define DB_PASTE56(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55)                          \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE55(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55)
#define DB_PASTE57(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56)                     \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE56(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56)
#define DB_PASTE58(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57)                \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE57(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57)
#define DB_PASTE59(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57, v58)           \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE58(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57, v58)
#define DB_PASTE60(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59)      \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE59(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57, v58, v59)
#define DB_PASTE61(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60) \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE60(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57, v58, v59, v60)
#define DB_PASTE62(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, \
                   v61)                                                        \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE61(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57, v58, v59, v60, v61)
#define DB_PASTE63(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, \
                   v61, v62)                                                   \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE62(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57, v58, v59, v60, v61, v62)
#define DB_PASTE64(func, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,    \
                   v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, \
                   v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, \
                   v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, \
                   v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, \
                   v61, v62, v63)                                              \
  DB_PASTE2(func, v1)                                                          \
  DB_PASTE63(func, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,    \
             v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27,  \
             v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40,  \
             v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53,  \
             v54, v55, v56, v57, v58, v59, v60, v61, v62, v63)

#define DEFINE_HAS_METHOD_TEMPLATE(name)                                       \
  template <typename T> class has_##name {                                     \
    typedef char one;                                                          \
    struct two {                                                               \
      char x[2];                                                               \
    };                                                                         \
    template <typename C> static one test(decltype(&C::name));                 \
    template <typename C> static two test(...);                                \
                                                                               \
  public:                                                                      \
    constexpr static std::size_t value = sizeof(test<T>(0)) == sizeof(char);   \
  };
