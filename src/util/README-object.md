# 对象系统

object.hh

### 现状

1. 指针的持有对于缺乏设计经验的工程师是一个巨大的挑战，当指针被多个所有者持有时，指针的生命周期管理是混乱的，难于管理，更不利于团队合作开发完成，容易产生悬吊指针
2. 现在可以通过管理器来集中管理的方式来管理指针对象，同时外部持有ID的方式，这种方式需要重复写大量的管理器代码

### 解决方案

1. 每个线程有一个独立的，隐藏的对象管理器，对使用者透明，用户不用关心管理器在哪里，是如何设计的
2. 同一个指针对象可以同时被多个所有者持有，任何一个持有者调用销毁方法，所有其他的持有者都可以在使用时判断指针是否有效
3. 明确区分单线程对象和多线程对象，单线程对象只能在创建它的线程内使用，多线程对象可以同时被多个线程持有，也可以在任何一个线程内释放（用户对象本身是否线程安全，由用户对象的设计者自行保证）
4. 可以用"对象指针"作为key,但是不会受到对象地址复用带来的一系列问题的困扰

## 建立对象


```
class MyObject {
public:
    MyObject(const std::string& a, int b) {
    }
    virtual ~MyObject() {}
    int method() { return 0; }    
};

using namespace kratos::object_system;

auto ref = spawn_object(MyObject, "hello", 1);
auto ref_mp = spawn_object_multithread(MyObject, "hello", 1);

ObjectRef<MyObject> objRef = spawn_object(MyObject, "hello world", 1);

ref->method();
ref_mp->method();

ref.dispose();
ref_mp.dispose()
```

## 判断有效性


```
auto ref = spawn_object(MyObject, "hello", 1);
auto temp = ref;
ref.dispose();
// 任何一个观察同一个对象的引用调用dispose则temp和ref同时失效
if (!ref) {
    // 判断失效了
}
if (!temp) {
    // 判断失效了
}
```


## 销毁对象


```
ref.dispose();
ref_mp.dispose();
```

## 单线程 VS 多线程

1. 使用spawn_object创建单线程对象
2. 使用spawn_object_multithread创建多线程对象

单线程对象只能在建立这个对象的线程内使用，否则会抛出异常，多线程对象可以在多个线程内使用

## 作为STL容器键

1. object_set 对应 std::set<ObjectRef<T>>
2. object_map 对应 std::map<ObjectRef<T1>, T2>
3. object_unordered_map 对应 std::unordered_map<ObjectRef<T1>, T2>
4. object_unordered_set 对应std::unordered_set<ObjectRef<T>>

## 类型转换
1. static_cast_ref
2. dynamic_cast_ref
3. const_cast_ref

## 自动释放包装类UniqueRef

使用UniqueRef可以自动释放所持有的引用所观察的对象，不需要调用dispose方法，作为对象释放的最后手段，如果怕忘记调用dispose导致的对象无法释放，UniqueRef不像std::unique_ptr，UniqueRef不需要排它的维持引用，因为所有ObjectRef都是实际对象的观察者，任何一个观察相同对象的ObjectRef调用dispose其他观察者都会通过检查有效性得到通知，不像std::unique_ptr的析构将导致所有其他指针持有者的调用崩溃，同时无法通过指针地址判断是否是悬吊指针.

## 使用用例
1. 父子对象
```
class BaseClass {
public:
	virtual ~BaseClass() {}
        void method() {}
};

class ChildClass : public BaseClass {
public:
	virtual ~ChildClass() {}
        void method1() {}
};

auto ref = spawn_object(ChildClass);
auto base_ref = dynamic_cast_ref<BaseClass>(ref);
base_ref->method();
base_ref.dispose();
if (!ref) {
    // base_ref和ref都已失效
}
try {
    ref->method1();
} catch (...) {
    // 将抛出一个异常，对象已经无效
}
```
2. 作为容器的健

```
class MyObject {
public:
    MyObject() {}
    MyObject(const std::string& a, int b) {
    }
    virtual ~MyObject() {}
    int method() { return 0; }    
};

object_map<MyObject, std::string> om;
auto ref1 = spawn_object(MyObject);
auto ref2 = spawn_object(MyObject);
om[ref1] = "a";
om[ref2] = "b";
for (auto& i : om) {
    i.first.dispose();
}
```
3. 跨线程销毁

```
ObjectRef<Dummy> ref = spawn_object_multithread(Dummy);
std::thread t([&]() {
    // 跨线程对象可以被另外一个线程销毁
    ref.dispose();
});
```
4. 单线程对象只能在所属线程内销毁

```
ObjectRef<Dummy> ref = spawn_object(Dummy);
std::thread t([&]() {
    // 跨线程对象可以被另外一个线程销毁
    if (!ref.dispose()) {
        // 会返回false, 不能在另外一个线程销毁
    }
});
```
5. 作为类成员变量

```
	class MyObject {
	public:
		MyObject(const std::string& a, int b) {
		}
		virtual ~MyObject() {}
		int method() const { return 0; }
	};

	using OtherRef = ObjectRef<MyObject>;

	class ComObject {
	private:
		OtherRef obj_ref_;
	public:
		ComObject(const OtherRef& other_obj) : obj_ref_(other_obj) {
		}
		virtual ~ComObject() {
			obj_ref_.dispose();
		}
		const OtherRef& getOther() const {
			return obj_ref_;
		}
	};

	auto com_ref = spawn_object(ComObject, spawn_object(MyObject, "hello", 1));
	com_ref->getOther()->method();
	com_ref.dispose();
```
6. 自动化释放

```
class MyObject {
	public:
		MyObject(const std::string& a, int b) {
		}
		virtual ~MyObject() {}
		int method() const { return 0; }
	};

	using UniqueOtherRef = UniqueRef<MyObject>;
	using OtherRef = ObjectRef<MyObject>;

	class ComObject {
	private:
		UniqueOtherRef obj_ref_; // 会自动释放，不需要调用dispose
	public:
		ComObject(const OtherRef& other_obj) : obj_ref_(other_obj) {
		}
		virtual ~ComObject() {
		}
		const OtherRef& getOther() const {
			return obj_ref_.get();
		}
	};

	auto com_ref = spawn_object(ComObject, spawn_object(MyObject, "hello", 1));
	UniqueRef<ComObject> unique_ref(com_ref);
	unique_ref.get()->getOther()->method();
        // 这里com_ref会自动释放, 不需要调用dispose
```
 7. 直接构造自动化释放对象

```
class MyObject {
public:
    MyObject() {}
    MyObject(const std::string& a, int b) {}
    virtual ~MyObject() {}
    int method() { return 0; }
};
auto unique_ref1 = UniqueRef<MyObject>(Object::ThreadMode::mode_single);
auto unique_ref2 = UniqueRef<MyObject>(Object::ThreadMode::mode_multi, "hello", 1);
```

8. 对象跨线程传递

```
std::list<ObjectRef<MyObject>> obj_list;
std::mutex mutex_;
std::atomic_bool running_(true);

auto std::thread t1([&]() {
    while (running_) {
        std::lock_guard<std::mutex> guard(mutex_);
        obj_list.push_back(spawn_object_multithread(MyObject, "hello", 1));
    }
});

auto std::thread t2([&]() {
    while (running_) {
        std::lock_guard<std::mutex> guard(mutex_);
        if (!obj_list.empty()) {
            auto obj_ref = obj_list.front();
            obj_list.pop_front();
            obj_ref->method();
            obj_ref.dispose();
        }
    }
});

t1.join();
t2.join();


```