#pragma once

#include "util/bucket_token/token_consumer.hh"

namespace kratos {
namespace util {

/**
 * 令牌消费者实现类.
 */
class TokenConsumerImpl : public TokenConsumer {
  TokenProducerPtr token_producer_; ///< 令牌产生器

public:
  /**
   * 构造.
   *
   * \param token_producer_ptr 令牌产生器
   */
  TokenConsumerImpl(TokenProducerPtr token_producer_ptr);
  virtual ~TokenConsumerImpl();
  virtual auto consume() -> bool override;
  virtual auto get_producer() -> TokenProducerPtr override;
};
} // namespace util
} // namespace kratos
