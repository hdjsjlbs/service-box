#pragma once

#include "util/box_std_allocator.hh"
#include "util/object_pool.hh"

#include <ctime>
#include <functional>
#include <memory>
#include <vector>

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace util {

class BoxFlowManager;

/**
 * 流程返回值.
 */
enum class FlowStatus {
  SUCCESS = 1, ///< 成功
  FAILURE, ///< 失败
  SKIP, ///< 跳过
};

using StartFlowFunc = std::function<FlowStatus()>;
using StopFlowFunc = std::function<FlowStatus()>;
using TickFlowFunc = std::function<FlowStatus(std::time_t now)>;

/**
 * 流程步驟.
 */
class BoxFlow {
  StartFlowFunc start_flow_func_;                       ///< 启动
  StopFlowFunc stop_flow_func_;                         ///< 关闭
  TickFlowFunc tick_flow_func_;                         ///< 循环帧
  BoxFlowManager *manager_ptr_{nullptr};                ///< 管理器
  std::string description_;                             ///< 描述
  FlowStatus last_update_status_{FlowStatus ::SUCCESS}; ///< 上一次更新流程状态

public:
  /**
   * 构造.
   *
   * \param manager 管理器指针
   * \param description 描述
   */
  BoxFlow(BoxFlowManager *manager, const std::string &description);
  /**
   * 析构.
   *
   */
  ~BoxFlow();
  /**
   * 调用启动流程.
   *
   * \return true成功, false失败
   */
  auto on_start() -> FlowStatus;
  /**
   * 调用关闭流程.
   *
   * \return true成功, false失败
   */
  auto on_stop() -> FlowStatus;
  /**
   * 调用循环帧流程.
   *
   * \param now 当前时间戳，毫秒
   * \return
   */
  auto on_update(std::time_t now) -> FlowStatus;
  /**
   * 设置启动流程.
   *
   * \param flow_func 启动流程函数
   * \return 流程引用
   */
  auto start(StartFlowFunc flow_func) -> BoxFlow &;
  /**
   * 设置关闭流程.
   *
   * \param flow_func 关闭流程函数
   * \return 流程引用
   */
  auto stop(StopFlowFunc flow_func) -> BoxFlow &;
  /**
   * 设置循环流程.
   *
   * \param flow_func 循环流程函数
   * \return 流程引用
   */
  auto update(TickFlowFunc flow_func) -> BoxFlow &;
  /**
   * 添加到管理器，作为启动流程步骤.
   *
   * \return 流程引用
   */
  auto add_start() -> BoxFlow &;
  /**
   * 添加到管理器，作为关闭流程步骤.
   *
   * \return 流程引用
   */
  auto add_term() -> BoxFlow &;
  /**
   * 添加到管理器，作为循环流程步骤.
   *
   * \return 流程引用
   */
  auto add_update() -> BoxFlow &;
  /**
   * 获取流程描述.
   *
   * \return 流程描述
   */
  auto get_description() -> const std::string &;
  /**
   * 设置上一次update状态.
   *
   * \param update_status 上一次update状态
   * \return
   */
  auto set_last_update_status(FlowStatus update_status) -> void;
  /**
   * 获取上一次update状态.
   * 
   * \return 上一次update状态
   */
  auto get_last_update_status()->FlowStatus;
};

using BoxFlowPtr = std::shared_ptr<BoxFlow>;

/**
 * 流程管理器.
 */
class BoxFlowManager {
  using FlowPtrVector = service::PoolVector<BoxFlowPtr>;
  using FlowRawPtrVector = service::PoolVector<BoxFlow *>;
  FlowPtrVector all_;            ///< 所有流程，维持引用
  FlowRawPtrVector startup_seq_; ///< 所有启动流程的指针数组
  FlowRawPtrVector term_seq_;    ///< 所有关闭流程的指针数组
  FlowRawPtrVector update_seq_;  ///< 所有更新流程的指针数组

public:
  /**
   * 构造.
   *
   */
  BoxFlowManager();
  /**
   * 析构.
   *
   */
  ~BoxFlowManager();
  /**
   * 添加启动流程步骤.
   *
   * \param module_ptr 流程指针
   * \return 管理器引用
   */
  auto add_start(BoxFlow *module_ptr) -> BoxFlowManager &;
  /**
   * 添加关闭流程步骤.
   *
   * \param module_ptr 流程指针
   * \return 管理器引用
   */
  auto add_term(BoxFlow *module_ptr) -> BoxFlowManager &;
  /**
   * 添加循环流程步骤.
   *
   * \param module_ptr 流程指针
   * \return 管理器引用
   */
  auto add_update(BoxFlow *module_ptr) -> BoxFlowManager &;
  /**
   * 建立新的流程.
   *
   * \param description 例程描述
   * \return 流程共享指针
   */
  auto new_flow(const std::string &description) -> BoxFlowPtr;
  /**
   * 调用启动流程序列.
   *
   * \param box 容器指针
   * \return true成功，false失败
   */
  auto start(service::ServiceBox *box) -> bool;
  /**
   * 调用关闭流程序列.
   *
   * \param box 容器指针
   * \return true成功，false失败
   */
  auto stop(service::ServiceBox *box) -> void;
  /**
   * 调用更新流程序列.
   *
   * \param box 容器指针
   * \return true成功，false失败
   */
  auto update(std::time_t now) -> void;
  /**
   * 打印更新流程.
   *
   * \return
   */
  auto print_update_flow() -> std::string;
};

} // namespace util
} // namespace kratos
