#include "object_pool.hh"
#include "singleton.hh"

namespace kratos {

_BoxObjectPoolManager* get_global_box_object_pool_manager() {
  return Singleton<_BoxObjectPoolManager>::instance();
}

}