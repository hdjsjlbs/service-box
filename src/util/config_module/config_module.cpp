#include "config_module.h"

#include "kconfig/interface/config.h"
#include "kconfig/interface/config_factory.h"
#include "src/kconfig/internal/bool_impl.h"
#include "src/kconfig/internal/domain.h"
#include "src/kconfig/internal/null_impl.h"
#include "src/kconfig/internal/number_impl.h"
#include "src/kconfig/internal/string_impl.h"
#include "json/json.h"

#include <memory>
#include <string>
#include <unordered_map>

std::unique_ptr<kconfig::Config> global_config_ptr;
std::string last_error;
int last_errno{0};

struct ConfigError {
  constexpr static int OK = 0;
  constexpr static int LOAD_FAILED = 1;
  constexpr static int KEY_NOT_FOUND = 2;
  constexpr static int VALUE_TYPE_MISMATCH = 3;
  constexpr static int VALUE_IS_COMPLEX = 4;
  constexpr static int INVALID_ARGUMENT = 5;
  constexpr static int NEED_LOAD_FIRST = 6;
};

int set_error(int error, const char *error_str) {
  last_errno = error;
  last_error = error_str;
  return error;
}

void clear_error() {
  last_errno = ConfigError::OK;
  last_error.clear();
}

DLL_EXPORT_DECL int kconfig_load(const char *path) {
  if (!path) {
    return set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
  }
  global_config_ptr.reset(ConfigFactory::createConfig());
  try {
    global_config_ptr->load("", path);
  } catch (std::exception &ex) {
    return set_error(ConfigError::LOAD_FAILED, ex.what());
  }
  return ConfigError::OK;
}

DLL_EXPORT_DECL const char *kconfig_get_last_error() {
  return last_error.c_str();
}

DLL_EXPORT_DECL int kconfig_get_last_errno() { return last_errno; }

DLL_EXPORT_DECL const char *kconfig_get_error(int error) {
  switch (error) {
  case ConfigError::OK:
    return "Ok";
  case ConfigError::LOAD_FAILED:
    return "Load failed";
  case ConfigError::KEY_NOT_FOUND:
    return "Key not found";
  case ConfigError::VALUE_TYPE_MISMATCH:
    return "Value type mismatch";
  case ConfigError::VALUE_IS_COMPLEX:
    return "Value must be a complex type";
  case ConfigError::INVALID_ARGUMENT:
    return "Invalid argument";
  case ConfigError::NEED_LOAD_FIRST:
    return "Need load config";
  default:
    break;
  }
  return "Unknown";
}

DLL_EXPORT_DECL void kconfig_close() { global_config_ptr.reset(); }

DLL_EXPORT_DECL int kconfig_get_bool(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return 0;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return 0;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return 0;
  }
  if (attr_ptr->isBool()) {
    return attr_ptr->boolean()->get() ? 1 : 0;
  } else {
    return set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
  }
}

DLL_EXPORT_DECL const char *kconfig_get_string(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return nullptr;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return nullptr;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return nullptr;
  }
  if (attr_ptr->isString()) {
    return attr_ptr->string()->get().c_str();
  } else {
    set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
    return nullptr;
  }
}

DLL_EXPORT_DECL int kconfig_get_int(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return 0;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return 0;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return 0;
  }
  if (attr_ptr->isNumber()) {
    try {
      return attr_ptr->number()->getInt();
    } catch (std::exception &) {
      set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
      return 0;
    }
  } else {
    set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
    return 0;
  }
}

DLL_EXPORT_DECL unsigned int kconfig_get_uint(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return 0;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return 0;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return 0;
  }
  if (attr_ptr->isNumber()) {
    try {
      return attr_ptr->number()->getUint();
    } catch (std::exception &) {
      set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
      return 0;
    }
  } else {
    set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
    return 0;
  }
}

DLL_EXPORT_DECL long long kconfig_get_llong(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return 0;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return 0;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return 0;
  }
  if (attr_ptr->isNumber()) {
    try {
      return attr_ptr->number()->getLlong();
    } catch (std::exception &) {
      set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
      return 0;
    }
  } else {
    set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
    return 0;
  }
}

DLL_EXPORT_DECL unsigned long long kconfig_get_ullong(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return 0;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return 0;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return 0;
  }
  if (attr_ptr->isNumber()) {
    try {
      return attr_ptr->number()->getULlong();
    } catch (std::exception &) {
      set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
      return 0;
    }
  } else {
    set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
    return 0;
  }
}

DLL_EXPORT_DECL float kconfig_get_float(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return 0;
  }
  if (!key) {
    set_error(ConfigError::INVALID_ARGUMENT, "Invalid argument");
    return 0;
  }
  auto *attr_ptr = global_config_ptr->get(key);
  if (!attr_ptr) {
    set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
    return 0;
  }
  if (attr_ptr->isNumber()) {
    try {
      return attr_ptr->number()->getFloat();
    } catch (std::exception &) {
      set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
      return 0;
    }
  } else {
    set_error(ConfigError::VALUE_TYPE_MISMATCH, "Value type mismatch");
    return 0;
  }
}

std::unordered_map<std::string, std::string> json_map;

void json_table_append(Json::Value &json_value, const std::string &key,
                       kconfig::Attribute *attr);

Json::Value get_json_object(kconfig::Attribute *attr);

void json_array_append(Json::Value &json_value, kconfig::Attribute *attr) {
  auto type = attr->getType();
  switch (type) {
  case kconfig::Attribute::NUMBER:
    json_value.append(attr->number()->getLlong());
    break;
  case kconfig::Attribute::STRING:
    json_value.append(attr->string()->get());
    break;
  case kconfig::Attribute::BOOL:
    json_value.append(attr->boolean()->get());
    break;
  case kconfig::Attribute::ARRAY: {
    json_value.append(get_json_object(attr));
  } break;
  case kconfig::Attribute::TABLE: {
    json_value.append(get_json_object(attr));
  } break;
  case kconfig::Attribute::ZONE: {
    json_value.append(get_json_object(attr));
  } break;
  default:
    break;
  }
}

void json_table_append(Json::Value &json_value, const std::string &key,
                       kconfig::Attribute *attr) {
  auto type = attr->getType();
  switch (type) {
  case kconfig::Attribute::NUMBER:
    json_value[key] = attr->number()->getLlong();
    break;
  case kconfig::Attribute::STRING:
    json_value[key] = attr->string()->get();
    break;
  case kconfig::Attribute::BOOL:
    json_value[key] = attr->boolean()->get();
    break;
  case kconfig::Attribute::ARRAY: {
    json_value[key] = get_json_object(attr);
  } break;
  case kconfig::Attribute::TABLE: {
    json_value[key] = get_json_object(attr);
  } break;
  case kconfig::Attribute::ZONE: {
    json_value[key] = get_json_object(attr);
  } break;
  default:
    break;
  }
}

Json::Value get_json_object(kconfig::Attribute *attr) {
  auto type = attr->getType();
  switch (type) {
  case kconfig::Attribute::ARRAY: {
    Json::Value json_value(Json::ValueType::arrayValue);
    auto *attr_list = attr->array();
    for (auto i = 0; i < attr_list->getSize(); i++) {
      json_array_append(json_value, attr_list->get(i));
    }
    return json_value;
  } break;
  case kconfig::Attribute::TABLE: {
    Json::Value json_value(Json::ValueType::objectValue);
    auto *attr_dict = attr->table();
    while (attr_dict->hasNext()) {
      auto *pair_ptr = attr_dict->next();
      auto key_str = pair_ptr->key();
      auto *value_ptr = pair_ptr->value();
      json_table_append(json_value, key_str, value_ptr);
    }
    return json_value;
  } break;
  case kconfig::Attribute::ZONE: {
    Json::Value json_value(Json::ValueType::objectValue);
    auto *attr_dict = attr->zone();
    while (attr_dict->hasNext()) {
      auto *zone_attr_ptr = attr_dict->next();
      auto name_str = zone_attr_ptr->name();
      json_table_append(json_value, name_str, zone_attr_ptr);
    }
    return json_value;
  } break;
  default:
    return Json::Value();
    break;
  }
  return Json::Value();
}

DLL_EXPORT_DECL const char *kconfig_get_json(const char *key) {
  clear_error();
  if (!global_config_ptr) {
    set_error(ConfigError::NEED_LOAD_FIRST, "Need load config");
    return nullptr;
  }
  if (key) {
    auto it = json_map.find(key);
    if (it != json_map.end()) {
      return it->second.c_str();
    }
    auto *attr = global_config_ptr->get(key);
    if (!attr) {
      set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
      return nullptr;
    } else {
      auto type = attr->getType();
      switch (type) {
      case kconfig::Attribute::NUMBER:
      case kconfig::Attribute::STRING:
      case kconfig::Attribute::BOOL:
        set_error(ConfigError::VALUE_IS_COMPLEX,
                  "Value must be a complex type");
        return nullptr;
      default:
        break;
      }
      auto json_obj = get_json_object(attr);
      json_map[key] = json_obj.toStyledString();
      return json_map[key].c_str();
    }
  } else {
    auto it = json_map.find("_root_domain");
    if (it != json_map.end()) {
      return it->second.c_str();
    }
    auto *root_domain = global_config_ptr->get_root_domain();
    if (!root_domain) {
      set_error(ConfigError::KEY_NOT_FOUND, "Key not found");
      return nullptr;
    } else {
      auto json_obj = get_json_object(root_domain);
      json_map["_root_domain"] = json_obj.toStyledString();
      return json_map["_root_domain"].c_str();
    }
  }
  return nullptr;
}
