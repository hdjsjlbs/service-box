﻿#include "json_pb_module.h"
#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>
#include <memory>
#include <unordered_set>

#include "util/directory_watcher.hpp"
#include "util/method_type.hh"
#include "util/spsc_queue.hpp"
#include "util/string_util.hh"
#include "util/write_buffer.hh"
#include "json/json.h"

#ifdef GetMessage
#undef GetMessage
#endif // GetMessage

#define ExceWrapper(field, expr)                                               \
  try {                                                                        \
    expr;                                                                      \
  } catch (std::exception & ex) {                                              \
    auto reason = field->name() + ",";                                         \
    throw std::runtime_error(reason + ex.what());                              \
  }

namespace internal {
/**
 * JSON与protobuf::Message之间的转换工具.
 */
class JsonPbBuilder {
  std::unique_ptr<kratos::util::MsgFactory> msg_factory_ptr_; ///< 消息工厂
  kratos::util::WriteBuffer buffer_;                          ///< 写入缓存
  using WatcherPtr = std::unique_ptr<kratos::util::DirectoryWatcher>;
  WatcherPtr watcher_ptr_;            ///< 目录监控
  std::string last_error_{"Success"}; ///< 最后的错误描述

public:
  /**
   * 构造.
   *
   */
  JsonPbBuilder() {}
  /**
   * 析构.
   *
   */
  ~JsonPbBuilder() {}
  /**
   * 加载配置, 如果已存在则重新加载.
   *
   * \param path 目录
   * \return true or false
   */
  auto set_directory(const std::string &path) noexcept -> bool {
    if (msg_factory_ptr_) {
      msg_factory_ptr_.reset();
    }
    msg_factory_ptr_ = std::make_unique<kratos::util::MsgFactory>(nullptr);
    try {
      if (!msg_factory_ptr_->load(path, path)) {
        last_error_ = "Retrive .proto file failed in[" + path + "]";
        return false;
      }
    } catch (std::exception &ex) {
      last_error_ = ex.what();
      return false;
    }
    return true;
  }
  auto to_bytes(const std::string &json_string, ProtobufMessage *pb_msg,
                const kratos::util::MethodType *method_type_ptr) noexcept
      -> bool {
    Json::Value root;
    if (!kratos::util::get_json_root(json_string, root, last_error_)) {
      last_error_ = "Invalid JSON format";
      return false;
    }
    if (!root.isMember("args")) {
      last_error_ = "Invalid JSON format, need 'args'";
      return false;
    }
    auto &arg_node = root["args"];
    auto arg_count = std::size_t(arg_node.size());
    if (arg_count != method_type_ptr->arg_name_vector.size()) {
      last_error_ = "Invalid argument number, need " +
                    std::to_string(method_type_ptr->arg_name_vector.size()) +
                    " but " + std::to_string(arg_count) + "given";
      return false;
    }
    for (const auto &arg_name : method_type_ptr->arg_name_vector) {
      if (arg_node.isMember(arg_name.pb_name)) {
        // JSON内字段存在这个PB参数名字
        continue;
      }
      if (!arg_node.isMember(arg_name.decl_name)) {
        // 没有PB名字也没有形参名
        last_error_ = "Method argument name invalid";
        return false;
      }
      // 处理不存在的情况
      arg_node[arg_name.pb_name] = arg_node[arg_name.decl_name];
      arg_node.removeMember(arg_name.decl_name);
    }
    try {
      if (!encode(arg_node, pb_msg)) {
        return false;
      }
    } catch (std::exception &ex) {
      last_error_ = std::string("Internal error ") + ex.what();
      return false;
    }
    buffer_.clear();
    auto ret = buffer_.apply(pb_msg->ByteSizeLong());
    if (!pb_msg->SerializeToArray(ret.first, (int)ret.second)) {
      last_error_ = "SerializeToArray failed";
      return false;
    }
    return true;
  }
  auto to_json(const ProtobufMessage *pb_msg, std::string &json_string) noexcept
      -> bool {
    try {
      auto json_obj_ptr = decode(*pb_msg);
      if (!json_obj_ptr) {
        return false;
      }
      json_string = json_obj_ptr->toStyledString();
    } catch (std::exception &ex) {
      last_error_ = std::string("Internal error ") + ex.what();
      return false;
    }
    return true;
  }
  auto get_msg_factory() -> kratos::util::MsgFactory * {
    return msg_factory_ptr_.get();
  }
  auto get_buffer() -> kratos::util::WriteBuffer * { return &buffer_; }
  auto get_last_error_string() -> const std::string & { return last_error_; }
  auto set_last_error_string(const std::string &error) -> void {
    last_error_ = error;
  }

private:
  auto decode(const ProtobufMessage &pb_msg) -> std::unique_ptr<Json::Value> {
    const auto *descriptor = pb_msg.GetDescriptor();
    const auto *reflection = pb_msg.GetReflection();
    auto root_ptr = std::make_unique<Json::Value>(Json::ValueType::objectValue);
    // 遍历pb_msg字段
    for (int i = 0; i < descriptor->field_count(); ++i) {
      const auto *field = descriptor->field(i);
      if (!field->is_repeated() && !reflection->HasField(pb_msg, field)) {
        //
        // 设置默认字段值
        //
        Json::Value default_value;
        auto type = field->cpp_type();
        switch (type) {
        case ProtobufFieldDescriptor::CPPTYPE_INT32:
        case ProtobufFieldDescriptor::CPPTYPE_UINT32:
          default_value = 0;
          break;
        case ProtobufFieldDescriptor::CPPTYPE_INT64:
        case ProtobufFieldDescriptor::CPPTYPE_UINT64:
          default_value = 0;
          break;
        case ProtobufFieldDescriptor::CPPTYPE_BOOL:
          default_value = false;
          break;
        case ProtobufFieldDescriptor::CPPTYPE_FLOAT:
        case ProtobufFieldDescriptor::CPPTYPE_DOUBLE:
          default_value = .0f;
          break;
        case ProtobufFieldDescriptor::CPPTYPE_STRING:
          default_value = "";
          break;
        case ProtobufFieldDescriptor::CPPTYPE_ENUM:
          default_value = 0;
          break;
        default:
          break;
        }
        if (!default_value.isNull()) {
          (*root_ptr)[field->name()] = default_value;
        }
      } else {
        auto obj_ptr = on_field(pb_msg, field);
        if (obj_ptr) {
          (*root_ptr)[field->name()] = *obj_ptr;
        }
      }
    }
    return root_ptr;
  }

  auto on_field(const ProtobufMessage &msg,
                const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value> {
    if (field->is_repeated()) {
      //
      // repeated/map字段
      //
      if (field->is_map()) {
        //
        // map字段
        //
        return on_map_field(msg, field);
      } else {
        //
        // repeated字段
        //
        return on_repeated_field(msg, field);
      }
    } else {
      switch (field->type()) {
      case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetBool(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT32: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetInt32(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT64: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetInt64(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetUInt32(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetUInt64(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetFloat(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetDouble(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_STRING: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetString(msg, field).c_str());
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
        return decode(msg.GetReflection()->GetMessage(msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
        return std::make_unique<Json::Value>(
            msg.GetReflection()->GetEnumValue(msg, field));
        break;
      }
      default:
        break;
      }
    }
    return nullptr;
  }

  auto on_map_field(const ProtobufMessage &msg,
                    const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value> {
    auto dict_obj_ptr =
        std::make_unique<Json::Value>(Json::ValueType::objectValue);
    auto count = msg.GetReflection()->FieldSize(msg, field);
    for (int i = 0; i < count; i++) {
      const auto &message_map =
          msg.GetReflection()->GetRepeatedMessage(msg, field, i);
      const auto *descriptor = message_map.GetDescriptor();
      const auto *key_field_descriptor = descriptor->field(0);
      const auto *value_field_descriptor = descriptor->field(1);
      auto key_obj_ptr = on_map_field_key(message_map, key_field_descriptor);
      auto value_obj_ptr =
          on_map_field_value(message_map, value_field_descriptor);
      // set map key, value
      (*dict_obj_ptr)[key_obj_ptr->asString()] = *value_obj_ptr;
    }
    return dict_obj_ptr;
  }

  auto on_map_field_key(const ProtobufMessage &msg,
                        const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value> {
    auto key_type = field->type();
    switch (key_type) {
    case ProtobufFieldDescriptor::Type::TYPE_INT32: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetInt32(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT64: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetString(msg, field).c_str());
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetUInt32(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetUInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetEnumValue(msg, field));
      break;
    }
    default:
      break;
    }
    return nullptr;
  }

  auto on_map_field_value(const ProtobufMessage &msg,
                          const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value> {
    auto value_type = field->type();
    switch (value_type) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetBool(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT32: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetInt32(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT64: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetUInt32(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetUInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetFloat(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetDouble(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetString(msg, field).c_str());
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      return decode(msg.GetReflection()->GetMessage(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetEnumValue(msg, field));
      break;
    }
    default:
      break;
    }
    return nullptr;
  }

  auto on_repeated_field(const ProtobufMessage &msg,
                         const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value> {
    auto count = msg.GetReflection()->FieldSize(msg, field);
    auto list_obj_ptr =
        std::make_unique<Json::Value>(Json::ValueType::arrayValue);
    if (field->type() == ProtobufFieldDescriptor::Type::TYPE_MESSAGE) {
      for (auto i = 0; i < count; i++) {
        const auto &repeated_message =
            msg.GetReflection()->GetRepeatedMessage(msg, field, i);
        auto msg_obj_ptr = decode(repeated_message);
        list_obj_ptr->append(*msg_obj_ptr);
      }
    } else {
      for (int i = 0; i < count; i++) {
        switch (field->type()) {
        case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
          list_obj_ptr->append(
              msg.GetReflection()->GetRepeatedBool(msg, field, i));
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_INT32: {
          list_obj_ptr->append(
              msg.GetReflection()->GetRepeatedInt32(msg, field, i));
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_INT64: {
          list_obj_ptr->append(
              msg.GetReflection()->GetRepeatedInt64(msg, field, i));
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
          auto value = msg.GetReflection()->GetRepeatedUInt32(msg, field, i);
          list_obj_ptr->append(value);
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
          auto value = msg.GetReflection()->GetRepeatedUInt64(msg, field, i);
          list_obj_ptr->append(value);
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
          auto value = msg.GetReflection()->GetRepeatedFloat(msg, field, i);
          list_obj_ptr->append(value);
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
          auto value = msg.GetReflection()->GetRepeatedDouble(msg, field, i);
          list_obj_ptr->append(value);
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
          auto value = msg.GetReflection()->GetRepeatedEnum(msg, field, i);
          auto num = value->number();
          list_obj_ptr->append(num);
          break;
        }
        case ProtobufFieldDescriptor::Type::TYPE_STRING: {
          auto value = msg.GetReflection()->GetRepeatedString(msg, field, i);
          list_obj_ptr->append(value);
          break;
        }
        default:
          break;
        }
      }
    }
    return list_obj_ptr;
  }

  auto on_object_value(const Json::Value &obj_ptr, ProtobufMessage &msg,
                       const ProtobufReflection *ref,
                       const ProtobufFieldDescriptor *field) -> void {
    if (field->is_repeated()) {
      if (field->is_map()) {
        on_object_map(obj_ptr, msg, ref, field);
      } else {
        on_object_repeated_value(obj_ptr, msg, ref, field);
      }
    } else {
      switch (field->type()) {
      case ProtobufFieldDescriptor::Type::TYPE_BOOL:
        ExceWrapper(field, ref->SetBool(&msg, field, obj_ptr.asBool()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_INT32:
        ExceWrapper(field, ref->SetInt32(&msg, field, obj_ptr.asInt()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_INT64:
        ExceWrapper(field, ref->SetInt64(&msg, field, obj_ptr.asInt64()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_UINT32:
        ExceWrapper(field, ref->SetUInt32(&msg, field, obj_ptr.asUInt()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_UINT64:
        ExceWrapper(field, ref->SetUInt64(&msg, field, obj_ptr.asUInt64()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
        ExceWrapper(field, ref->SetFloat(&msg, field, obj_ptr.asFloat()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
        ExceWrapper(field, ref->SetDouble(&msg, field, obj_ptr.asDouble()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_STRING: {
        ExceWrapper(field, ref->SetString(&msg, field, obj_ptr.asString()));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
        encode(obj_ptr, ref->MutableMessage(&msg, field));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
        ExceWrapper(field, ref->SetEnumValue(&msg, field, obj_ptr.asInt()));
        break;
      }
      default:
        break;
      }
    }
  }

  auto on_object_map(const Json::Value &obj_ptr, ProtobufMessage &msg,
                     const ProtobufReflection *ref,
                     const ProtobufFieldDescriptor *field) -> void {
    for (const auto &key : obj_ptr.getMemberNames()) {
      auto *pair_message = ref->AddMessage(&msg, field);
      auto *pair_reflection = pair_message->GetReflection();
      const auto *key_descriptor = field->message_type()->field(0);
      const auto *value_descriptor = field->message_type()->field(1);
      auto value_obj = obj_ptr[key];
      auto key_type = key_descriptor->type();
      switch (key_type) {
      case ProtobufFieldDescriptor::Type::TYPE_INT32: {
        pair_reflection->SetInt32(pair_message, key_descriptor, std::stoi(key));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT64: {
        pair_reflection->SetInt64(pair_message, key_descriptor,
                                  std::stoll(key));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_STRING: {
        pair_reflection->SetString(pair_message, key_descriptor, key);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
        pair_reflection->SetUInt32(pair_message, key_descriptor,
                                   std::stoul(key));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
        pair_reflection->SetUInt64(pair_message, key_descriptor,
                                   std::stoull(key));
        break;
      }
      default:
        break;
      }
      auto value_type = value_descriptor->type();
      switch (value_type) {
      case ProtobufFieldDescriptor::Type::TYPE_BOOL:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetBool(pair_message, value_descriptor,
                                             value_obj.asBool()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_INT32:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetInt32(pair_message, value_descriptor,
                                              value_obj.asInt()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_INT64:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetInt64(pair_message, value_descriptor,
                                              value_obj.asInt64()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_UINT32:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetUInt32(pair_message, value_descriptor,
                                               value_obj.asUInt()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_UINT64:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetUInt64(pair_message, value_descriptor,
                                               value_obj.asUInt64()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetFloat(pair_message, value_descriptor,
                                              value_obj.asFloat()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetDouble(pair_message, value_descriptor,
                                               value_obj.asDouble()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_STRING:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetString(pair_message, value_descriptor,
                                               value_obj.asString()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
        auto *new_message =
            pair_reflection->MutableMessage(pair_message, value_descriptor);
        encode(value_obj, new_message);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_ENUM:
        ExceWrapper(value_descriptor,
                    pair_reflection->SetEnumValue(
                        pair_message, value_descriptor, value_obj.asInt()));
        break;
      default:
        break;
      }
    }
  }

  auto on_object_repeated_value(const Json::Value &obj_ptr,
                                ProtobufMessage &msg,
                                const ProtobufReflection *ref,
                                const ProtobufFieldDescriptor *field) -> void {
    for (auto &v : obj_ptr) {
      switch (field->type()) {
      case ProtobufFieldDescriptor::Type::TYPE_BOOL:
        ExceWrapper(field, ref->AddBool(&msg, field, v.asBool()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_INT32:
        ExceWrapper(field, ref->AddInt32(&msg, field, v.asInt()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_INT64:
        ExceWrapper(field, ref->AddInt64(&msg, field, v.asInt64()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_UINT32:
        ExceWrapper(field, ref->AddUInt32(&msg, field, v.asUInt()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_UINT64:
        ExceWrapper(field, ref->AddUInt64(&msg, field, v.asUInt64()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
        ExceWrapper(field, ref->AddFloat(&msg, field, v.asFloat()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
        ExceWrapper(field, ref->AddDouble(&msg, field, v.asDouble()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_STRING:
        ExceWrapper(field, ref->AddString(&msg, field, v.asString()));
        break;
      case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
        auto *new_message = ref->AddMessage(&msg, field);
        encode(v, new_message);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
        ExceWrapper(field, ref->AddEnumValue(&msg, field, v.asInt()));
        break;
      }
      default:
        break;
      }
    }
  }

  auto encode(const Json::Value &value, ProtobufMessage *msg) -> bool {
    const auto *descriptor = msg->GetDescriptor();
    const auto *reflection = msg->GetReflection();
    for (auto &key : value.getMemberNames()) {
      const auto *field = descriptor->FindFieldByName(key);
      if (field) {
        on_object_value(value[key], *msg, reflection, field);
      }
    }
    return true;
  }
};

} // namespace internal

using ObjectPtrVector = std::vector<std::unique_ptr<internal::JsonPbBuilder>>;
static ObjectPtrVector obj_ptr_vec;

auto check_jpb(int jpb) -> bool {
  if (jpb > (int)obj_ptr_vec.size() || jpb <= 0) {
    return false;
  }
  return (obj_ptr_vec[std::size_t(jpb) - 1] != nullptr);
}

FuncExport int json_pb_init() {
  obj_ptr_vec.emplace_back(std::make_unique<internal::JsonPbBuilder>());
  return (int)obj_ptr_vec.size();
}

FuncExport int json_pb_set_directory(int jpb, const char *path) {
  if (!check_jpb(jpb)) {
    return -1;
  }
  auto &pb_ptr = obj_ptr_vec[std::size_t(jpb) - 1];
  return pb_ptr->set_directory(path);
}

FuncExport const char *json_pb_to_pb_bytes(int jpb, const char *service_name,
                                           const char *method_name,
                                           const char *json_args) {
  if (!check_jpb(jpb)) {
    return nullptr;
  }
  auto &pb_ptr = obj_ptr_vec[std::size_t(jpb) - 1];
  auto *msg_factory_ptr = pb_ptr->get_msg_factory();
  auto method_info =
      msg_factory_ptr->get_method_info(service_name, method_name);
  if (!method_info.first || !method_info.second) {
    pb_ptr->set_last_error_string("Service or method not found");
    return nullptr;
  }
  auto *method_type_ptr =
      msg_factory_ptr->get_type(method_info.first, method_info.second);
  if (!method_type_ptr) {
    pb_ptr->set_last_error_string("Service or method not found");
    return nullptr;
  }
  auto *pb_msg_ptr = msg_factory_ptr->new_call_param(service_name, method_name);
  if (!pb_msg_ptr) {
    pb_ptr->set_last_error_string("Service or method not found");
    return nullptr;
  }
  if (!pb_ptr->to_bytes(json_args, pb_msg_ptr, method_type_ptr)) {
    return nullptr;
  }
  return pb_ptr->get_buffer()->get();
}

FuncExport int json_pb_get_pb_bytes_length(int jpb) {
  if (!check_jpb(jpb)) {
    return -1;
  }
  auto &pb_ptr = obj_ptr_vec[std::size_t(jpb) - 1];
  return (int)pb_ptr->get_buffer()->length();
}

FuncExport const char *json_pb_to_json_string(int jpb, const char *service_name,
                                              const char *method_name,
                                              const char *pb_bytes,
                                              int length) {
  if (!check_jpb(jpb)) {
    return nullptr;
  }
  auto &pb_ptr = obj_ptr_vec[std::size_t(jpb) - 1];
  auto *msg_factory_ptr = pb_ptr->get_msg_factory();

  auto *pb_msg_ptr =
      msg_factory_ptr->new_call_return(service_name, method_name);
  if (!pb_msg_ptr->ParseFromArray(pb_bytes, length)) {
    return nullptr;
  }
  std::string json_string;
  if (!pb_ptr->to_json(pb_msg_ptr, json_string)) {
    return nullptr;
  }
  pb_ptr->get_buffer()->clear();
  auto ret = pb_ptr->get_buffer()->apply(json_string.size() + 1);
  memcpy(ret.first, json_string.c_str(), ret.second);
  return ret.first;
}

FuncExport const char *json_pb_get_error_string(int jpb) {
  if (!check_jpb(jpb)) {
    return nullptr;
  }
  auto &pb_ptr = obj_ptr_vec[std::size_t(jpb) - 1];
  return pb_ptr->get_last_error_string().c_str();
}

FuncExport int json_pb_deinit(int jpb) {
  if (!check_jpb(jpb)) {
    return -1;
  }
  obj_ptr_vec[std::size_t(jpb) - 1] = nullptr;
  return 0;
}
