﻿#include "python_helper.hh"

#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>

#include "box/box_channel.hh"
#include "detail/box_config_impl.hh"

#ifndef PYTHON_SDK
#include "call_tracer/call_tracer_util.hh"
#include "detail/rpc_probe_impl.hh"
#include "detail/service_layer.hh"
#include "detail/zookeeper/service_finder_zookeeper.hh"
#include "detail/zookeeper/service_register_zookeeper.hh"
#endif // PYTHON_SDK

#include "util/os_util.hh"
#include "util/string_util.hh"
#include "util/time_util.hh"
#include "json/json.h"

#include <filesystem>
#include <memory>
#include <vector>

#include <iostream>
#include <stdexcept>

#ifdef GetMessage
#undef GetMessage
#endif

namespace kratos {
namespace util {

namespace python {

using ProtobufMessagePtr = std::shared_ptr<ProtobufMessage>;
using ProtobufErrorCollector =
    google::protobuf::compiler::MultiFileErrorCollector;

template <typename T> inline auto PyCast(PyObject *obj_ptr) -> T;

template <>
inline auto PyCast<const char *>(PyObject *obj_ptr) -> const char * {

#if PY_MAJOR_VERSION >= 3
  if (!PyUnicode_Check(obj_ptr)) {
    return "";
  }
#else
  if (!PyUnicode_Check(obj_ptr) && !PyString_Check(obj_ptr)) {
    return "";
  }
#endif // PY_MAJOR_VERSION >= 3

#if PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 3
  return PyUnicode_AsUTF8(obj_ptr);
#else
  if (PyString_Check(obj_ptr)) {
    return PyString_AS_STRING(obj_ptr);
  } else {
    // do not support other encode type
    return "";
  }
#endif // PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 3
}

template <>
inline auto PyCast<std::int32_t>(PyObject *obj_ptr) -> std::int32_t {
  if (!PyNumber_Check(obj_ptr)) {
    return std::int32_t(0);
  }
  auto *tmp_obj = PyNumber_Long(obj_ptr);
  if (!tmp_obj) {
    return std::int32_t(0);
  }
  return std::int32_t(PyLong_AsLong(tmp_obj));
}

template <>
inline auto PyCast<std::uint32_t>(PyObject *obj_ptr) -> std::uint32_t {
  if (!PyNumber_Check(obj_ptr)) {
    return std::uint32_t(0);
  }
  auto *tmp_obj = PyNumber_Long(obj_ptr);
  if (!tmp_obj) {
    return std::uint32_t(0);
  }
  return std::uint32_t(PyLong_AsUnsignedLong(tmp_obj));
}

template <>
inline auto PyCast<std::int64_t>(PyObject *obj_ptr) -> std::int64_t {
  if (!PyNumber_Check(obj_ptr)) {
    return std::int64_t(0);
  }
  auto *tmp_obj = PyNumber_Long(obj_ptr);
  if (!tmp_obj) {
    return std::int64_t(0);
  }
  return std::int64_t(PyLong_AsLongLong(tmp_obj));
}

template <>
inline auto PyCast<std::uint64_t>(PyObject *obj_ptr) -> std::uint64_t {
  if (!PyNumber_Check(obj_ptr)) {
    return std::uint64_t(0);
  }
  auto *tmp_obj = PyNumber_Long(obj_ptr);
  if (!tmp_obj) {
    return std::uint64_t(0);
  }
  return std::uint64_t(PyLong_AsUnsignedLongLong(tmp_obj));
}

template <> inline auto PyCast<float>(PyObject *obj_ptr) -> float {
  if (PyLong_Check(obj_ptr)) {
    return (float)PyLong_AsLong(obj_ptr);
  }
  if (!PyFloat_Check(obj_ptr)) {
    return float(.0f);
  }
  return float(PyFloat_AsDouble(obj_ptr));
}

template <> inline auto PyCast<double>(PyObject *obj_ptr) -> double {
  if (PyLong_Check(obj_ptr)) {
    return (double)PyLong_AsLongLong(obj_ptr);
  }
  if (!PyFloat_Check(obj_ptr)) {
    return double(.0f);
  }
  return PyFloat_AsDouble(obj_ptr);
}

/**
 * @brief pb错误收集类
 */
class MsgErrorCollector : public ProtobufErrorCollector {
public:
  MsgErrorCollector() {}
  virtual ~MsgErrorCollector(){};
  /**
   * @brief  记录一条Error
   */
  virtual void AddError(const std::string &filename, int line, int column,
                        const std::string &message);
  /**
   * @brief 记录一条Warning
   */
  virtual void AddWarning(const std::string &filename, int line, int column,
                          const std::string &message);
  /**
   * @brief 收集错误，转化为字符串
   *
   * @return const std::string&
   */
  const std::string PopError();

private:
  std::stringstream ss; ///< 错误汇总输出流
};

void MsgErrorCollector::AddError(const std::string &filename, int line,
                                 int column, const std::string &message) {
  ss << "[Error] " << filename << ":" << line << ":" << column << " " << message
     << std::endl;
}

void MsgErrorCollector::AddWarning(const std::string &filename, int line,
                                   int column, const std::string &message) {
  ss << "[Warn] " << filename << ":" << line << ":" << column << " " << message
     << std::endl;
}

const std::string MsgErrorCollector::PopError() {
  //格式化错误
  std::string msg = ss.str();
  //重置
  ss.clear();
  return msg;
}

/**
 * 方法调用原型
 */
struct MethodType {
  const ProtobufDescriptor *request_message_descriptor{nullptr}; ///< 调用参数
  const ProtobufDescriptor *response_message_descriptor{nullptr}; ///< 返回值
  bool oneway{false};                              ///< 是否是oneway方法
  std::string service_name;                        ///< 服务名
  std::string method_name;                         ///< 方法名
  int timeout;                                     ///< 调用的timeout
  ProtobufMessagePtr request_message{nullptr};     ///< 参数
  ProtobufMessagePtr response_message{nullptr};    ///< 返回值
  rpc::MethodID method_id{rpc::INVALID_METHOD_ID}; ///< 方法ID
  MethodType(const ProtobufDescriptor *request_descriptor,
             const ProtobufDescriptor *response_descriptor, bool is_oneway,
             const std::string &service_name,
             const std::string &service_method_name, int new_call_timeout_pack,
             ProtobufMessage *request, ProtobufMessage *response,
             rpc::MethodID method_id) noexcept;
  MethodType(const MethodType &rht) noexcept;
  MethodType(MethodType &&rht) noexcept;
  ~MethodType() noexcept;
  const MethodType &operator=(const MethodType &) = delete;
  /**
   * 有效性检测
   * \return true或false
   */
  operator bool() { return !method_name.empty(); }
};

MethodType::MethodType(const ProtobufDescriptor *request_descriptor,
                       const ProtobufDescriptor *response_descriptor,
                       bool is_oneway, const std::string &service_name,
                       const std::string &service_method_name,
                       int new_call_timeout_pack, ProtobufMessage *request,
                       ProtobufMessage *response, rpc::MethodID mid) noexcept
    : request_message_descriptor(request_descriptor),
      response_message_descriptor(response_descriptor), oneway(is_oneway),
      service_name(service_name), method_name(service_method_name),
      timeout(new_call_timeout_pack), request_message(request),
      response_message(response), method_id(mid) {}

MethodType::MethodType(const MethodType &rht) noexcept
    : request_message_descriptor(rht.request_message_descriptor),
      response_message_descriptor(rht.response_message_descriptor),
      oneway(rht.oneway), service_name(rht.service_name),
      method_name(rht.method_name), timeout(rht.timeout),
      request_message(rht.request_message),
      response_message(rht.response_message), method_id(rht.method_id) {}

MethodType::MethodType(MethodType &&rht) noexcept
    : request_message_descriptor(rht.request_message_descriptor),
      response_message_descriptor(rht.response_message_descriptor),
      oneway(rht.oneway), service_name(std::move(rht.service_name)),
      method_name(std::move(rht.method_name)), timeout(rht.timeout),
      request_message(std::move(rht.request_message)),
      response_message(std::move(rht.response_message)),
      method_id(rht.method_id) {}

MethodType::~MethodType(){
  if(request_message_descriptor){
    request_message_descriptor = nullptr;
  }
  if (response_message_descriptor){
    response_message_descriptor = nullptr;
  }

  if (request_message){
    request_message.reset();
  }

  if (response_message) {
    response_message.reset();
  }
}

/**
 * 协议工厂
 */
class MsgFactory {
  using MessageFactoryVector = std::vector<MethodType>;
  std::unique_ptr<ProtobufDynamicMessageFactory> factory_{
      nullptr}; ///< 协议动态工厂
  using MessageFactoryMap =
      std::unordered_map<rpc::ServiceUUID, MessageFactoryVector>;
  MessageFactoryMap msg_factory_map_;                   ///< 协议工厂
  std::unique_ptr<ProtobufImporter> importer_{nullptr}; ///< 加载器

public:
  /**
   * 构造
   */
  MsgFactory();
  /**
   * 析构
   */
  ~MsgFactory();
  /**
   * 加载
   *
   * \param idl_json_file IDL JSON文件
   * \param idl_proto_root_dir IDL所对应的.proto文件的根目录
   * \param file_name 服务所在的.proto文件名
   * \return true或false
   */
  auto load(const std::string &idl_json_file,
            const std::string &idl_proto_root_dir, const std::string &file_name)
      -> bool;
  /**
   * 加载idl_proto_root_dir内所有的.proto文件
   *
   * \param idl_json_root_dir IDL JSON文件根目录
   * \param idl_proto_root_dir IDL所对应的.proto文件的根目录
   * \return true或false
   */
  auto load(const std::string &idl_json_root_dir,
            const std::string &idl_proto_root_dir) -> bool;

  /**
  *  卸载消息工厂中的对应资源
  */
  auto deinit() -> void;

  /**
   * 建立方法的调用参数
   *
   * \param service_uuid 服务UUID
   * \param method_id 方法ID
   * \return 调用参数类
   */
  auto new_call_param(rpc::ServiceUUID service_uuid,
                      rpc::MethodID method_id) noexcept -> ProtobufMessage *;
  /**
   * 建立方法的调用返回
   *
   * \param service_uuid 服务UUID
   * \param method_id 方法ID
   * \return 调用返回类
   */
  auto new_call_return(rpc::ServiceUUID service_uuid,
                       rpc::MethodID method_id) noexcept -> ProtobufMessage *;
  /**
   * 获取调用类型
   *
   * \param service_uuid 服务UUID
   * \param method_id 方法ID
   * \return 调用类型
   */
  auto get_type(rpc::ServiceUUID service_uuid,
                rpc::MethodID method_id) const noexcept -> const MethodType *;

  auto get_type(rpc::ServiceUUID service_uuid,
                const std::string &method_name) const noexcept
      -> const MethodType *;

private:
  /**
   * 加载
   *
   * \param idl_json_root JSON根节点
   * \param file_descriptor FileDescriptor
   * \return true或false
   */
  auto load(const Json::Value &idl_json_root,
            const ProtobufFileDescriptor *file_descriptor) -> bool;
};

static python::MsgFactory global_msg_factory;

MsgFactory::MsgFactory() {
  //factory_ = std::make_unique<ProtobufDynamicMessageFactory>();
}

MsgFactory::~MsgFactory() {
  msg_factory_map_.clear();
  factory_.reset();
  importer_.reset();
}

auto MsgFactory::load(const std::string &idl_json_file,
                      const std::string &idl_proto_root_dir,
                      const std::string &file_name) -> bool {
  Json::Value root;
  std::string error;

  //
  // 初始化factory
  //
  if (!factory_){
    factory_ = std::make_unique<ProtobufDynamicMessageFactory>();
  }

  //
  // 获取JSON对象
  //
  auto ret = util::get_json_root_from_file(idl_json_file, root, error);
  if (!ret) {
    return false;
  }
  ProtobufDiskSourceTree source_tree;
  MsgErrorCollector err_collector;
  //
  // 设置source tree的路径
  //
  source_tree.MapPath("proto", idl_proto_root_dir);
  if (importer_) {
    //
    //  清理上次使用残余的import，清理失效的source_tree 与 err_collector
    //
    importer_.reset();
  }
  importer_ = std::make_unique<ProtobufImporter>(&source_tree, &err_collector);
  //
  // 导入文件描述
  //
  const auto *file_descriptor = importer_->Import("proto/" + file_name);
  if (!file_descriptor) {
    return false;
  }
  return load(root, file_descriptor);
}

auto MsgFactory::load(const std::string &idl_json_root_dir,
                      const std::string &idl_proto_root_dir) -> bool {

  //
  // 初始化factory
  //
  if (!factory_){
    factory_ = std::make_unique<ProtobufDynamicMessageFactory>();
  }

  //
  // 获取所有的JSON文件
  //
  std::vector<std::string> proto_file_names;
  if (!util::get_file_in_directory(idl_proto_root_dir, ".proto",
                                   proto_file_names)) {
    PyErr_SetString(PyExc_RuntimeError, "No .proto found");
    return false;
  }
  ProtobufDiskSourceTree source_tree;
  MsgErrorCollector err_collector;
  //
  // 设置source tree的路径
  //
  source_tree.MapPath("proto", idl_proto_root_dir);
  if (importer_) {
    //
    //  清理上次使用残余的import，清理失效的source_tree 与 err_collector
    //
    importer_.reset();
  }
  importer_ = std::make_unique<ProtobufImporter>(&source_tree, &err_collector);
  for (const auto &file_name : proto_file_names) {
    //
    // 没有扩展名的文件名
    //
    auto file_name_no_ext =
        std::filesystem::path(file_name).stem().stem().string();
    //
    // source tree虚拟文件路径
    //
    auto virtual_proto_file_path =
        "proto/" + std::filesystem::path(file_name).filename().string();
    const auto *descriptor = importer_->Import(virtual_proto_file_path);
    if (!descriptor) {
      const auto msg_str = err_collector.PopError();
      PyErr_SetString(PyExc_RuntimeError, msg_str.c_str());
      return false;
    }
    std::string json_file_path = util::complete_path(
        idl_json_root_dir, file_name_no_ext + ".idl.protobuf.json");
    Json::Value root;
    std::string error;
    auto ret = util::get_json_root_from_file(json_file_path, root, error);
    if (!ret) {
      PyErr_SetString(PyExc_RuntimeError, "No .json found");
      return false;
    }
    if (!load(root, descriptor)) {
      return false;
    }
  }
  return true;
}

auto MsgFactory::load(const Json::Value &idl_json_root,
                      const ProtobufFileDescriptor *file_descriptor) -> bool {
  if (!file_descriptor) {
    return false;
  }
  for (const auto &service : idl_json_root["services"]) {
    //
    // 校验
    //
    if (!service.isMember("name") || !service.isMember("methods")) {
      continue;
    }
    auto service_name = service["name"].asString();
    for (const auto &method : service["methods"]) {
      //
      // PB内message的名字, 对应方法的参数, {service name}_{method name}_args
      //
      auto arg_name = file_descriptor->package() + "." +
                      service["name"].asString() + "_" +
                      method["name"].asString() + "_args";
      //
      // PB内message的名字, 对应方法的返回值, {service name}_{method name}_ret
      //
      auto ret_name = file_descriptor->package() + "." +
                      service["name"].asString() + "_" +
                      method["name"].asString() + "_ret";
      const auto *req_desc = importer_->pool()->FindMessageTypeByName(arg_name);
      const auto *ack_desc = importer_->pool()->FindMessageTypeByName(ret_name);
      auto oneway = method.isMember("oneway");
      ProtobufMessage *arg_msg = nullptr;
      ProtobufMessage *ret_msg = nullptr;
      //
      // 方法的参数和返回值对象只建立一个
      //
      if (req_desc) {
        const auto *arg_type = factory_->GetPrototype(req_desc);
        if (arg_type) {
          arg_msg = arg_type->New();
        }
      }
      if (ack_desc) {
        const auto *ret_type = factory_->GetPrototype(ack_desc);
        if (ret_type) {
          ret_msg = ret_type->New();
        }
      }
      auto method_name = method["name"].asString();
      auto timeout = method["timeout"].asInt();
      auto service_uuid_str = service["uuid"].asString();
      auto service_uuid = std::stoull(service_uuid_str);
      auto method_id = method["index"].asInt();
      MethodType method_call{
          req_desc,     ack_desc,    oneway,
          service_name, method_name, timeout,
          arg_msg,      ret_msg,     rpc::MethodID(method_id)};
      //
      // 放入消息表, 后续用于快速获取
      //
      msg_factory_map_[service_uuid].emplace_back(std::move(method_call));
    }
  }
  return true;
}

auto MsgFactory::deinit()->void {
    msg_factory_map_.clear();
    // pb 相关资源必须后销毁，否则会有崩溃，动态消息中存在对其的引用
    factory_.reset();
    importer_.reset();
}

auto MsgFactory::new_call_param(rpc::ServiceUUID service_uuid,
                                rpc::MethodID method_id) noexcept
    -> ProtobufMessage * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  const auto &methods = it->second;
  if ((method_id > methods.size()) || (method_id == 0)) {
    return nullptr;
  }
  auto &req_msg_ptr = methods[method_id - 1].request_message;
  if (!req_msg_ptr) {
    return nullptr;
  }
  //
  // 清理后返回, 复用
  //
  req_msg_ptr->Clear();
  return req_msg_ptr.get();
}

auto MsgFactory::new_call_return(rpc::ServiceUUID service_uuid,
                                 rpc::MethodID method_id) noexcept
    -> ProtobufMessage * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  const auto &methods = it->second;
  if ((method_id > methods.size()) || (method_id == 0)) {
    return nullptr;
  }
  auto &rep_msg_ptr = methods[method_id - 1].response_message;
  if (!rep_msg_ptr) {
    return nullptr;
  }
  //
  // 清理后返回, 复用
  //
  rep_msg_ptr->Clear();
  return rep_msg_ptr.get();
}

auto MsgFactory::get_type(rpc::ServiceUUID service_uuid,
                          rpc::MethodID method_id) const noexcept
    -> const MethodType * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  const auto &methods = it->second;
  if ((method_id > methods.size()) || (method_id == 0)) {
    return nullptr;
  }
  return &methods[method_id - 1];
}

auto MsgFactory::get_type(rpc::ServiceUUID service_uuid,
                          const std::string &method_name) const noexcept
    -> const MethodType * {
  auto it = msg_factory_map_.find(service_uuid);
  if (it == msg_factory_map_.end()) {
    return nullptr;
  }
  for (const auto &method : it->second) {
    if (method_name == method.method_name) {
      return &method;
    }
  }
  return nullptr;
}

} // namespace python

/**
 * 设置对象属性.
 *
 * \param runtime CppRuntime
 * \param obj_ptr 对象实例
 * \param name 属性名
 * \param value_ptr 属性值
 * \return
 */
inline static auto SET_ATTR_STRING_XDECREF(CppRuntime *runtime,
                                           PyObject *obj_ptr,
                                           const std::string &name,
                                           PyObject *value_ptr) -> void {
#if PY_MAJOR_VERSION >= 3
  auto *name_obj = PyUnicode_FromString(name.c_str());
#else
  auto *name_obj = PyString_FromString(name.c_str());
#endif // PY_MAJOR_VERSION >= 3       
  if (PyDict_SetItem(obj_ptr, name_obj, value_ptr)) {
    runtime->write_error_log("PyDict_SetItem failed, key[" + name + "]");
    PyErr_Clear();
  }
  if (!PyBool_Check(value_ptr) && (value_ptr != Py_None)) {
    Py_XDECREF(value_ptr);
  }
  Py_XDECREF(name_obj);
}
/**
 * 添加List元素.
 *
 * \param runtime CppRuntime
 * \param list list对象
 * \param value_ptr 元素
 * \return
 */
inline static auto LIST_APPEND_XDECREF(CppRuntime *runtime, PyObject *list,
                                       PyObject *value_ptr) -> void {
  if (PyList_Append(list, value_ptr)) {
    runtime->write_error_log("PyList_Append failed");
    PyErr_Clear();
  }
  if (!PyBool_Check(value_ptr) && (value_ptr != Py_None)) {
    Py_XDECREF(value_ptr);
  }
}

ProtobufCoder::ProtobufCoder(CppRuntime *runtime) { runtime_ = runtime; }

ProtobufCoder::~ProtobufCoder() {}

auto ProtobufCoder::decode(const ProtobufMessage &pb_msg) -> PyObject * {
  auto *dict_ptr = PyDict_New();
  if (!dict_ptr) {
    write_error_log("PyDict_New failed");
    Py_RETURN_NONE;
  }
  const auto *descriptor = pb_msg.GetDescriptor();
  const auto *reflection = pb_msg.GetReflection();
  // 遍历pb_msg字段
  for (int i = 0; i < descriptor->field_count(); ++i) {
    const auto *field = descriptor->field(i);
    if (!field->is_repeated() && !reflection->HasField(pb_msg, field)) {
      //
      // 设置默认字段值
      //
      PyObject *default_obj_ptr{nullptr};
      auto type = field->cpp_type();
      switch (type) {
      case ProtobufFieldDescriptor::CPPTYPE_INT32:
      case ProtobufFieldDescriptor::CPPTYPE_UINT32:
#if PY_MAJOR_VERSION >= 3
        default_obj_ptr = PyLong_FromUnsignedLong(0);
#else
        default_obj_ptr = PyInt_FromLong(0);
#endif // PY_MAJOR_VERSION >= 3
        break;
      case ProtobufFieldDescriptor::CPPTYPE_INT64:
      case ProtobufFieldDescriptor::CPPTYPE_UINT64:
        default_obj_ptr = PyLong_FromUnsignedLongLong(0);
        break;
      case ProtobufFieldDescriptor::CPPTYPE_BOOL:
        default_obj_ptr = PyBool_FromLong(0);
        break;
      case ProtobufFieldDescriptor::CPPTYPE_FLOAT:
      case ProtobufFieldDescriptor::CPPTYPE_DOUBLE:
        default_obj_ptr = PyFloat_FromDouble(.0f);
        break;
      case ProtobufFieldDescriptor::CPPTYPE_STRING:
#if PY_MAJOR_VERSION >= 3
        default_obj_ptr = PyUnicode_FromString("");
#else
        default_obj_ptr = PyString_FromString("");
#endif // PY_MAJOR_VERSION >= 3             
        break;
      case ProtobufFieldDescriptor::CPPTYPE_ENUM:
#if PY_MAJOR_VERSION >= 3
        default_obj_ptr = PyLong_FromLong(0);
#else
        default_obj_ptr = PyInt_FromLong(0);
#endif // PY_MAJOR_VERSION >= 3
        break;
      default:
        break;
      }
      if (default_obj_ptr) {
        SET_ATTR_STRING_XDECREF(runtime_, dict_ptr, field->name(),
                                default_obj_ptr);
      }
    } else {
      auto *obj_ptr = on_field(pb_msg, field);
      if (obj_ptr && (obj_ptr != Py_None)) {
        SET_ATTR_STRING_XDECREF(runtime_, dict_ptr, field->name(), obj_ptr);
      }
    }
  }
  return dict_ptr;
}

auto ProtobufCoder::encode(PyObject *py_obj_ptr, ProtobufMessage &pb_msg)
    -> void {
  if (!py_obj_ptr) {
    write_error_log("Invalid argument for encode");
    return;
  }
  const auto *descriptor = pb_msg.GetDescriptor();
  const auto *reflection = pb_msg.GetReflection();
  for (auto i = 0; i < descriptor->field_count(); i++) {
    auto *attr_obj_ptr =
        PyDict_GetItemString(py_obj_ptr, descriptor->field(i)->name().c_str());
    if (attr_obj_ptr) {
      on_object_value(attr_obj_ptr, pb_msg, reflection, descriptor->field(i));
    } else {
      PyErr_Clear();
    }
  }
}

auto ProtobufCoder::on_field(const ProtobufMessage &msg,
                             const ProtobufFieldDescriptor *field)
    -> PyObject * {
  if (field->is_repeated()) {
    //
    // repeated/map字段
    //
    if (field->is_map()) {
      //
      // map字段
      //
      return on_map_field(msg, field);
    } else {
      //
      // repeated字段
      //
      return on_repeated_field(msg, field);
    }
  } else {
    switch (field->type()) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
      return msg.GetReflection()->GetBool(msg, field) ? Py_True : Py_False;
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT32: {
#if PY_MAJOR_VERSION >= 3
      return PyLong_FromLong(msg.GetReflection()->GetInt32(msg, field));
#else
      return PyInt_FromLong(msg.GetReflection()->GetInt32(msg, field));
#endif // PY_MAJOR_VERSION >= 3
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT64: {
      return PyLong_FromLongLong(msg.GetReflection()->GetInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
#if PY_MAJOR_VERSION >= 3
      return PyLong_FromUnsignedLong(
          msg.GetReflection()->GetUInt32(msg, field));
#else
      return PyInt_FromLong(msg.GetReflection()->GetUInt32(msg, field));
#endif // PY_MAJOR_VERSION >= 3
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
      return PyLong_FromUnsignedLongLong(
          msg.GetReflection()->GetUInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
      return PyFloat_FromDouble(msg.GetReflection()->GetFloat(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
      return PyFloat_FromDouble(msg.GetReflection()->GetDouble(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
#if PY_MAJOR_VERSION >= 3
      return PyUnicode_FromString(
          msg.GetReflection()->GetString(msg, field).c_str());
#else
      return PyString_FromString(
          msg.GetReflection()->GetString(msg, field).c_str());
#endif // PY_MAJOR_VERSION >= 3               
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      return decode(msg.GetReflection()->GetMessage(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
#if PY_MAJOR_VERSION >= 3
      return PyLong_FromLong(msg.GetReflection()->GetEnumValue(msg, field));
#else
      return PyInt_FromLong(msg.GetReflection()->GetEnumValue(msg, field));
#endif // PY_MAJOR_VERSION >= 3      
      break;
    }
    default:
      break;
    }
  }
  return nullptr;
}

auto ProtobufCoder::on_map_field(const ProtobufMessage &msg,
                                 const ProtobufFieldDescriptor *field)
    -> PyObject * {
  auto *dict_obj_ptr = PyDict_New();
  auto count = msg.GetReflection()->FieldSize(msg, field);
  for (int i = 0; i < count; i++) {
    const auto &message_map =
        msg.GetReflection()->GetRepeatedMessage(msg, field, i);
    const auto *descriptor = message_map.GetDescriptor();
    const auto *key_field_descriptor = descriptor->field(0);
    const auto *value_field_descriptor = descriptor->field(1);
    auto *key_obj_ptr = on_map_field_key(message_map, key_field_descriptor);
    auto *value_obj_ptr =
        on_map_field_value(message_map, value_field_descriptor);
    PyDict_SetItem(dict_obj_ptr, key_obj_ptr, value_obj_ptr);
    Py_XDECREF(key_obj_ptr);
    Py_XDECREF(value_obj_ptr);
  }
  return dict_obj_ptr;
}

auto ProtobufCoder::on_map_field_key(const ProtobufMessage &msg,
                                     const ProtobufFieldDescriptor *field)
    -> PyObject * {
  auto key_type = field->type();
  switch (key_type) {
  case ProtobufFieldDescriptor::Type::TYPE_INT32: {
#if PY_MAJOR_VERSION >= 3
    return PyLong_FromLong(msg.GetReflection()->GetInt32(msg, field));
#else
    return PyInt_FromLong(msg.GetReflection()->GetInt32(msg, field));
#endif // PY_MAJOR_VERSION >= 3
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_INT64: {
    return PyLong_FromLongLong(msg.GetReflection()->GetInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_STRING: {
#if PY_MAJOR_VERSION >= 3
    return PyUnicode_FromString(
        msg.GetReflection()->GetString(msg, field).c_str());
#else
    return PyString_FromString(
        msg.GetReflection()->GetString(msg, field).c_str());
#endif // PY_MAJOR_VERSION >= 3             
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
#if PY_MAJOR_VERSION >= 3
    return PyLong_FromUnsignedLong(msg.GetReflection()->GetUInt32(msg, field));
#else
    return PyInt_FromLong(msg.GetReflection()->GetUInt32(msg, field));
#endif // PY_MAJOR_VERSION >= 3
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
    return PyLong_FromUnsignedLongLong(
        msg.GetReflection()->GetUInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
#if PY_MAJOR_VERSION >= 3
    return PyLong_FromLong(msg.GetReflection()->GetEnumValue(msg, field));
#else
    return PyInt_FromLong(msg.GetReflection()->GetEnumValue(msg, field));
#endif // PY_MAJOR_VERSION >= 3         
    break;
  }
  default:
    break;
  }
  return nullptr;
}

auto ProtobufCoder::on_map_field_value(const ProtobufMessage &msg,
                                       const ProtobufFieldDescriptor *field)
    -> PyObject * {
  auto value_type = field->type();
  switch (value_type) {
  case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
    return msg.GetReflection()->GetBool(msg, field) ? Py_True : Py_False;
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_INT32: {
#if PY_MAJOR_VERSION >= 3
    return PyLong_FromLong(msg.GetReflection()->GetInt32(msg, field));
#else
    return PyInt_FromLong(msg.GetReflection()->GetInt32(msg, field));
#endif // PY_MAJOR_VERSION >= 3
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_INT64: {
    return PyLong_FromLongLong(msg.GetReflection()->GetInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
#if PY_MAJOR_VERSION >= 3
    return PyLong_FromUnsignedLong(msg.GetReflection()->GetUInt32(msg, field));
#else
    return PyInt_FromLong(msg.GetReflection()->GetUInt32(msg, field));
#endif // PY_MAJOR_VERSION >= 3
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
    return PyLong_FromUnsignedLongLong(
        msg.GetReflection()->GetUInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
    return PyFloat_FromDouble(msg.GetReflection()->GetFloat(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
    return PyFloat_FromDouble(msg.GetReflection()->GetDouble(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_STRING: {
#if PY_MAJOR_VERSION >= 3
    return PyUnicode_FromString(
        msg.GetReflection()->GetString(msg, field).c_str());
#else
    return PyString_FromString(
        msg.GetReflection()->GetString(msg, field).c_str());
#endif // PY_MAJOR_VERSION >= 3             
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
    return decode(msg.GetReflection()->GetMessage(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
#if PY_MAJOR_VERSION >= 3
    return PyLong_FromLong(msg.GetReflection()->GetEnumValue(msg, field));
#else
    return PyInt_FromLong(msg.GetReflection()->GetEnumValue(msg, field));
#endif // PY_MAJOR_VERSION >= 3         
    break;
  }
  default:
    break;
  }
  return nullptr;
}

auto ProtobufCoder::on_repeated_field(const ProtobufMessage &msg,
                                      const ProtobufFieldDescriptor *field)
    -> PyObject * {
  auto count = msg.GetReflection()->FieldSize(msg, field);
  auto *list_obj_ptr = PyList_New(0);
  if (field->type() == ProtobufFieldDescriptor::Type::TYPE_MESSAGE) {
    for (auto i = 0; i < count; i++) {
      const auto &repeated_message =
          msg.GetReflection()->GetRepeatedMessage(msg, field, i);
      auto *msg_obj_ptr = decode(repeated_message);
      PyList_Append(list_obj_ptr, msg_obj_ptr);
      Py_XDECREF(msg_obj_ptr);
    }
  } else {
    for (int i = 0; i < count; i++) {
      switch (field->type()) {
      case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
        PyList_Append(list_obj_ptr,
                      msg.GetReflection()->GetRepeatedBool(msg, field, i)
                          ? Py_True
                          : Py_False);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT32: {
#if PY_MAJOR_VERSION >= 3
        LIST_APPEND_XDECREF(
            runtime_, list_obj_ptr,
            PyLong_FromLong(
                msg.GetReflection()->GetRepeatedInt32(msg, field, i)));
#else
        LIST_APPEND_XDECREF(
            runtime_, list_obj_ptr,
            PyInt_FromLong(
                msg.GetReflection()->GetRepeatedInt32(msg, field, i)));
#endif // PY_MAJOR_VERSION >= 3
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT64: {
        LIST_APPEND_XDECREF(
            runtime_, list_obj_ptr,
            PyLong_FromLongLong(
                msg.GetReflection()->GetRepeatedInt64(msg, field, i)));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
        auto value = msg.GetReflection()->GetRepeatedUInt32(msg, field, i);
#if PY_MAJOR_VERSION >= 3
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr,
                            PyLong_FromUnsignedLong(value));
#else
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr, PyInt_FromLong(value));
#endif // PY_MAJOR_VERSION >= 3
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
        auto value = msg.GetReflection()->GetRepeatedUInt64(msg, field, i);
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr,
                            PyLong_FromUnsignedLongLong(value));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
        auto value = msg.GetReflection()->GetRepeatedFloat(msg, field, i);
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr, PyFloat_FromDouble(value));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
        auto value = msg.GetReflection()->GetRepeatedDouble(msg, field, i);
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr, PyFloat_FromDouble(value));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
        auto value = msg.GetReflection()->GetRepeatedEnum(msg, field, i);
        auto num = value->number();
#if PY_MAJOR_VERSION >= 3
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr, PyLong_FromLong(num));
#else
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr, PyInt_FromLong(num));
#endif // PY_MAJOR_VERSION >= 3           
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_STRING: {
        auto value = msg.GetReflection()->GetRepeatedString(msg, field, i);
#if PY_MAJOR_VERSION >= 3
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr,
                            PyUnicode_FromString(value.c_str()));
#else
        LIST_APPEND_XDECREF(runtime_, list_obj_ptr,
                            PyString_FromString(value.c_str()));
#endif // PY_MAJOR_VERSION >= 3                                 
        break;
      }
      default:
        break;
      }
    }
  }
  return list_obj_ptr;
}

auto ProtobufCoder::on_object_value(PyObject *py_obj_ptr, ProtobufMessage &msg,
                                    const ProtobufReflection *ref,
                                    const ProtobufFieldDescriptor *field)
    -> void {
  if (!py_obj_ptr) {
    return;
  }
  if (field->is_repeated()) {
    if (field->is_map()) {
      on_object_map(py_obj_ptr, msg, ref, field);
    } else {
      on_object_repeated_value(py_obj_ptr, msg, ref, field);
    }
  } else {
    switch (field->type()) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL:
      ref->SetBool(&msg, field, py_obj_ptr == Py_True);
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT32:
      ref->SetInt32(&msg, field, python::PyCast<std::int32_t>(py_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT64:
      ref->SetInt64(&msg, field, python::PyCast<std::int64_t>(py_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT32:
      ref->SetUInt32(&msg, field, python::PyCast<std::uint32_t>(py_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT64:
      ref->SetUInt64(&msg, field, python::PyCast<std::uint64_t>(py_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
      ref->SetFloat(&msg, field, python::PyCast<float>(py_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
      ref->SetDouble(&msg, field, python::PyCast<double>(py_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      ref->SetString(&msg, field, python::PyCast<const char *>(py_obj_ptr));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      encode(py_obj_ptr, *ref->MutableMessage(&msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      ref->SetEnumValue(&msg, field, python::PyCast<int>(py_obj_ptr));
      break;
    }
    default:
      break;
    }
  }
}

auto ProtobufCoder::on_object_map(PyObject *py_obj_ptr, ProtobufMessage &msg,
                                  const ProtobufReflection *ref,
                                  const ProtobufFieldDescriptor *field)
    -> void {
  if (!PyDict_Check(py_obj_ptr)) {
    return;
  }
  auto *items_ptr = PyDict_Items(py_obj_ptr);
  auto *list_iter_ptr = PyObject_GetIter(items_ptr);
  PyObject *tuple_item_ptr{nullptr};
  while (tuple_item_ptr = PyIter_Next(list_iter_ptr)) {
    auto *pair_message = ref->AddMessage(&msg, field);
    auto *pair_reflection = pair_message->GetReflection();
    const auto *key_descriptor = field->message_type()->field(0);
    const auto *value_descriptor = field->message_type()->field(1);
    auto *key_obj_ptr = PyTuple_GetItem(tuple_item_ptr, 0);
    auto *value_obj_ptr = PyTuple_GetItem(tuple_item_ptr, 1);
    auto key_type = key_descriptor->type();
    switch (key_type) {
    case ProtobufFieldDescriptor::Type::TYPE_INT32: {
      pair_reflection->SetInt32(pair_message, key_descriptor,
                                python::PyCast<std::int32_t>(key_obj_ptr));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT64: {
      pair_reflection->SetInt64(pair_message, key_descriptor,
                                python::PyCast<std::int64_t>(key_obj_ptr));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      pair_reflection->SetString(pair_message, key_descriptor,
                                 python::PyCast<const char *>(key_obj_ptr));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
      pair_reflection->SetUInt32(pair_message, key_descriptor,
                                 python::PyCast<std::uint32_t>(key_obj_ptr));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
      pair_reflection->SetUInt64(pair_message, key_descriptor,
                                 python::PyCast<std::uint64_t>(key_obj_ptr));
      break;
    }
    default:
      break;
    }
    auto value_type = value_descriptor->type();
    switch (value_type) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL:
      pair_reflection->SetBool(pair_message, value_descriptor,
                               value_obj_ptr == Py_True);
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT32:
      pair_reflection->SetInt32(pair_message, value_descriptor,
                                python::PyCast<std::int32_t>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT64:
      pair_reflection->SetInt64(pair_message, value_descriptor,
                                python::PyCast<std::int64_t>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT32:
      pair_reflection->SetUInt32(pair_message, value_descriptor,
                                 python::PyCast<std::uint32_t>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT64:
      pair_reflection->SetUInt64(pair_message, value_descriptor,
                                 python::PyCast<std::uint64_t>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
      pair_reflection->SetFloat(pair_message, value_descriptor,
                                python::PyCast<float>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
      pair_reflection->SetDouble(pair_message, value_descriptor,
                                 python::PyCast<double>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_STRING:
      pair_reflection->SetString(pair_message, value_descriptor,
                                 python::PyCast<const char *>(value_obj_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      auto *new_message =
          pair_reflection->MutableMessage(pair_message, value_descriptor);
      encode(value_obj_ptr, *new_message);
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM:
      pair_reflection->SetEnumValue(pair_message, value_descriptor,
                                    python::PyCast<int>(value_obj_ptr));
      break;
    default:
      break;
    }
    Py_XDECREF(tuple_item_ptr);
  }
  Py_XDECREF(list_iter_ptr);
  Py_XDECREF(items_ptr);
}

auto ProtobufCoder::on_object_repeated_value(
    PyObject *py_obj_ptr, ProtobufMessage &msg, const ProtobufReflection *ref,
    const ProtobufFieldDescriptor *field) -> void {
  if (!PyList_Check(py_obj_ptr) && !PySet_Check(py_obj_ptr)) {
    write_error_log("Invalid field");
    return;
  }
  auto *list_iter_ptr = PyObject_GetIter(py_obj_ptr);
  PyObject *list_item_ptr{nullptr};
  while (list_item_ptr = PyIter_Next(list_iter_ptr)) {
    switch (field->type()) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL:
      ref->AddBool(&msg, field, list_item_ptr == Py_True);
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT32:
      ref->AddInt32(&msg, field, python::PyCast<std::int32_t>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT64:
      ref->AddInt64(&msg, field, python::PyCast<std::int64_t>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT32:
      ref->AddUInt32(&msg, field, python::PyCast<std::uint32_t>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT64:
      ref->AddUInt64(&msg, field, python::PyCast<std::uint64_t>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
      ref->AddFloat(&msg, field, python::PyCast<float>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
      ref->AddDouble(&msg, field, python::PyCast<double>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_STRING:
      ref->AddString(&msg, field, python::PyCast<const char *>(list_item_ptr));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      auto *new_message = ref->AddMessage(&msg, field);
      encode(list_item_ptr, *new_message);
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      ref->AddEnumValue(&msg, field, python::PyCast<int>(list_item_ptr));
      break;
    }
    default:
      break;
    }
    Py_DECREF(list_item_ptr);
  }
  Py_DECREF(list_iter_ptr);
}

auto ProtobufCoder::write_error_log(const std::string &log) -> void {
  if (runtime_) {
    runtime_->write_error_log(log);
  }
}

CppRuntime::CppRuntime() {
  sched_ptr_ = std::make_unique<kratos::service::SchedulerImpl>(nullptr);
  timer_cb_func_ = std::bind(&CppRuntime::timer_cb, this, std::placeholders::_1,
                             std::placeholders::_2);
  ext_timer_cb_func_ = std::bind(&CppRuntime::external_timer_cb, this,
                                 std::placeholders::_1, std::placeholders::_2);
}

CppRuntime::~CppRuntime() { /*deinit();*/
}

auto CppRuntime::get_config() -> kratos::config::BoxConfig & {
  return *config_.get();
}

auto CppRuntime::get_logger_appender() -> klogger::Appender * {
  return nullptr;
}

auto CppRuntime::get_lang() -> kratos::lang::Lang * { return nullptr; }

void CppRuntime::on_listen(const std::string &name, bool success,
                           kratos::service::BoxChannelPtr &channel) {
  if (!success) {
    write_error_log("Listen [" + name + "] failed");
  }
  return;
}

void CppRuntime::on_accept(kratos::service::BoxChannelPtr &channel) {
  write_info_log("Accept channel from [" + channel->get_channel_name() + "]");
}

void CppRuntime::on_connect(const std::string &name, bool success,
                            kratos::service::BoxChannelPtr &channel) {
#ifndef PYTHON_SDK
  if (channel) {
    service_layer_->on_connect(name, channel->get_id(), success);
  } else {
    service_layer_->on_connect(name, 0, success);
  }
#endif // PYTHON_SDK
  if (success) {
#ifdef PYTHON_SDK
    // 网关连接成功
    proxy_channel_ = channel;
#endif // PYTHON_SDK
    write_info_log("Connect to [" + name + "] successfully");
  } else {
    write_error_log("Connect to [" + name + "] failed");
#ifdef PYTHON_SDK
    is_connecting_ = false;
#endif // PYTHON_SDK
  }
}

void CppRuntime::on_close(kratos::service::BoxChannelPtr &channel) {
  if (!channel->get_channel_name().empty()) {
    write_info_log("Disconnect from [" + channel->get_channel_name() + "]");
#ifndef PYTHON_SDK
    service_layer_->on_close(channel->get_channel_name(), channel->get_id());
#else
    // 网关连接断开
    proxy_channel_.reset();
    is_connecting_ = false;
#endif // PYTHON_SDK
    // 连接断开时清理订阅
    for (auto it = sub_info_map_.begin(); it != sub_info_map_.end();) {
      if (it->second.channel_id == channel->get_id()) {
        it = sub_info_map_.erase(it);
      } else {
        it++;
      }
    }
  }
}

void CppRuntime::on_data(kratos::service::BoxChannelPtr &channel) {
  while (true) {
    auto *py_obj = pop(channel);
    if (py_obj && (py_obj != Py_None)) {
      Py_XINCREF(py_obj);
      pack_list_.push_back(py_obj);
    } else {
      break;
    }
  }
}

auto CppRuntime::pop(kratos::service::BoxChannelPtr &channel) -> PyObject * {
  //
  // 检测整包
  //
  auto avail_size = channel->size();
  if (avail_size < int(sizeof(rpc::RpcMsgHeader))) {
    Py_RETURN_NONE;
  }
  //
  // 总的包头
  //
  rpc::RpcMsgHeader header;
  channel->peek((char *)&header, (int)sizeof(header));
  header.ntoh();
  if (header.length > std::uint32_t(avail_size)) {
    Py_RETURN_NONE;
  }
  if (config_->get_max_packet_size()) {
    if (header.length - sizeof(rpc::RpcMsgHeader) >
        config_->get_max_packet_size()) {
      // 配置格式错误
      write_error_log("Reach maximum packet size [" +
                      std::to_string(header.length) + "]");
      channel->skip(header.length);
      Py_RETURN_NONE;
    }
  }
  std::size_t header_length = sizeof(header);
  auto *buf_ptr = get_buffer_ptr(std::size_t(header.length));
  channel->recv(buf_ptr, header.length);
  //
  // 根据ID进行解包，封装OBJECT返回给python层
  //
  switch (rpc::RpcMsgType(header.type)) {
  case rpc::RpcMsgType::RPC_CALL: {
    auto *header_ptr = (rpc::RpcCallRequestHeader *)(buf_ptr + sizeof(header));
    header_ptr->ntoh();
    header_length += sizeof(rpc::RpcCallRequestHeader);
    return on_call(channel->get_id(), header_ptr, buf_ptr + header_length,
                   int(header.length) - int(header_length));
  } break;
  case rpc::RpcMsgType::RPC_RETURN: {
    auto *header_ptr = (rpc::RpcCallRetHeader *)(buf_ptr + sizeof(header));
    header_ptr->ntoh();
    header_length += sizeof(rpc::RpcCallRetHeader);
    return on_call_ret(channel->get_id(), header_ptr, buf_ptr + header_length,
                       int(header.length) - int(header_length));
  } break;
  case rpc::RpcMsgType::RPC_PROXY_CALL: {
    auto *header_ptr =
        (rpc::RpcProxyCallRequestHeader *)(buf_ptr + sizeof(header));
    header_ptr->ntoh();
    header_length += sizeof(rpc::RpcProxyCallRequestHeader);
    return on_proxy_call(channel->get_id(), header_ptr, buf_ptr + header_length,
                         int(header.length) - int(header_length));
  } break;
  case rpc::RpcMsgType::RPC_PROXY_RETURN: {
    auto *header_ptr = (rpc::RpcProxyCallRetHeader *)(buf_ptr + sizeof(header));
    header_ptr->ntoh();
    header_length += sizeof(rpc::RpcProxyCallRetHeader);
    return on_proxy_call_ret(channel->get_id(), header_ptr,
                             buf_ptr + header_length,
                             int(header.length) - int(header_length));
  } break;
  case rpc::RpcMsgType::RPC_EVENT_SUB: {
    auto *header_ptr = (rpc::RpcSubHeader *)(buf_ptr);
    header_ptr->ntoh();
    return on_sub(channel->get_id(), header_ptr,
                  buf_ptr + sizeof(rpc::RpcSubHeader),
                  int(header.length) - int(sizeof(rpc::RpcSubHeader)));
  } break;
  case rpc::RpcMsgType::RPC_EVENT_CANCEL: {
    auto *header_ptr = (rpc::RpcCancelSubHeader *)(buf_ptr);
    header_ptr->ntoh();
    return on_cancel(channel->get_id(), header_ptr);
  } break;
  case rpc::RpcMsgType::RPC_EVENT_PUB: {
    auto *header_ptr = (rpc::RpcPubHeader *)(buf_ptr);
    header_ptr->ntoh();
    return on_pub(channel->get_id(), header_ptr,
                  buf_ptr + sizeof(rpc::RpcPubHeader),
                  int(header.length) - int(sizeof(rpc::RpcPubHeader)));
  } break;
  default:
    break;
  }
  Py_RETURN_NONE;
}

auto CppRuntime::start_all_listener() -> bool {
  for (const auto &host : config_->get_listener_list()) {
    std::string ip;
    int port{0};
    if (!util::get_host_config(host, ip, port)) {
      // 配置格式错误
      write_error_log("Listener host configuration format error [" + host +
                      "]");
      return false;
    }
    if (!BoxNetwork::listen_at(host, util::get_network_type(host), ip, port)) {
      // 开启监听器失败
      write_error_log("Start listener[" + host + "] failed");
      return false;
    } else {
      write_info_log("Start listener[" + host + "] successfully");
    }
  }
  return true;
}

auto CppRuntime::runtime_update() -> void {
  auto now = util::get_os_time_millionsecond();
  //
  // 超时协议对象
  //
  sched_ptr_->do_schedule(now);

#ifndef PYTHON_SDK
  //
  // 服务发现/注册
  //
  if (service_register_) {
    service_register_->update(now);
  }
  if (service_finder_) {
    service_finder_->update(now);
  }
#else
  // 检测连接
  check_connection(now);
#endif // PYTHON_SDK

  //
  // 网络主循环
  //
  update();
}

auto CppRuntime::random_get_expose_listener_host() -> std::string {
  const auto &local_listener = config_->get_expose_listener_list();
  if (local_listener.empty()) {
    return "";
  }
  auto index =
      util::get_random_uint32(0, (std::uint32_t)local_listener.size() - 1);
  if (index >= local_listener.size()) {
    return "";
  }
  return local_listener[index];
}

auto CppRuntime::unregister_all_service() -> void {
  //
  // 防止边遍历边修改
  //
  auto temp = registered_service_map_;
  for (const auto &it : temp) {
    unregister_service(it.first);
  }
  registered_service_map_.clear();
}

auto CppRuntime::pop() -> PyObject * {
  if (!pack_list_.empty()) {
    //
    // 队列不为空, 优先返回事件
    //
    auto *py_obj = pack_list_.front();
    pack_list_.pop_front();
    Py_XDECREF(py_obj);
    return py_obj;
  } else {
    //
    // 队列为空时启动运行时的循环
    //
    runtime_update();
    Py_RETURN_NONE;
  }
}

auto CppRuntime::reg_proxy_call(rpc::CallID call_id, rpc::ServiceUUID uuid,
                                rpc::MethodID method_id,
                                rpc::ServiceID service_id, std::time_t timeout)
    -> void {
  auto *method_type = python::global_msg_factory.get_type(uuid, method_id);
  if (!method_type) {
    return;
  }
  if (method_type->oneway) {
    //
    // ONEWAY方法不记录
    //
    return;
  }
  set_call_info(call_id, uuid, method_id, service_id, timeout);
}

auto CppRuntime::call(std::uint64_t channel_id, rpc::ServiceUUID serviceUUID,
                      rpc::ServiceID serviceID, rpc::CallID callID,
                      rpc::MethodID methodID, PyObject *obj_ptr) -> bool {
  auto *msg_ptr = get_msg_arg(serviceUUID, methodID);
  if (!msg_ptr) {
    write_error_log("Message not found UUID[" + std::to_string(serviceUUID) +
                    "], method ID[" + std::to_string(methodID) + "]");
    return false;
  }
  coder_.encode(obj_ptr, *msg_ptr);
  auto length = msg_ptr->ByteSizeLong();
  auto header_length =
      sizeof(rpc::RpcMsgHeader) + sizeof(rpc::RpcCallRequestHeader);
  auto total_length = header_length + length;
  auto *buf_ptr = get_buffer_ptr(total_length);
  auto *msg_header = reinterpret_cast<rpc::RpcMsgHeader *>(buf_ptr);
  msg_header->type = decltype(msg_header->type)(rpc::RpcMsgType::RPC_CALL);
  msg_header->length = decltype(msg_header->length)(total_length);
  msg_header->hton();
  auto *msg_call_req_header = reinterpret_cast<rpc::RpcCallRequestHeader *>(
      buf_ptr + sizeof(rpc::RpcMsgHeader));
  msg_call_req_header->callID = callID;
  msg_call_req_header->methodID = methodID;
  msg_call_req_header->serviceID = serviceID;
  msg_call_req_header->serviceUUID = serviceUUID;
  msg_call_req_header->hton();
  if (!msg_ptr->SerializeToArray(buf_ptr + header_length, (int)length)) {
    write_error_log("Message SerializeToArray failed, UUID[" +
                    std::to_string(serviceUUID) + "], method ID[" +
                    std::to_string(methodID) + "]");
    return false;
  }
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (channel) {
    channel->send(buf_ptr, (int)total_length);
  } else {
    write_error_log("Send failed call ID[" + std::to_string(callID) + "]");
    return false;
  }
  return true;
}

auto CppRuntime::call_proxy(std::uint64_t channel_id,
                            rpc::ServiceUUID serviceUUID,
                            rpc::ServiceID serviceID, rpc::CallID callID,
                            rpc::MethodID methodID,
                            rpc::GlobalIndex globalIndex, std::uint16_t oneway,
                            PyObject *obj_ptr) -> bool {
  auto *msg_ptr = get_msg_arg(serviceUUID, methodID);
  if (!msg_ptr) {
    write_error_log("Message not found UUID[" + std::to_string(serviceUUID) +
                    "], method ID[" + std::to_string(methodID) + "]");
    return false;
  }
  coder_.encode(obj_ptr, *msg_ptr);
  auto length = msg_ptr->ByteSizeLong();
  auto header_length =
      sizeof(rpc::RpcMsgHeader) + sizeof(rpc::RpcProxyCallRequestHeader);
  auto total_length = header_length + length;
  auto *buf_ptr = get_buffer_ptr(total_length);
  auto *msg_header = (rpc::RpcMsgHeader *)buf_ptr;
  msg_header->type = (rpc::MsgTypeID)rpc::RpcMsgType::RPC_PROXY_CALL;
  msg_header->length = (std::uint32_t)total_length;
  msg_header->hton();
  auto *msg_prx_call_req_header =
      (rpc::RpcProxyCallRequestHeader *)(buf_ptr + sizeof(rpc::RpcMsgHeader));
  msg_prx_call_req_header->callID = callID;
  msg_prx_call_req_header->methodID = methodID;
  msg_prx_call_req_header->serviceID = serviceID;
  msg_prx_call_req_header->serviceUUID = serviceUUID;
  msg_prx_call_req_header->globalIndex = globalIndex;
  msg_prx_call_req_header->oneWay = oneway;
  msg_prx_call_req_header->hton();
  if (!msg_ptr->SerializeToArray(buf_ptr + header_length, (int)length)) {
    write_error_log("Message SerializeToArray failed, UUID[" +
                    std::to_string(serviceUUID) + "], method ID[" +
                    std::to_string(methodID) + "]");
    return false;
  }
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (channel) {
    channel->send(buf_ptr, (int)total_length);
  } else {
    return false;
  }
  return true;
}

auto CppRuntime::call_ret(std::uint64_t channel_id,
                          rpc::ServiceUUID serviceUUID, rpc::MethodID methodID,
                          rpc::ServiceID serviceID, rpc::CallID callID,
                          rpc::ErrorID errorID, PyObject *obj_ptr) -> bool {
  auto *msg_ptr = get_msg_ret(serviceUUID, methodID);
  if (!msg_ptr) {
    write_error_log("Message not found UUID[" + std::to_string(serviceUUID) +
                    "], method ID[" + std::to_string(methodID) + "]");
    return false;
  }
  coder_.encode(obj_ptr, *msg_ptr);
  auto length = msg_ptr->ByteSizeLong();
  auto header_length =
      sizeof(rpc::RpcMsgHeader) + sizeof(rpc::RpcCallRetHeader);
  auto total_length = header_length + length;
  auto *buf_ptr = get_buffer_ptr(total_length);
  auto *msg_header = (rpc::RpcMsgHeader *)buf_ptr;
  msg_header->type = (rpc::MsgTypeID)rpc::RpcMsgType::RPC_RETURN;
  msg_header->length = (std::uint32_t)total_length;
  msg_header->hton();
  auto *msg_call_ret_header =
      (rpc::RpcCallRetHeader *)(buf_ptr + sizeof(rpc::RpcMsgHeader));
  msg_call_ret_header->callID = callID;
  msg_call_ret_header->serviceID = serviceID;
  msg_call_ret_header->errorID = errorID;
  msg_call_ret_header->hton();
  if (!msg_ptr->SerializeToArray(buf_ptr + header_length, (int)length)) {
    write_error_log("Message SerializeToArray failed, UUID[" +
                    std::to_string(serviceUUID) + "], method ID[" +
                    std::to_string(methodID) + "]");
    return false;
  }
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (channel) {
    channel->send(buf_ptr, (int)total_length);
  } else {
    return false;
  }
  return true;
}

auto CppRuntime::call_proxy_ret(std::uint64_t channel_id,
                                rpc::ServiceUUID serviceUUID,
                                rpc::MethodID methodID,
                                rpc::ServiceID serviceID, rpc::CallID callID,
                                rpc::ErrorID errorID,
                                rpc::GlobalIndex globalIndex, PyObject *obj_ptr)
    -> bool {
  auto *msg_ptr = get_msg_ret(serviceUUID, methodID);
  if (!msg_ptr) {
    write_error_log("Message not found UUID[" + std::to_string(serviceUUID) +
                    "], method ID[" + std::to_string(methodID) + "]");
    return false;
  }
  coder_.encode(obj_ptr, *msg_ptr);
  auto length = msg_ptr->ByteSizeLong();
  auto header_length =
      sizeof(rpc::RpcMsgHeader) + sizeof(rpc::RpcProxyCallRetHeader);
  auto total_length = header_length + length;
  auto *buf_ptr = get_buffer_ptr(total_length);
  auto *msg_header = (rpc::RpcMsgHeader *)buf_ptr;
  msg_header->type =
      decltype(msg_header->type)(rpc::RpcMsgType::RPC_PROXY_RETURN);
  msg_header->length = decltype(msg_header->length)(total_length);
  msg_header->hton();
  auto *msg_prx_call_ret_header =
      (rpc::RpcProxyCallRetHeader *)(buf_ptr + sizeof(rpc::RpcMsgHeader));
  msg_prx_call_ret_header->callID = callID;
  msg_prx_call_ret_header->serviceID = serviceID;
  msg_prx_call_ret_header->errorID = errorID;
  msg_prx_call_ret_header->globalIndex = globalIndex;
  msg_prx_call_ret_header->hton();
  if (!msg_ptr->SerializeToArray(buf_ptr + header_length, (int)length)) {
    write_error_log("Message SerializeToArray failed, UUID[" +
                    std::to_string(serviceUUID) + "], method ID[" +
                    std::to_string(methodID) + "]");
    return false;
  }
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (channel) {
    channel->send(buf_ptr, (int)total_length);
  } else {
    return false;
  }
  return true;
}

auto CppRuntime::subscribe(std::uint64_t channel_id, const std::string &sub_id,
                           rpc::ProxyID proxy_id, rpc::ServiceUUID serviceUUID,
                           rpc::MethodID methodID, rpc::ServiceID serviceID,
                           const std::string &evt_name, PyObject *obj_ptr)
    -> bool {
  if (sub_id.size() != rpc::SUB_ID_LEN) {
    // TODO error
    return false;
  }
  // 尝试获取协议对象
  auto *msg_ptr = get_msg_arg(serviceUUID, methodID);
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (PyDict_Check(obj_ptr)) {
    if (!msg_ptr) {
      // 表对象同时没有找到协议对象
      write_error_log("Message not found UUID[" + std::to_string(serviceUUID) +
                      "], method ID[" + std::to_string(methodID) + "]");
      return false;
    }
    coder_.encode(obj_ptr, *msg_ptr);
    // 协议对象长度
    auto body_length = msg_ptr->ByteSizeLong();
    // 总长度: 消息头+事件名+协议对象
    auto total_length =
        sizeof(rpc::RpcSubHeader) + evt_name.size() + body_length;
    auto buffer = std::make_unique<char[]>(total_length);
    auto *subHeader = reinterpret_cast<rpc::RpcSubHeader *>(buffer.get());
    subHeader->header.type = rpc::MsgTypeID(rpc::RpcMsgType::RPC_EVENT_SUB);
    subHeader->header.length = std::uint32_t(total_length);
    subHeader->data_length = std::uint32_t(body_length);
    subHeader->name_length = std::uint32_t(evt_name.size());
    subHeader->proxy_id = proxy_id;
    subHeader->serviceUUID = serviceUUID;
    subHeader->serviceID = serviceID;
    std::memcpy(subHeader->sub_id, sub_id.c_str(), sub_id.size());
    std::memcpy(buffer.get() + sizeof(rpc::RpcSubHeader), evt_name.c_str(),
                evt_name.size());
    subHeader->hton();
    if (!msg_ptr->SerializeToArray(buffer.get() + sizeof(rpc::RpcSubHeader) +
                                       evt_name.size(),
                                   (int)body_length)) {
      write_error_log("Event[" + evt_name + "] SerializeToArray failed");
      return false;
    }
    if (channel) {
      channel->send(buffer.get(), (int)total_length);
    } else {
      return false;
    }
  } else {
#if PY_MAJOR_VERSION >= 3
    if (!PyUnicode_Check(obj_ptr)) {
#else
    if (!PyUnicode_Check(obj_ptr) && !PyString_Check(obj_ptr)) {
#endif // PY_MAJOR_VERSION >= 3           
      write_error_log("Event[" + evt_name + "] data not a string");
      return false;
    }
    Py_ssize_t size{0};
#if PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 3
    auto *cstr = PyUnicode_AsUTF8AndSize(obj_ptr, &size);
#else
    size = PyString_Size(obj_ptr);
    auto *cstr = PyString_AsString(obj_ptr);
#endif // PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 3
    if (!cstr) {
      write_error_log("Event[" + evt_name + "] data not a string");
      return false;
    }
    auto total_length = sizeof(rpc::RpcSubHeader) + evt_name.size() + size;
    auto buffer = std::make_unique<char[]>(total_length);
    auto *subHeader = reinterpret_cast<rpc::RpcSubHeader *>(buffer.get());
    subHeader->header.type = rpc::MsgTypeID(rpc::RpcMsgType::RPC_EVENT_SUB);
    subHeader->header.length = std::uint32_t(total_length);
    subHeader->data_length = std::uint32_t(size);
    subHeader->name_length = std::uint32_t(evt_name.size());
    subHeader->proxy_id = proxy_id;
    subHeader->serviceUUID = serviceUUID;
    subHeader->serviceID = serviceID;
    std::memcpy(subHeader->sub_id, sub_id.c_str(), sub_id.size());
    std::memcpy(buffer.get() + sizeof(rpc::RpcSubHeader), evt_name.c_str(),
                evt_name.size());
    std::memcpy(buffer.get() + sizeof(rpc::RpcSubHeader) + evt_name.size(),
                cstr, size);
    subHeader->hton();
    if (channel) {
      channel->send(buffer.get(), (int)total_length);
    } else {
      return false;
    }
  }
  sub_info_map_[sub_id] =
      SubInfo{channel_id, serviceUUID, methodID, serviceID, evt_name, proxy_id};
  return true;
}

auto CppRuntime::cancel(std::uint64_t channel_id, const std::string &sub_id)
    -> void {
  if (sub_id.size() != rpc::SUB_ID_LEN) {
    write_error_log("SubID must length(bytes) equal " +
                    std::to_string(rpc::SUB_ID_LEN));
    return;
  }
  rpc::RpcCancelSubHeader cancelHeader;
  cancelHeader.header.type = rpc::MsgTypeID(rpc::RpcMsgType::RPC_EVENT_CANCEL);
  cancelHeader.header.length = sizeof(cancelHeader);
  std::memcpy(cancelHeader.sub_id, sub_id.c_str(), sub_id.size());
  cancelHeader.hton();
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (channel) {
    channel->send(reinterpret_cast<char *>(&cancelHeader),
                  (int)sizeof(cancelHeader));
  }
}

auto CppRuntime::publish(std::uint64_t channel_id, const std::string &sub_id,
                         rpc::ProxyID proxy_id, PyObject *obj_ptr) -> void {
  if (sub_id.size() != rpc::SUB_ID_LEN) {
    write_error_log("SubID must length(bytes) equal " +
                    std::to_string(rpc::SUB_ID_LEN));
    return;
  }
  auto info_it = sub_info_map_.find(sub_id);
  if (info_it == sub_info_map_.end()) {
    return;
  }
  auto *msg_ptr = get_msg_ret(info_it->second.uuid, info_it->second.method_id);
#ifndef PYTHON_SDK
  auto &channel = get_channel(channel_id);
#else
  (void)channel_id;
  auto &channel = proxy_channel_;
#endif
  if (PyDict_Check(obj_ptr)) {
    if (!msg_ptr) {
      write_error_log("Message not found UUID[" +
                      std::to_string(info_it->second.uuid) + "], method ID[" +
                      std::to_string(info_it->second.method_id) + "]");
      return;
    }
    coder_.encode(obj_ptr, *msg_ptr);
    auto body_length = msg_ptr->ByteSizeLong();
    auto total_length = std::uint32_t(sizeof(rpc::RpcPubHeader) + body_length);
    auto buffer = std::make_unique<char[]>(total_length);
    auto *pubHeader = reinterpret_cast<rpc::RpcPubHeader *>(buffer.get());
    pubHeader->header.type = rpc::MsgTypeID(rpc::RpcMsgType::RPC_EVENT_PUB);
    pubHeader->header.length = total_length;
    std::memcpy(pubHeader->sub_id, info_it->first.c_str(),
                info_it->first.size());
    pubHeader->proxy_id = info_it->second.proxy_id;
    pubHeader->value_length = std::uint32_t(body_length);
    if (!msg_ptr->SerializeToArray(buffer.get() + sizeof(rpc::RpcPubHeader),
                                   std::uint32_t(body_length))) {
      return;
    }
    pubHeader->hton();
    if (channel) {
      channel->send(buffer.get(), (int)total_length);
    }
  } else {
#if PY_MAJOR_VERSION >= 3
    if (!PyUnicode_Check(obj_ptr)) {
#else
    if (!PyUnicode_Check(obj_ptr) && !PyString_Check(obj_ptr)) {
#endif // PY_MAJOR_VERSION >= 3            
      write_error_log("Event[" + info_it->second.evt_name +
                      "] data not a string");
      return;
    }
    Py_ssize_t size{0};
#if PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 3
    auto *cstr = PyUnicode_AsUTF8AndSize(obj_ptr, &size);
#else
    size = PyString_Size(obj_ptr);
    auto *cstr = PyString_AsString(obj_ptr);
#endif // PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 3
    if (!cstr) {
      return;
    }
    auto total_length = sizeof(rpc::RpcPubHeader) + size;
    auto buffer = std::make_unique<char[]>(total_length);
    auto *pubHeader = reinterpret_cast<rpc::RpcPubHeader *>(buffer.get());
    pubHeader->header.type = rpc::MsgTypeID(rpc::RpcMsgType::RPC_EVENT_PUB);
    pubHeader->header.length = std::uint32_t(total_length);
    std::memcpy(pubHeader->sub_id, info_it->first.c_str(),
                info_it->first.size());
    pubHeader->proxy_id = info_it->second.proxy_id;
    pubHeader->value_length = std::uint32_t(size);
    std::memcpy(buffer.get() + sizeof(rpc::RpcPubHeader), cstr, size);
    pubHeader->hton();
    if (channel) {
      channel->send(buffer.get(), (int)total_length);
    }
  }
}

auto CppRuntime::register_service(const std::string &service_name) -> bool {
#ifndef PYTHON_SDK
  //
  // 从listener.expose_host(如果配置,优先选择)/listener.host内随机一个监听端口
  //
  auto host = random_get_expose_listener_host();
  if (host.empty()) {
    return false;
  }
  if (!service_register_->register_service(service_name, host)) {
    write_log("Register service [" + service_name + "] failed");
    return false;
  }
  registered_service_map_[service_name] = host;
  write_log("Register service [" + service_name + "]");
  return true;
#else
  return false;
#endif // PYTHON_SDK
}

auto CppRuntime::unregister_service(const std::string &service_name) -> bool {
#ifndef PYTHON_SDK
  auto it = registered_service_map_.find(service_name);
  if (it == registered_service_map_.end()) {
    return false;
  }
  auto retval = service_register_->unregister_service(service_name, it->second);
  if (retval) {
    registered_service_map_.erase(it);
  }
  write_log("Unregister service [" + service_name + "]");
  return retval;
#else
  return false;
#endif // PYTHON_SDK
}

auto CppRuntime::on_call(std::uint64_t channel_id,
                         rpc::RpcCallRequestHeader *call_req_ptr,
                         const char *data, int length) -> PyObject * {
  auto *obj_ptr = deserialize_arg(data, length, call_req_ptr->serviceUUID,
                                  call_req_ptr->methodID);
  if (!obj_ptr || obj_ptr == Py_None) {
    Py_RETURN_NONE;
  }
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "MSGID",
                          PyLong_FromLong((long)rpc::RpcMsgType::RPC_CALL));
  SET_ATTR_STRING_XDECREF(
      this, obj_ptr, "UUID",
      PyLong_FromUnsignedLongLong(call_req_ptr->serviceUUID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                          PyLong_FromUnsignedLongLong(channel_id));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "SERVICEID",
                          PyLong_FromUnsignedLong(call_req_ptr->serviceID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "METHODID",
                          PyLong_FromUnsignedLong(call_req_ptr->methodID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CALLID",
                          PyLong_FromUnsignedLong(call_req_ptr->callID));
  return obj_ptr;
}

auto CppRuntime::on_call_ret(std::uint64_t channel_id,
                             rpc::RpcCallRetHeader *call_ret_ptr,
                             const char *data, int length) -> PyObject * {
  const auto &call_info = get_call_info(call_ret_ptr->callID);
  if (!call_info) {
    Py_RETURN_NONE;
  }
  auto *obj_ptr = deserialize_ret(data, length, call_info.uuid,
                                  call_info.method_id, call_ret_ptr->errorID);
  if (!obj_ptr) {
    Py_RETURN_NONE;
  }
  call_info_map_.erase(call_ret_ptr->callID);
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "MSGID",
                          PyLong_FromLong((long)rpc::RpcMsgType::RPC_RETURN));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                          PyLong_FromUnsignedLongLong(channel_id));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "SERVICEID",
                          PyLong_FromUnsignedLong(call_ret_ptr->serviceID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CALLID",
                          PyLong_FromUnsignedLong(call_ret_ptr->callID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "ERRORID",
                          PyLong_FromUnsignedLong(call_ret_ptr->errorID));
  return obj_ptr;
}

#ifndef PYTHON_SDK
auto CppRuntime::init(const char *config_file_path) -> bool {
  config_ = std::make_unique<kratos::config::BoxConfigImpl>(nullptr);
  std::string config_error;
  if (!config_->load(config_file_path, config_error)) {
    write_error_log(config_error);
    return false;
  }
  if (!config_->has_attribute("rpc_root_dir")) {
    write_error_log("Need rpc_root_dir attribute but not found");
    return false;
  }
  if (!start()) {
    write_error_log("Start network failed");
    return false;
  }
  auto rpc_root_dir = config_->get_string("rpc_root_dir");
  if (!python::global_msg_factory.load(rpc_root_dir, rpc_root_dir)) {
    write_error_log("Apply RPC configuration failed");
    return false;
  }
  service_layer_ = std::make_unique<kratos::service::ServiceLayer>(nullptr);
  service_register_ =
      std::make_unique<kratos::service::ServiceRegisterZookeeper>();
  service_finder_ = std::make_unique<kratos::service::ServiceFinderZookeeper>();
  write_info_log("Start service register ...");
  if (!service_register_->start(
          config_->get_service_finder_hosts(),
          (int)config_->get_service_finder_connect_timeout(),
          config_->get_version())) {
    write_error_log("Start service register failed");
    service_register_->stop();
    return false;
  }
  write_info_log("Start service register success");
  write_info_log("Start service finder ...");
  if (!service_finder_->start(
          config_->get_service_finder_hosts(),
          (int)config_->get_service_finder_connect_timeout(),
          config_->get_version())) {
    write_error_log("Start service finder failed");
    service_register_->stop();
    service_finder_->stop();
    return false;
  }
  write_info_log("Start service finder success");
  service_layer_->set_service_mach(this, service_finder_.get(),
                                   service_register_.get());
  if (!start_all_listener()) {
    return false;
  }
  if (config_->is_open_trace()) {
    probe_ = std::make_unique<kratos::service::RpcProbeImpl>(config_.get());
  }
  if (config_->has_attribute("service_layer.balance_type")) {
    auto balance_type = config_->get_string("service_layer.balance_type");
    service_layer_->set_balance_type(balance_type);
  }
  write_info_log("Initialize successfully");
  return true;
}
auto CppRuntime::get_probe() -> kratos::service::RpcProbeImpl * {
  return probe_.get();
}
#else
auto CppRuntime::init_sdk(const char *config_file_path) -> bool {
  config_ = std::make_unique<kratos::config::BoxConfigImpl>(nullptr);
  std::string config_error;
  if (!config_->load(config_file_path, config_error)) {
    write_error_log(config_error);
    return false;
  }
  if (!config_->has_attribute("rpc_root_dir")) {
    write_error_log("Need rpc_root_dir attribute but not found");
    return false;
  }
  if (config_->has_attribute("python_sdk_reconn_timeout")) {
    reconn_timeout_ =
        config_->get_number<std::time_t>("python_sdk_reconn_timeout");
  }
  if (!start()) {
    write_error_log("Start network failed");
    return false;
  }
  auto rpc_root_dir = config_->get_string("rpc_root_dir");
  if (!python::global_msg_factory.load(rpc_root_dir, rpc_root_dir)) {
    write_error_log("Apply RPC configuration failed");
    return false;
  }
  write_info_log("Initialize successfully");
  return true;
}

auto CppRuntime::connect_proxy(const std::vector<std::string> &hosts) -> bool {
  if (hosts.empty()) {
    return false;
  }
  is_connecting_ = true;
  proxy_hosts_ = hosts;
  auto index = util::get_random_uint32(0, std::uint32_t(hosts.size() - 1));
  std::string host;
  std::int32_t port{0};
  if (!util::get_host_config(hosts[index], host, port)) {
    write_error_log("Invalid proxy host format");
    return false;
  }
  auto type = util::get_host_type(hosts[index]);
  return connect_to("cluster_proxy-" + host, type, host, port, 3);
}

auto CppRuntime::get_channel(const std::string & /*service_name*/)
    -> std::uint64_t {
  if (proxy_channel_) {
    return proxy_channel_->get_id();
  }
  return 0;
}

auto CppRuntime::close_channel() -> void {
  if (proxy_channel_) {
    proxy_channel_->close();
  }
}
auto CppRuntime::check_connection(std::time_t now) -> void {
  if (proxy_channel_ || is_connecting_) {
    return;
  }
  if (!last_reconn_ms_) {
    last_reconn_ms_ = now;
  }
  if (now - last_reconn_ms_ < reconn_timeout_) {
    return;
  }
  last_reconn_ms_ = now;
  auto temp_hosts = proxy_hosts_;
  connect_proxy(temp_hosts);
}
#endif // PYTHON_SDK

auto CppRuntime::deinit(bool py_clean) -> void {
  if (py_log_cb_) {
    if (py_clean) {
      Py_XDECREF(py_log_cb_);
    }
    py_log_cb_ = nullptr;
  }

#ifndef PYTHON_SDK
  //
  // 取消注册
  //
  unregister_all_service();
#endif // PYTHON_SDK

  //
  // 关闭网络
  //
  stop();
  //
  // 清理
  //
  if (py_clean) {
    for (auto *py_obj : pack_list_) {
      Py_XDECREF(py_obj);
    }
  }
  pack_list_.clear();

#ifndef PYTHON_SDK
  if (service_register_) {
    service_register_->stop();
    service_register_.reset();
  }
  if (service_finder_) {
    service_finder_->stop();
    service_finder_.reset();
  }
#else
  proxy_hosts_.clear();
  python::global_msg_factory.deinit();
#endif // PYTHON_SDK
}

auto CppRuntime::set_log_cb(PyObject *log_cb_obj) -> void {
  if (!PyCallable_Check(log_cb_obj)) {
    return;
  }
  Py_XINCREF(log_cb_obj);
  py_log_cb_ = log_cb_obj;
}

auto CppRuntime::on_proxy_call(std::uint64_t channel_id,
                               rpc::RpcProxyCallRequestHeader *prx_call_req_ptr,
                               const char *data, int length) -> PyObject * {
  auto *obj_ptr = deserialize_arg(data, length, prx_call_req_ptr->serviceUUID,
                                  prx_call_req_ptr->methodID);
  if (!obj_ptr) {
    Py_RETURN_NONE;
  }
  SET_ATTR_STRING_XDECREF(
      this, obj_ptr, "MSGID",
      PyLong_FromLong((long)rpc::RpcMsgType::RPC_PROXY_CALL));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                          PyLong_FromUnsignedLongLong(channel_id));
  SET_ATTR_STRING_XDECREF(
      this, obj_ptr, "UUID",
      PyLong_FromUnsignedLongLong(prx_call_req_ptr->serviceUUID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "SERVICEID",
                          PyLong_FromUnsignedLong(prx_call_req_ptr->serviceID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "METHODID",
                          PyLong_FromUnsignedLong(prx_call_req_ptr->methodID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CALLID",
                          PyLong_FromUnsignedLong(prx_call_req_ptr->callID));
  SET_ATTR_STRING_XDECREF(
      this, obj_ptr, "GLOBALINDEX",
      PyLong_FromUnsignedLong(prx_call_req_ptr->globalIndex));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "ONEWAY",
                          PyLong_FromLong((long)prx_call_req_ptr->oneWay));
  return obj_ptr;
}

auto CppRuntime::on_proxy_call_ret(std::uint64_t channel_id,
                                   rpc::RpcProxyCallRetHeader *prx_call_ret_ptr,
                                   const char *data, int length) -> PyObject * {
  const auto &call_info = get_call_info(prx_call_ret_ptr->callID);
  if (!call_info) {
    Py_RETURN_NONE;
  }
  auto *obj_ptr =
      deserialize_ret(data, length, call_info.uuid, call_info.method_id);
  if (!obj_ptr) {
    Py_RETURN_NONE;
  }
  call_info_map_.erase(prx_call_ret_ptr->callID);
  SET_ATTR_STRING_XDECREF(
      this, obj_ptr, "MSGID",
      PyLong_FromLong((long)rpc::RpcMsgType::RPC_PROXY_RETURN));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                          PyLong_FromUnsignedLongLong(channel_id));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "SERVICEID",
                          PyLong_FromUnsignedLong(prx_call_ret_ptr->serviceID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CALLID",
                          PyLong_FromUnsignedLong(prx_call_ret_ptr->callID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "ERRORID",
                          PyLong_FromUnsignedLong(prx_call_ret_ptr->errorID));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "GLOBALINDEX",
                          PyLong_FromLong((long)prx_call_ret_ptr->globalIndex));
  return obj_ptr;
}

auto CppRuntime::on_sub(std::uint64_t channel_id,
                        rpc::RpcSubHeader *sub_header_ptr, const char *data,
                        int length) -> PyObject * {
  std::string sub_id(sub_header_ptr->sub_id, rpc::SUB_ID_LEN);
  std::string evt_name(data, sub_header_ptr->name_length);
  auto *method_info = python::global_msg_factory.get_type(
      sub_header_ptr->serviceUUID, evt_name);
  if (method_info) {
    auto *obj_ptr = deserialize_arg(
        data + sub_header_ptr->name_length, sub_header_ptr->data_length,
        sub_header_ptr->serviceUUID, method_info->method_id);
    if (!obj_ptr) {
      Py_RETURN_NONE;
    }
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "MSGID",
        PyLong_FromLong((long)rpc::RpcMsgType::RPC_EVENT_SUB));
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "UUID",
        PyLong_FromUnsignedLongLong(sub_header_ptr->serviceUUID));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                            PyLong_FromUnsignedLongLong(channel_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "SERVICEID",
                            PyLong_FromUnsignedLong(sub_header_ptr->serviceID));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "PROXYID",
                            PyLong_FromUnsignedLong(sub_header_ptr->proxy_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "SUBID",
                            PyUnicode_FromString(sub_id.c_str()));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "EVENT",
                            PyUnicode_FromString(evt_name.c_str()));
    sub_info_map_[sub_id] = SubInfo{channel_id,
                                    sub_header_ptr->serviceUUID,
                                    method_info->method_id,
                                    sub_header_ptr->serviceID,
                                    evt_name,
                                    sub_header_ptr->proxy_id};
    return obj_ptr;
  } else {
    auto *obj_ptr = PyDict_New();
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "MSGID",
        PyLong_FromLong((long)rpc::RpcMsgType::RPC_EVENT_SUB));
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "UUID",
        PyLong_FromUnsignedLongLong(sub_header_ptr->serviceUUID));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                            PyLong_FromUnsignedLongLong(channel_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "SERVICEID",
                            PyLong_FromUnsignedLong(sub_header_ptr->serviceID));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "PROXYID",
                            PyLong_FromUnsignedLong(sub_header_ptr->proxy_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "SUBID",
                            PyUnicode_FromString(sub_id.c_str()));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "EVENT",
                            PyUnicode_FromString(evt_name.c_str()));
    auto *data_obj_ptr = PyUnicode_FromStringAndSize(
        data + sub_header_ptr->name_length, sub_header_ptr->data_length);
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "DATA", data_obj_ptr);
    sub_info_map_[sub_id] = SubInfo{channel_id, sub_header_ptr->serviceUUID,
                                    0,          sub_header_ptr->serviceID,
                                    evt_name,   sub_header_ptr->proxy_id};
    return obj_ptr;
  }
}

auto CppRuntime::on_cancel(std::uint64_t channel_id,
                           rpc::RpcCancelSubHeader *cancel_header_ptr)
    -> PyObject * {
  // TODO 连接断开清理订阅
  std::string sub_id(cancel_header_ptr->sub_id, rpc::SUB_ID_LEN);
  auto *obj_ptr = PyDict_New();
  SET_ATTR_STRING_XDECREF(
      this, obj_ptr, "MSGID",
      PyLong_FromLong((long)rpc::RpcMsgType::RPC_EVENT_CANCEL));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                          PyLong_FromUnsignedLongLong(channel_id));
  SET_ATTR_STRING_XDECREF(this, obj_ptr, "SUBID",
                          PyUnicode_FromString(sub_id.c_str()));
  sub_info_map_.erase(sub_id);
  return obj_ptr;
}

auto CppRuntime::on_pub(std::uint64_t channel_id,
                        rpc::RpcPubHeader *pub_header_ptr, const char *data,
                        int length) -> PyObject * {
  std::string sub_id(pub_header_ptr->sub_id, rpc::SUB_ID_LEN);
  auto info_it = sub_info_map_.find(sub_id);
  if (info_it == sub_info_map_.end()) {
    Py_RETURN_NONE;
  }
  if (info_it->second.method_id != rpc::INVALID_METHOD_ID) {
    auto *method_info = python::global_msg_factory.get_type(
        info_it->second.uuid, info_it->second.evt_name);
    if (!method_info) {
      Py_RETURN_NONE;
    }
    auto *obj_ptr =
        deserialize_ret(data, pub_header_ptr->value_length,
                        info_it->second.uuid, method_info->method_id);
    if (!obj_ptr) {
      Py_RETURN_NONE;
    }
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "MSGID",
        PyLong_FromLong((long)rpc::RpcMsgType::RPC_EVENT_PUB));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                            PyLong_FromUnsignedLongLong(channel_id));
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "SERVICEID",
        PyLong_FromUnsignedLong(info_it->second.service_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "PROXYID",
                            PyLong_FromUnsignedLong(info_it->second.proxy_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "SUBID",
                            PyUnicode_FromString(sub_id.c_str()));
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "EVENT",
        PyUnicode_FromString(info_it->second.evt_name.c_str()));
    return obj_ptr;
  } else {
    auto *obj_ptr = PyDict_New();
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "MSGID",
        PyLong_FromLong((long)rpc::RpcMsgType::RPC_EVENT_PUB));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "CHANNELID",
                            PyLong_FromUnsignedLongLong(channel_id));
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "SERVICEID",
        PyLong_FromUnsignedLong(info_it->second.service_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "PROXYID",
                            PyLong_FromUnsignedLong(info_it->second.proxy_id));
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "SUBID",
                            PyUnicode_FromString(sub_id.c_str()));
    SET_ATTR_STRING_XDECREF(
        this, obj_ptr, "EVENT",
        PyUnicode_FromString(info_it->second.evt_name.c_str()));
    auto *data_obj_ptr =
        PyUnicode_FromStringAndSize(data, pub_header_ptr->value_length);
    SET_ATTR_STRING_XDECREF(this, obj_ptr, "DATA", data_obj_ptr);
    return obj_ptr;
  }
}

auto CppRuntime::deserialize_arg(const char *data, int size,
                                 rpc::ServiceUUID uuid, rpc::MethodID method_id)
    -> PyObject * {
  auto *msg_ptr = get_msg_arg(uuid, method_id);
  if (!msg_ptr) {
    Py_RETURN_NONE;
  }
  if (!msg_ptr->ParseFromArray(data, size)) {
    Py_RETURN_NONE;
  }
  return coder_.decode(*msg_ptr);
}

auto CppRuntime::set_exec_info(ProtobufMessage *msg_ptr, PyObject *ret_dict)
    -> void {
  for (int i = 0; i < msg_ptr->GetDescriptor()->field_count(); ++i) {
    auto *field = msg_ptr->GetDescriptor()->field(i);
    if (field->name() != "exec_info") {
      continue;
    }
    if (msg_ptr->GetReflection()->HasField(*msg_ptr, field)) {
      auto *exec_info =
          msg_ptr->GetReflection()->MutableMessage(msg_ptr, field);
      auto *ref = exec_info->GetReflection();
      auto *desc = exec_info->GetDescriptor();
      // name
      if (ref->HasField(*exec_info, desc->field(0))) {
        SET_ATTR_STRING_XDECREF(
            this, ret_dict, "EXECNAME",
            PyUnicode_FromString(
                ref->GetString(*exec_info, desc->field(0)).c_str()));
      }
      // detail
      if (ref->HasField(*exec_info, desc->field(1))) {
        SET_ATTR_STRING_XDECREF(
            this, ret_dict, "EXECDETAIL",
            PyUnicode_FromString(
                ref->GetString(*exec_info, desc->field(1)).c_str()));
      }
      break;
    }
  }
}

auto CppRuntime::deserialize_ret(const char *data, int size,
                                 rpc::ServiceUUID uuid, rpc::MethodID method_id,
                                 rpc::ErrorID error_id) -> PyObject * {
  auto *msg_ptr = get_msg_ret(uuid, method_id);
  if (!msg_ptr) {
    Py_RETURN_NONE;
  }
  if (!msg_ptr->ParseFromArray(data, size)) {
    Py_RETURN_NONE;
  }
  auto *ret_dict = coder_.decode(*msg_ptr);
  if (error_id == rpc::ErrorID(rpc::RpcError::EXCEPTION)) {
    set_exec_info(msg_ptr, ret_dict);
  }
  return ret_dict;
}

auto CppRuntime::get_buffer_ptr(std::size_t length) -> char * {
  if (!char_buffer_ || buffer_length_ < length) {
    char_buffer_.reset(new char[length]);
    buffer_length_ = length;
  }
  return char_buffer_.get();
}

auto CppRuntime::set_call_info(rpc::CallID call_id, rpc::ServiceUUID uuid,
                               rpc::MethodID method_id,
                               rpc::ServiceID service_id, std::time_t timeout)
    -> void {
  if (config_->is_open_rpc_timeout()) {
    auto timer_id = sched_ptr_->schedule(timeout, timer_cb_func_, call_id);
    auto call_info = CallInfo{util::get_os_time_millionsecond() + timeout,
                              service_id, uuid, method_id, timer_id};
    call_info_map_.emplace(call_id, std::move(call_info));
  } else {
    auto call_info = CallInfo{util::get_os_time_millionsecond() + timeout,
                              service_id, uuid, method_id, 0};
    call_info_map_.emplace(call_id, std::move(call_info));
  }
}

auto CppRuntime::get_call_info(rpc::CallID call_id) -> const CallInfo & {
  static CallInfo NullCallInfo;
  auto it = call_info_map_.find(call_id);
  if (it == call_info_map_.end()) {
    return NullCallInfo;
  }
  return it->second;
}

auto CppRuntime::get_msg_arg(rpc::ServiceUUID uuid, rpc::MethodID method_id)
    -> ProtobufMessage * {
  return python::global_msg_factory.new_call_param(uuid, method_id);
}

auto CppRuntime::get_msg_ret(rpc::ServiceUUID uuid, rpc::MethodID method_id)
    -> ProtobufMessage * {
  return python::global_msg_factory.new_call_return(uuid, method_id);
}

#ifndef PYTHON_SDK
auto CppRuntime::on_get_trace_info(ProtobufMessage &msg, PyObject *obj_ptr)
    -> void {
  if (config_->is_open_trace()) {
    std::string trace_id;
    std::uint64_t span_id{0};
    std::uint64_t parent_span_id{0};
    kratos::get_trace_info(msg, trace_id, span_id, parent_span_id);
    if (!trace_id.empty()) {
      SET_ATTR_STRING_XDECREF(this, obj_ptr, "TRACEID",
                              PyUnicode_FromString(trace_id.c_str()));
      SET_ATTR_STRING_XDECREF(this, obj_ptr, "SPANID",
                              PyLong_FromUnsignedLongLong(span_id));
      SET_ATTR_STRING_XDECREF(this, obj_ptr, "PARENTSPANID",
                              PyLong_FromUnsignedLongLong(parent_span_id));
    }
  }
}
#endif // !PYTHON_SDK

auto CppRuntime::get_service_layer() -> kratos::service::ServiceLayer * {
#ifndef PYTHON_SDK
  if (service_layer_) {
    return service_layer_.get();
  }
  return nullptr;
#else
  return nullptr;
#endif // PYTHON_SDK
}

auto CppRuntime::timer_cb(std::uint64_t call_id, std::time_t ms) -> bool {
  auto it = call_info_map_.find((rpc::CallID)call_id);
  if (it == call_info_map_.end()) {
    return true;
  }
  //
  // 建立并记录超时事件
  //
  auto *obj_ptr =
      new_call_timeout_pack(it->second.service_id, (rpc::CallID)call_id);
  if (obj_ptr) {
    Py_XINCREF(obj_ptr);
    pack_list_.push_back(obj_ptr);
  }
  return true;
}

auto CppRuntime::external_timer_cb(std::uint64_t usr_data, std::time_t ms)
    -> bool {
  //
  // 建立并记录超时事件
  //
  auto *obj_ptr = new_timeout_pack(usr_data);
  if (obj_ptr) {
    Py_XINCREF(obj_ptr);
    pack_list_.push_back(obj_ptr);
  }
  return true;
}

auto CppRuntime::write_log(const std::string &log_line) -> void {
  auto real_log_line = "[rpc]" + log_line;
  if (py_log_cb_) {
    auto *str_obj = PyUnicode_FromString(real_log_line.c_str());
    auto *ret_obj = PyObject_CallFunctionObjArgs(py_log_cb_, str_obj, nullptr);
    if (ret_obj) {
      Py_XDECREF(ret_obj);
    }
    Py_XDECREF(str_obj);
  } else {
    std::cerr << real_log_line << std::endl;
  }
}

auto CppRuntime::write_error_log(const std::string &log_line) -> void {
  write_log("[erro]" + log_line);
}

auto CppRuntime::write_info_log(const std::string &log_line) -> void {
  write_log("[info]" + log_line);
}

auto CppRuntime::new_call_timeout_pack(rpc::ServiceID service_id,
                                       rpc::CallID call_id) -> PyObject * {
  auto *msg_obj_ptr = PyDict_New();
  if (!msg_obj_ptr) {
    Py_RETURN_NONE;
  }
  SET_ATTR_STRING_XDECREF(this, msg_obj_ptr, "MSGID",
                          PyLong_FromLong((long)rpc::RpcMsgType::RPC_RETURN));
  SET_ATTR_STRING_XDECREF(this, msg_obj_ptr, "SERVICEID",
                          PyLong_FromUnsignedLong(service_id));
  SET_ATTR_STRING_XDECREF(this, msg_obj_ptr, "CALLID",
                          PyLong_FromUnsignedLong(call_id));
  SET_ATTR_STRING_XDECREF(
      this, msg_obj_ptr, "ERRORID",
      PyLong_FromUnsignedLong((unsigned long)rpc::RpcError::TIMEOUT));
  return msg_obj_ptr;
}

auto CppRuntime::new_timeout_pack(std::uint64_t usr_data) -> PyObject * {
  auto *msg_obj_ptr = PyDict_New();
  if (!msg_obj_ptr) {
    Py_RETURN_NONE;
  }
  SET_ATTR_STRING_XDECREF(this, msg_obj_ptr, "MSGID",
                          PyLong_FromLong((long)rpc::RpcMsgType::NO_RPC));
  SET_ATTR_STRING_XDECREF(
      this, msg_obj_ptr, "USRDATA",
      PyLong_FromUnsignedLongLong((unsigned long long)usr_data));
  SET_ATTR_STRING_XDECREF(
      this, msg_obj_ptr, "ERRORID",
      PyLong_FromUnsignedLong((unsigned long)rpc::RpcError::TIMEOUT));
  return msg_obj_ptr;
}

auto CppRuntime::new_timer(std::time_t timeout, std::uint64_t usr_data)
    -> PyObject * {
  auto timer_id = sched_ptr_->schedule(timeout, ext_timer_cb_func_, usr_data);
  if (!timer_id) {
    Py_RETURN_FALSE;
  }
  Py_RETURN_TRUE;
}

auto CppRuntime::create_uuid() -> PyObject * {
  auto uuid = util::create_uuid();
  return PyUnicode_FromString(uuid.c_str());
}

} // namespace util
} // namespace kratos
