﻿#pragma once

#include "box/service_context.hh"
#include <cstddef>
#include <memory>
#include <stdexcept>
#include <unordered_map>

namespace kratos {
namespace util {

// 协程全局锁管理器
struct GlobalCoLockManager {
  GlobalCoLockManager(GlobalCoLockManager &&) = delete;
  GlobalCoLockManager(const GlobalCoLockManager &) = delete;
  GlobalCoLockManager &operator=(const GlobalCoLockManager &) = delete;
  GlobalCoLockManager &operator=(GlobalCoLockManager &&) = delete;
  GlobalCoLockManager() {}
  ~GlobalCoLockManager() {
    if (ctx) {
      // 关闭所有协程
      for (auto &[addr, coid] : key_addr_map) {
        (void)addr;
        ctx->get_rpc()->coro_close(coid);
      }
      key_addr_map.clear();
    }
  }
  auto set_context(kratos::service::ServiceContext *srv_ctx) -> void {
    ctx = srv_ctx;
  }
  // 获取当前线程内协程锁数量
  auto co_lock_count() -> std::size_t {
    return key_addr_map.size();
  }
  using KeyAddrMap = std::unordered_map<std::ptrdiff_t, coroutine::CoroID>;
  KeyAddrMap key_addr_map;                       ///< { address, coroutine ID }
  kratos::service::ServiceContext *ctx{nullptr}; ///< context
};

// 协程全局锁管理器
inline static thread_local GlobalCoLockManager colock_manager;
// 锁定后唤醒间隔, 毫秒
inline static thread_local std::time_t co_lock_intval = 30;
// 设置锁定后唤醒间隔, 毫秒, 如果intval < 帧间隔, 则按帧间隔唤醒
inline static auto set_co_lock_wake_intval(std::time_t intval) {
  co_lock_intval = intval;
}

// 获取当前线程内协程锁数量
inline static auto co_lock_count() -> std::size_t {
  return colock_manager.co_lock_count();
}

//
// 协程锁, 用来保护某一个地址, 当有多个协程访问同一个地址时抛出异常
// 调用者需要确保地址的有效性
//
template <typename T> class CoLock {
  std::ptrdiff_t ptr_addr_{0};                          ///< 受保护的地址
  coroutine::CoroID co_id_{coroutine::INVALID_CORO_ID}; ///< 当前持有的协程ID
  kratos::service::ServiceContext *ctx_{nullptr};       ///< 上下文

public:
  CoLock(kratos::service::ServiceContext *ctx, T *ptr) {
    colock_manager.set_context(ctx);
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(ptr);
    co_id_ = ctx->get_rpc()->coro_id();
    ctx_ = ctx;
  }

  CoLock(kratos::service::ServiceContext *ctx, T &ptr) {
    colock_manager.set_context(ctx);
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(&ptr);
    co_id_ = ctx->get_rpc()->coro_id();
    ctx_ = ctx;
  }

  CoLock(kratos::service::ServiceContext *ctx, std::shared_ptr<T> ptr) {
    colock_manager.set_context(ctx);
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(ptr.get());
    co_id_ = ctx->get_rpc()->coro_id();
    ctx_ = ctx;
  }

  CoLock(kratos::service::ServiceContext *ctx, std::unique_ptr<T> ptr) {
    colock_manager.set_context(ctx);
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(ptr.get());
    co_id_ = ctx->get_rpc()->coro_id();
    ctx_ = ctx;
  }

  CoLock(coroutine::CoroID co_id, T *ptr) {
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(ptr);
    co_id_ = co_id;
  }

  CoLock(coroutine::CoroID co_id, T &ptr) {
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(&ptr);
    co_id_ = co_id;
  }

  CoLock(coroutine::CoroID co_id, std::shared_ptr<T> ptr) {
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(ptr.get());
    co_id_ = co_id;
  }

  CoLock(coroutine::CoroID co_id, std::unique_ptr<T> ptr) {
    ptr_addr_ = reinterpret_cast<decltype(ptr_addr_)>(ptr.get());
    co_id_ = co_id;
  }

  ~CoLock() { colock_manager.key_addr_map.erase(ptr_addr_); }

  //
  // 检查地址是否被锁定, 如果被锁定则抛出异常
  //
  // @return 返回地址指针
  //
  T *own() {
    if (!ptr_addr_) {
      throw std::runtime_error("Null pointer");
    }
    auto it = colock_manager.key_addr_map.find(ptr_addr_);
    if (it != colock_manager.key_addr_map.end()) {
      if (it->second != co_id_) {
        throw std::runtime_error("Address use in another coroutine");
      }
    }
    colock_manager.key_addr_map[ptr_addr_] = co_id_;
    return reinterpret_cast<T *>(ptr_addr_);
  }

  //
  // 检查地址是否被锁定, 如果被锁定则抛出异常
  //
  // @return 返回引用
  //
  T &own_ref() { return *own(); }

  //
  // 检查地址是否被锁定, 如果被锁定则等待
  //
  // @return 返回地址指针
  //
  T *own_wait() {
    if (!ptr_addr_) {
      throw std::runtime_error("Null pointer");
    }
    while (true) {
      auto it = colock_manager.key_addr_map.find(ptr_addr_);
      if (it != colock_manager.key_addr_map.end()) {
        if (it->second != co_id_) {
          // 睡眠一个间隔后唤醒检查条件
          ctx_->sleep_co(co_lock_intval);
        } else {
          break;
        }
      } else {
        break;
      }
    }
    colock_manager.key_addr_map[ptr_addr_] = co_id_;
    return reinterpret_cast<T *>(ptr_addr_);
  }

  //
  // 检查地址是否被锁定, 如果被锁定则等待
  //
  // @return 返回引用
  //
  T &own_wait_ref() { return *own_wait(); }
};

} // namespace util
} // namespace kratos
