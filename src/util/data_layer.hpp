﻿#pragma once

#include "box/service_context.hh"
#include "co_manager/co_manager.h"
#include "json/json.h"
#include <bitset>
#include <chrono>
#include <cstdint>
#include <fstream>
#include <initializer_list>
#include <memory>
#include <stdexcept>
#include <tuple>
#include <unordered_map>
#include <vector>

/*
 *
 * 编译注意事项:
 * 1. 需要添加源文件jsoncpp下的源文件
 *
 * 包装类要求:
 *
 * 需要指定要求的静态方法:
 * 1. static auto get_config() -> const std::string&;
 *    获取数据配置字符串
 * 2. static auto get_type_name() -> const std::string&;
 *    获取类型名
 * 3. static auto index_name() -> std::string;
 *    获取索引名
 * 4. static auto index_type() -> std::string;
 *    获取索引类型
 *
 * 需要指定要求的方法:
 * 1. 无参数构造函数
 *    建立包装的DbObjectPtr
 * 2. auto get_obj_ptr() -> kratos::util::DbObjectPtr;
 *    获取包装的DbObjectPtr
 */

namespace kratos {
namespace util {

class DbQuery;
class DbFieldBase;
struct DbObject;
class DbManager;
class IntField;
class UIntField;
class Int64Field;
class UInt64Field;
class StringField;
class FloatField;
class DoubleField;

using FieldPtr = std::shared_ptr<DbFieldBase>;
using DbObjectPtr = std::shared_ptr<DbObject>;
using ObjPtrVector = std::vector<DbObjectPtr>;
using ValuePtr = std::shared_ptr<Json::Value>;
using ConfigMap = std::unordered_map<std::string, ValuePtr>;
using DbObjMap = std::unordered_map<std::uint64_t, DbObjectPtr>;

/**
 * 数据字段基类.
 */
class DbFieldBase {
public:
  virtual ~DbFieldBase() {}
  /**
   * 获取字段名字.
   *
   * \return
   */
  virtual auto get_name() -> const std::string & = 0;
  /**
   * 检测字段是否有数据修改.
   *
   * \return
   */
  virtual auto is_dirty() -> bool = 0;
  /**
   * 转为JSON数值.
   *
   * \return
   */
  virtual auto to_json() -> std::string = 0;
  /**
   * 从JSON串转为数据.
   *
   * \param value
   * \return
   */
  virtual auto from_json(const std::string &value) -> void = 0;
  /**
   * 获取字段类型.
   *
   * \return
   */
  virtual auto get_type() -> const std::string & = 0;
  /**
   * 设置字段名.
   *
   * \param name
   * \return
   */
  virtual auto set_name(const std::string &name) -> void = 0;
};

/**
 * 数据字段基础实现类.
 */
template <typename T> class DbField : public DbFieldBase {
  bool dirty_{false}; ///< 脏标记

  DbField(const DbField &) = delete;
  DbField(DbField &&) = delete;

protected:
  T t_;              ///< 数据
  std::string type_; ///< 数据类型
  std::string name_; ///< 字段名

public:
  virtual ~DbField() {}

  /**
   * 返回const引用.
   *
   * \return
   */
  const T &use() { return t_; }

  /**
   * 返回可变引用.
   *
   * \return
   */
  T &mut() {
    dirty_ = true;
    return t_;
  }

  /**
   * 返回可变引用, 但不设置脏标记.
   *
   * \return
   */
  T &mut_no_dirty() { return t_; }

  virtual auto get_name() -> const std::string & override { return name_; }

  virtual auto is_dirty() -> bool override { return dirty_; }

  virtual auto get_type() -> const std::string & override { return type_; }

  virtual auto set_name(const std::string &name) -> void override {
    name_ = name;
  }

protected:
  DbField() {}
};

class IntField : public DbField<std::int32_t> {
public:
  IntField() {
    t_ = 0;
    type_ = "int";
  }
  virtual ~IntField() {}
  virtual auto to_json() -> std::string override { return std::to_string(t_); }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = std::stoi(value);
  }
  inline static auto create() -> FieldPtr {
    auto ptr = std::make_shared<IntField>();
    ptr->mut_no_dirty() = 0;
    return ptr;
  }
};

class Int64Field : public DbField<std::int64_t> {
public:
  Int64Field() {
    t_ = 0ll;
    type_ = "int64";
  }
  virtual ~Int64Field() {}
  virtual auto to_json() -> std::string override { return std::to_string(t_); }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = std::stoll(value);
  }
  inline static auto create() -> FieldPtr {
    auto ptr = std::make_shared<Int64Field>();
    ptr->mut_no_dirty() = 0;
    return ptr;
  }
};

class UIntField : public DbField<std::uint32_t> {
public:
  UIntField() {
    t_ = 0;
    type_ = "uint";
  }
  virtual ~UIntField() {}
  virtual auto to_json() -> std::string override { return std::to_string(t_); }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = std::stoul(value);
  }
  inline static auto create() -> FieldPtr {
    auto ptr = std::make_shared<UIntField>();
    ptr->mut_no_dirty() = 0u;
    return ptr;
  }
};

class UInt64Field : public DbField<std::uint64_t> {
public:
  UInt64Field() {
    t_ = 0ull;
    type_ = "uint64";
  }
  virtual ~UInt64Field() {}
  virtual auto to_json() -> std::string override { return std::to_string(t_); }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = std::stoull(value);
  }
  inline static auto create() -> FieldPtr {
    auto ptr = std::make_shared<UInt64Field>();
    ptr->mut_no_dirty() = 0ull;
    return ptr;
  }
};

class StringField : public DbField<std::string> {
public:
  StringField() { type_ = "string"; }
  virtual ~StringField() {}
  virtual auto to_json() -> std::string override { return t_; }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = value;
  }
  inline static auto create() -> FieldPtr {
    return std::make_shared<StringField>();
  }
};

class FloatField : public DbField<float> {
public:
  FloatField() {
    t_ = 0.f;
    type_ = "float";
  }
  virtual ~FloatField() {}
  virtual auto to_json() -> std::string override { return std::to_string(t_); }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = std::stof(value);
  }
  inline static auto create() -> FieldPtr {
    auto ptr = std::make_shared<FloatField>();
    ptr->mut_no_dirty() = .0f;
    return ptr;
  }
};

class DoubleField : public DbField<double> {
public:
  DoubleField() {
    t_ = .0;
    type_ = "double";
  }
  virtual ~DoubleField() {}
  virtual auto to_json() -> std::string override { return std::to_string(t_); }
  virtual auto from_json(const std::string &value) -> void override {
    t_ = std::stod(value);
  }
  inline static auto create() -> FieldPtr {
    auto ptr = std::make_shared<DoubleField>();
    ptr->mut_no_dirty() = .0;
    return ptr;
  }
};

/**
 * 数据管理器.
 */
class DbManager {
public:
  /**
   * 初始化.
   *
   * \param ctx 服务上下文
   *
   * \return
   */
  inline auto init(kratos::service::ServiceContext *ctx) -> void {
    co_mgr_ptr_ = ctx->new_co_manager();
    ctx_ = ctx;
  }
  /**
   * 清理.
   */
  inline auto cleanup() -> void {
    co_mgr_ptr_.reset();
    db_obj_map_.clear();
    config_map_.clear();
  }
  /**
   * 保存脏对象.
   *
   * \param prx 数据服务代理
   * \param binding 是否使用绑定代理
   * \return
   */
  template <typename T>
  inline auto update(std::shared_ptr<T> prx, bool binding = false) -> void;
  /**
   * 删除数据对象.
   *
   * \param id 对象ID
   * \return
   */
  inline auto remove(std::uint64_t id) -> void { db_obj_map_.erase(id); }
  /**
   * 添加数据对象.
   *
   * \param obj_ptr 对象指针
   * \return
   */
  inline auto add(DbObjectPtr obj_ptr) -> void;
  /**
   * 获取上下文.
   */
  inline auto get_context() -> kratos::service::ServiceContext * {
    return ctx_;
  }
  /**
   * 获取配置快查表.
   *
   * \return
   */
  inline auto get_config_map() -> ConfigMap & { return config_map_; }
  /**
   * 获取数据对象表.
   *
   * \return
   */
  inline auto get_db_obj_map() -> DbObjMap & { return db_obj_map_; }
  /**
   * 获取数据对象.
   *
   * \param id 对象ID
   * \return 数据对象
   */
  inline auto get_db_object(std::uint64_t id) -> DbObjectPtr {
    auto it = db_obj_map_.find(id);
    if (it == db_obj_map_.end()) {
      return nullptr;
    }
    return it->second;
  }
  /**
   * 获取数据对象的数量.
   *
   */
  inline auto get_obj_count() -> std::size_t { return db_obj_map_.size(); }

private:
  friend struct DbObject;
  kratos::service::ServiceContext *ctx_{nullptr}; ///< 上下文
  kratos::service::CoManagerPtr co_mgr_ptr_;      ///< 协程管理器
  DbObjMap db_obj_map_;                           ///< 数据对象字典
  std::uint64_t id_{1};                           ///< 数据对象ID池
  ConfigMap config_map_;                          ///< 全局配置.
};

/**
 * 获取全局数据对象管理器
 */
inline static auto get_db_manager() -> DbManager & {
  static DbManager db_obj_mgr_;
  return db_obj_mgr_;
}
/**
 * 获取配置的JSON对象
 *
 * \param json_string JSON字符串
 * \return JSON对象指针
 */
inline static auto get_json_object(const std::string &json_string) -> ValuePtr {
  Json::CharReaderBuilder builder;
  std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
  const auto *start = json_string.c_str();
  const auto *end = json_string.c_str() + json_string.size();
  auto value_ptr = std::make_shared<Json::Value>();
  std::string error;
  if (!reader->parse(start, end, value_ptr.get(), &error)) {
    return nullptr;
  }
  return value_ptr;
}
/**
 * 获取配置的JSON对象
 *
 * \param config JSON配置文件
 * \return JSON对象指针
 */
inline static auto get_json_string(const std::string &config) -> ValuePtr {
  auto &config_map = get_db_manager().get_config_map();
  auto it = config_map.find(config);
  if (it != config_map.end()) {
    return it->second;
  }
  std::ifstream ifs;
  ifs.open(config, std::ios::in);
  if (!ifs) {
    return nullptr;
  }
  std::string json_string;
  std::string line;
  while (!ifs.eof()) {
    ifs >> line;
    json_string += line;
  }
  ifs.close();
  auto value_ptr = get_json_object(json_string);
  if (!value_ptr) {
    return nullptr;
  }
  config_map[config] = value_ptr;
  return value_ptr;
}
/**
 * 数据对象.
 */
struct DbObject : public std::enable_shared_from_this<DbObject> {
  DbObject(const DbObject &) = delete;
  DbObject(DbObject &&) = delete;
  const DbObject &operator=(const DbObject &) = delete;
  const DbObject &operator=(DbObject &&) = delete;

protected:
  friend class DbQuery;
  friend class DbManager;
  using FieldMap = std::unordered_map<std::string, FieldPtr>;
  using FieldIndexVector = std::vector<FieldPtr>;
  FieldMap field_map_;         ///< 属性字典, {属性名, 属性指针}
  FieldIndexVector field_vec_; ///< 属性数组
  std::string type_name_;      ///< 类型名
  bool dirty_{false};          ///< 脏标记
  bool save_pending_{false};   ///< 正在保存中
  std::uint64_t id_{0};        ///< 对象ID, 用于对象管理器索引
  DbManager *db_obj_mgr_ptr_{nullptr};           ///< 数据对象管理器
  rpc::ProxyID proxy_id_{rpc::INVALID_PROXY_ID}; ///< 代理ID
  bool read_only_{false};                        ///< 是否只读
  bool new_obj_{false};                          ///< 是否是新建对象
  std::int32_t index_pos_{-1};                   ///< 对象索引名称
  std::int32_t expiration_{ 0 };                 ///< 过期时间

public:
  /**
   * 构造.
   */
  DbObject() {}
  /**
   * 构造
   *
   * \param type_name 数据类型名.
   */
  DbObject(const std::string &type_name) { type_name_ = type_name; }
  /**
   * 析构.
   */
  ~DbObject() {}
  /**
   * 添加数据字段
   *
   * \param field_ptr 数据字段
   * \return true or false
   */
  auto add_field(FieldPtr field_ptr) -> bool {
    field_map_[field_ptr->get_name()] = field_ptr;
    field_vec_.emplace_back(field_ptr);
    return true;
  }
  /**
   * 设置只读开关
   *
   * \param flag 开关, true表示只读, false表示可读写.
   */
  auto set_read_only(bool flag) -> void { read_only_ = flag; }
  /**
   * 获取只读开关.
   */
  auto is_read_only() -> bool { return read_only_; }
  /**
   * 获取上次操作的服务代理ID.
   *
   * \return 服务代理ID
   */
  auto get_proxy_id() -> rpc::ProxyID { return proxy_id_; }
  /**
   * 设置新对象标记, 将在到数据服务内新建
   *
   * \param flag true表示新建, false表示非新建
   */
  auto set_new_object(bool flag) -> void { new_obj_ = flag; }
  /**
   * 获取新建开关.
   */
  auto is_new_object() -> bool { return new_obj_; }
  /**
   * 设置过期时间.
   * 
   * \param expiration 过期时间
   */
  auto set_expiration(std::uint32_t expiration) -> void { expiration_ = expiration; }
  /**
   * 获取过期时间
   *
   * \return 过期时间
   */
  auto get_expiration() -> std::uint32_t { return expiration_; }
  /**
   * 设置对象属性.
   *
   * \param name 属性名
   * \param t 属性值
   * \return
   */
  template <typename T> auto set(const std::string &name, const T &t) -> void {
    if (read_only_) {
      return;
    }
    auto it = field_map_.find(name);
    if (it == field_map_.end()) {
      throw std::runtime_error("Field not found:" + name);
    }
    auto ptr = std::dynamic_pointer_cast<DbField<T>>(it->second);
    if (ptr) {
      ptr->mut() = t;
    } else {
      throw std::runtime_error("Field type invalid:" + name);
    }
    dirty_ = true;
  }
  /**
   * 设置对象属性, 不设置脏标记.
   *
   * \param name 属性名
   * \param t 属性值
   * \return
   */
  template <typename T>
  auto set_no_dirty(const std::string &name, const T &t) -> void {
    if (read_only_) {
      return;
    }
    auto it = field_map_.find(name);
    if (it == field_map_.end()) {
      throw std::runtime_error("Field not found:" + name);
    }
    auto ptr = std::dynamic_pointer_cast<DbField<T>>(it->second);
    if (ptr) {
      ptr->mut_no_dirty() = t;
    } else {
      throw std::runtime_error("Field type invalid:" + name);
    }
  }
  /**
   * 设置对象属性.
   *
   * \param index 属性索引
   * \param t 属性值
   * \return
   */
  template <typename T> auto set(std::size_t index, const T &t) -> void {
    if (read_only_) {
      return;
    }
    if (index >= field_vec_.size()) {
      return;
    }
    auto ptr = std::dynamic_pointer_cast<DbField<T>>(field_vec_[index]);
    if (ptr) {
      ptr->mut() = t;
    } else {
      throw std::runtime_error("Field type invalid");
    }
    dirty_ = true;
  }
  /**
   * 设置对象属性, 不设置脏标记.
   *
   * \param index 属性索引
   * \param t 属性值
   * \return
   */
  template <typename T>
  auto set_no_dirty(std::size_t index, const T &t) -> void {
    if (read_only_) {
      return;
    }
    if (index >= field_vec_.size()) {
      return;
    }
    auto ptr = std::dynamic_pointer_cast<DbField<T>>(field_vec_[index]);
    if (ptr) {
      ptr->mut_no_dirty() = t;
    } else {
      throw std::runtime_error("Field type invalid");
    }
  }
  /**
   * 获取属性不可变引用.
   *
   * \param name 属性名
   * \return 属性不可变引用
   */
  template <typename T> auto get(const std::string &name) -> const T & {
    auto it = field_map_.find(name);
    if (it == field_map_.end()) {
      throw std::runtime_error("Field not found:" + name);
    }
    auto ptr = std::dynamic_pointer_cast<DbField<T>>(it->second);
    if (ptr) {
      return ptr->use();
    } else {
      throw std::runtime_error("Field not found:" + name);
    }
  }
  /**
   * 获取属性不可变引用.
   *
   * \param index 属性索引
   * \return 属性不可变引用
   */
  template <typename T> auto get(std::size_t index) -> const T & {
    if (index >= field_vec_.size()) {
      throw std::overflow_error("DbObject field index overflow");
    }
    auto ptr = std::dynamic_pointer_cast<DbField<T>>(field_vec_[index]);
    if (ptr) {
      return ptr->use();
    } else {
      throw std::runtime_error("Field not found");
    }
  }
  /**
   * 保存.
   *
   * \param prx 数据服务代理
   * \return
   */
  template <typename T> auto save(std::shared_ptr<T> prx, const std::string& filter = "") -> void {
    if (read_only_) {
      return;
    }
    if (save_pending_) {
      // 上一个保存操作未完成
      return;
    }
    proxy_id_ = prx->getID();
    auto json_str = save(filter);
    if (json_str.empty()) {
      return;
    }
    save_pending_ = true;
    try {
      // CALL proxy
      prx->save(json_str);
    } catch (std::exception &) {
      // 吃掉异常
    }
    dirty_ = false;
    save_pending_ = false;
  }
  /**
   * 保存.
   *
   * \param prx 数据服务代理
   * \return
   */
  template <typename T> auto save_all(std::shared_ptr<T> prx) -> void {
    if (read_only_) {
      return;
    }
    if (save_pending_) {
      // 上一个保存操作未完成
      return;
    }
    proxy_id_ = prx->getID();
    auto json_str = save_all();
    if (json_str.empty()) {
      return;
    }
    save_pending_ = true;
    try {
      // CALL proxy
      prx->save(json_str);
    } catch (std::exception &) {
      // 吃掉异常
    }
    dirty_ = false;
    save_pending_ = false;
  }
  /**
   * 新建.
   *
   * \param prx 数据服务代理
   * \return
   */
  template <typename T> auto save_new(std::shared_ptr<T> prx) -> void {
    if (read_only_) {
      return;
    }
    if (save_pending_) {
      // 上一个保存操作未完成
      return;
    }
    proxy_id_ = prx->getID();
    auto json_str = save_all("new");
    if (json_str.empty()) {
      return;
    }
    save_pending_ = true;
    try {
      // CALL proxy
      prx->save(json_str);
    } catch (std::exception &) {
      // 吃掉异常
    }
    dirty_ = false;
    save_pending_ = false;
  }
  /**
   * 覆盖, 脏属性也会被覆盖.
   *
   * \param prx 数据服务代理
   * \return
   */
  template <typename T> auto overwrite(std::shared_ptr<T> prx) -> bool {
    /*
     * {
     *     "cmd" : "load_index",
     *     "type" : "table name",
     *     "index_name" : "索引名",
     *     "index_type" : "索引类型",
     *     "index_value" : "索引值字符串"
     * }
     */
    proxy_id_ = prx->getID();
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_index";
    json_cmd["type"] = type_name_;
    json_cmd["index_name"] = std::move(get_index_name());
    json_cmd["index_type"] = std::move(get_index_type_string());
    json_cmd["index_value"] = std::move(get_index_value_string());
    json_cmd["expiration"] = expiration_;
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto value_ptr = get_json_object(*json_string);
    if (value_ptr) {
      if (value_ptr->empty()) {
        return false;
      }
      overwrite(*value_ptr);
      return true;
    }
    return false;
  }

  /**
   * 返回删除命令.
   */
  inline auto remove() -> std::string {
    /*
     * 从表内删除
     *
     * {
     *    "cmd" : "remove",
     *    "type": "table name",
     *    "index_name" : "索引名",
     *    "index_type" : "索引类型",
     *    "index_value" : "索引值字符串"
     * }
     *
     */
    Json::Value json_cmd;
    json_cmd["cmd"] = "remove";
    json_cmd["type"] = type_name_;
    json_cmd["index_name"] = std::move(get_index_name());
    json_cmd["index_type"] = std::move(get_index_type_string());
    json_cmd["index_value"] = std::move(get_index_value_string());
    return json_cmd.toStyledString();
  }

  /**
   * 从数据库删除对象.
   */
  template <typename T>
  inline auto remove(std::shared_ptr<T> prx) -> std::size_t {
    /*
     * 从表内删除
     *
     * {
     *    "cmd" : "remove",
     *    "type": "table name",
     *    "index_name" : "索引名",
     *    "index_type" : "索引类型",
     *    "index_value" : "索引值字符串"
     * }
     *
     */
    proxy_id_ = prx->getID();
    Json::Value json_cmd;
    json_cmd["cmd"] = "remove";
    json_cmd["type"] = type_name_;
    json_cmd["index_name"] = std::move(get_index_name());
    json_cmd["index_type"] = std::move(get_index_type_string());
    json_cmd["index_value"] = std::move(get_index_value_string());
    // CALL proxy
    return prx->remove(json_cmd.toStyledString());
  }

  /**
   * 从数据管理器内删除.
   *
   * \return
   */
  inline auto dispose() -> void {
    if (db_obj_mgr_ptr_) {
      db_obj_mgr_ptr_->remove(id_);
    }
  }

  /**
   * 检测脏标记.
   *
   * \return
   */
  inline auto is_dirty() -> bool { return dirty_; }

  /**
   * 获取字段数量.
   *
   * \return
   */
  inline auto get_field_size() -> std::size_t { return field_vec_.size(); }
  /**
   * 获取对象ID.
   *
   * \return
   */
  inline auto get_id() -> std::uint64_t { return id_; }
  /**
   * 设置对象ID.
   *
   * \param id 对象ID
   * \return
   */
  inline auto set_id(std::uint64_t id) -> void { id_ = id; }
  /**
   * 设置对象管理器指针.
   *
   * \param obj_mgr 对象管理器指针
   * \return
   */
  inline auto set_mananger(DbManager *obj_mgr) -> void {
    db_obj_mgr_ptr_ = obj_mgr;
  }
  /**
   * 获取类型名.
   *
   * \return
   */
  inline auto get_type_name() -> const std::string & { return type_name_; }
  /**
   * 创建属性字段.
   *
   * \param type 字段类型
   * \return 字段指针
   */
  inline static auto create_field(std::string &&type) -> FieldPtr {
    static std::unordered_map<std::string, FieldPtr (*)()> FuncMap = {
        {"int", IntField::create},       {"uint", UIntField::create},
        {"int64", Int64Field::create},   {"uint64", UInt64Field::create},
        {"string", StringField::create}, {"float", FloatField::create},
        {"double", DoubleField::create},
    };
    auto it = FuncMap.find(type);
    if (it == FuncMap.end()) {
      return nullptr;
    }
    return it->second();
  }
  /**
   * 获取索引值字符串
   */
  inline auto get_index_value_string() -> std::string {
    if (index_pos_ < 0) {
      throw std::runtime_error("No index");
    }
    return field_vec_[index_pos_]->to_json();
  }
  /**
   * 获取索引值类型
   */
  inline auto get_index_type_string() -> std::string {
    if (index_pos_ < 0) {
      throw std::runtime_error("No index");
    }
    return field_vec_[index_pos_]->get_type();
  }
  /**
   * 获取索引名
   */
  inline auto get_index_name() -> std::string {
    if (index_pos_ < 0) {
      return "index";
    }
    return field_vec_[index_pos_]->get_name();
  }
  /**
   * 从配置创建对象.
   *
   * \param config
   * \return true or false
   */
  inline auto from_config(const Json::Value &config) -> bool {
    /*
     * 从配置创建表
     *
     * {
     *    "type": "table name",
     *    "values" : [
     *        {
     *            "name" : "field name",
     *            "type" : "field type",
     *            "default" : "default value",
     *            "is_obj_id" : true
     *        },
     *        {
     *            ...
     *        }
     *    ]
     * }
     *
     */
    if (!config.isMember("type")) {
      return false;
    }
    type_name_ = config["type"].asString();
    if (!config.isMember("values") || !config["values"].isArray()) {
      return false;
    }
    expiration_ = config.get("expiration", 0).asUInt();
    std::int32_t pos = 0;
    for (const auto &value : config["values"]) {
      if (!value.isMember("name") || !value.isMember("type")) {
        return false;
      }
      auto name = value["name"].asString();
      if (field_map_.find(name) != field_map_.end()) {
        return false;
      }
      auto field_ptr = create_field(value["type"].asString());
      if (!field_ptr) {
        throw std::runtime_error("Invalid field type:" +
                                 value["type"].asString());
      }
      if (value.isMember("default")) {
        field_ptr->from_json(value["default"].asString());
      }
      if (value.isMember("is_obj_id")) {
        index_pos_ = pos;
      }
      pos += 1;
      field_ptr->set_name(value["name"].asString());
      field_map_[name] = field_ptr;
      field_vec_.emplace_back(std::move(field_ptr));
    }
    return true;
  }
  /**
   * 获取保存数据的JSON串.
   *
   * \return 保存数据的JSON串
   */
  inline auto save(const std::string& filter = "") -> std::string {
    /*
     * 保存
     *
     * {
     *    "cmd" : "save",
     *    "type": "table name",
     *    "index_name" : "index name",
     *    "index_type" : "index type",
     *    "index_value" " "index value string",
     *    "values" : [
     *        {
     *            "name" : "field name",
     *            "value" : "field value",
     *            "type" : "field type string"
     *        },
     *        {
     *            ...
     *        }
     *    ]
     * }
     *
     */
    if (!dirty_) {
      return "";
    }
    if (is_new_object()) {
      return save_all("new");
    }
    Json::Value root;
    root["cmd"] = "save";
    root["type"] = type_name_;
    root["index_name"] = std::move(get_index_name());
    root["index_type"] = std::move(get_index_type_string());
    root["index_value"] = std::move(get_index_value_string());
    root["expiration"] = expiration_;
    root["values"] = Json::Value(Json::ValueType::arrayValue);
    if(!filter.empty())
        root["filter"] = filter;
    for (auto &ptr : field_vec_) {
      if (ptr->is_dirty()) {
        Json::Value value;
        value["name"] = ptr->get_name();
        value["value"] = std::move(ptr->to_json());
        value["type"] = ptr->get_type();
        root["values"].append(std::move(value));
      }
    }
    dirty_ = false;
    set_new_object(false);
    return root.toStyledString();
  }
  /**
   * 获取全量数据JSON串.
   *
   * \return 全量数据JSON串
   */
  inline auto save_all(const std::string &cmd = "save") -> std::string {
    /*
     * 保存
     *
     * {
     *    "type": "table name",
     *    "index_name" : "索引名",
     *    "index_type" : "索引类型",
     *    "index_value" : "索引值字符串",
     *    "values" : [
     *        {
     *            "name" : "field name",
     *            "value" : "field value"
     *        },
     *        {
     *            ...
     *        }
     *    ]
     * }
     *
     */
    Json::Value root;
    root["cmd"] = cmd;
    root["type"] = type_name_;
    root["index_name"] = std::move(get_index_name());
    root["index_type"] = std::move(get_index_type_string());
    root["index_value"] = std::move(get_index_value_string());
    root["expiration"] = expiration_;
    root["values"] = Json::Value(Json::ValueType::arrayValue);
    for (auto &ptr : field_vec_) {
      Json::Value value;
      value["name"] = ptr->get_name();
      value["value"] = std::move(ptr->to_json());
      value["type"] = ptr->get_type();
      root["values"].append(std::move(value));
    }
    dirty_ = false;
    set_new_object(false);
    return root.toStyledString();
  }
  /**
   * 根据名字或索引获取数据字段指针.
   *
   * \param value JSON对象
   * \param index 索引
   * \return 数据字段指针
   */
  inline auto get_field_ptr(const Json::Value &value, std::size_t index)
      -> FieldPtr {
    if (value.isMember("name")) {
      auto it = field_map_.find(value["name"].asString());
      if (it == field_map_.end()) {
        return nullptr;
      }
      return it->second;
    } else {
      if (index >= field_vec_.size()) {
        return nullptr;
      }
      return field_vec_[index];
    }
  }
  /**
   * 加载数据, 如果有未保存的脏数据则抛出异常.
   *
   * \param root JSON对象
   * \return
   */
  inline auto load(const Json::Value &root) -> void {
    /*
     * {
     *     "values" : {
     *         {
     *             "name" : "field name",
     *             "value" : "field value string"
     *         },
     *         {
     *             ...
     *         }
     *     }
     * }
     */
    std::size_t index = 0;
    for (const auto &kv : root["values"]) {
      auto field_ptr = get_field_ptr(kv, index++);
      if (!field_ptr) {
        throw std::runtime_error("Field not found");
      }
      auto value = kv["value"].asString();
      if (field_ptr->is_dirty()) {
        throw std::runtime_error("Field is dirty but need overwrite");
      }
      field_ptr->from_json(std::move(value));
    }
  }
  /**
   * 加载数据.
   *
   * \param root JSON对象
   * \return
   */
  inline auto overwrite(const Json::Value &root) -> void {
    /*
     * {
     *     "values" : [
     *         {
     *             "name" : "field name",
     *             "value" : "field value string"
     *         },
     *         {
     *             ...
     *         }
     *     ]
     * }
     */
    std::size_t index = 0;
    for (const auto &kv : root["values"]) {
      auto field_ptr = get_field_ptr(kv, index++);
      if (!field_ptr) {
        throw std::runtime_error("Field not found");
      }
      auto value = kv["value"].asString();
      field_ptr->from_json(std::move(value));
    }
    dirty_ = false;
  }
  /**
   * 获取索引字符串.
   *
   * \return
   */
  inline auto get_index_string() -> std::string {
    return get_index_value_string();
  }
  /**
   * 获取字段字典信息.
   *
   * \param root
   * \return
   */
  inline auto get_field_schema(Json::Value &root) -> bool {
    for (const auto &[name, ptr] : field_map_) {
      Json::Value kv;
      kv["name"] = name;
      kv["type"] = ptr->get_type();
      // set default value
      kv["default"] = ptr->to_json();
      root.append(std::move(kv));
    }
    return true;
  }
};

/**
 * 数据查询.
 */
class DbQuery {
public:
  /**
   * 创建数据表, 数据包装类型为ObjectT.
   *
   * \param prx 数据服务
   * \return true or false
   */
  template <typename ObjectT, typename T>
  static inline auto create(std::shared_ptr<T> prx) -> bool {
    auto wrap_obj_ptr = std::make_shared<ObjectT>();
    auto obj_ptr = wrap_obj_ptr->get_obj_ptr();
    Json::Value json_cmd;
    json_cmd["cmd"] = "create";
    json_cmd["type"] = ObjectT::get_type_name();
    json_cmd["expiration"] = obj_ptr->get_expiration();
    Json::Value field_schema(Json::ValueType::arrayValue);
    if (!obj_ptr->get_field_schema(field_schema)) {
      return false;
    }
    json_cmd["values"] = std::move(field_schema);
    if (!prx->create(json_cmd.toStyledString())) {
      return false;
    }
    return true;
  }
  /**
   * 创建数据表.
   *
   * \param prx 数据服务
   * \param config 数据配置文件
   * \return true or false
   */
  template <typename T>
  static inline auto create(std::shared_ptr<T> prx, const std::string &config)
      -> bool {
    /*
     * 从配置创建表
     *
     * {
     *    "cmd" : "create",
     *    "type": "table name",
     *    "values" : [
     *        {
     *            "name" : "field name",
     *            "type" : "field type",
     *            "default" : "default value string"
     *        },
     *        {
     *            ...
     *        }
     *    ]
     * }
     *
     */
    auto value_ptr = get_json_string(config);
    auto obj_ptr = std::make_shared<DbObject>();
    if (!obj_ptr->from_config(*value_ptr)) {
      return false;
    }
    Json::Value json_cmd;
    json_cmd["cmd"] = "create";
    json_cmd["type"] = obj_ptr->get_type_name();
    json_cmd["expiration"] = obj_ptr->get_expiration();
    Json::Value field_schema(Json::ValueType::arrayValue);
    if (!obj_ptr->get_field_schema(field_schema)) {
      return false;
    }
    json_cmd["values"] = std::move(field_schema);
    if (!prx->create(json_cmd.toStyledString())) {
      return false;
    }
    return true;
  }
  /**
   * 创建数据对象.
   *
   * \param config 数据配置文件
   * \return 数据对象
   */
  static inline auto create(const std::string &config, bool read_only = false)
      -> DbObjectPtr {
    auto value_ptr = get_json_string(config);
    auto obj_ptr = std::make_shared<DbObject>();
    if (!obj_ptr->from_config(*value_ptr)) {
      return nullptr;
    }
    obj_ptr->set_read_only(read_only);
    get_db_manager().add(obj_ptr);
    return obj_ptr;
  }
  /**
   * 创建数据对象.
   *
   * \return 数据对象
   */
  template <typename T>
  static inline auto create(bool read_only = false) -> std::shared_ptr<T> {
    auto obj_ptr = T::create();
    obj_ptr->mut().set_read_only(read_only);
    return obj_ptr;
  }
  /**
   * 创建数据对象.
   *
   * \param config 数据配置名
   * \param json_string 数据配置字符串
   * \return 数据对象
   */
  static inline auto create_from_string(const std::string &config,
                                        const std::string &json_string,
                                        const std::string &index = "",
                                        bool read_only = false) -> DbObjectPtr {
    auto value_ptr = get_json_object(json_string);
    if (!value_ptr) {
      return nullptr;
    }
    get_db_manager().get_config_map()[config] = value_ptr;
    auto obj_ptr = std::make_shared<DbObject>();
    if (!obj_ptr->from_config(*value_ptr)) {
      return nullptr;
    }
    obj_ptr->set_read_only(read_only);
    get_db_manager().add(obj_ptr);
    return obj_ptr;
  }
  /**
   * 加载指定UUID的数据对象, 数据包装类型ObjectT.
   *
   * \param prx 数据服务
   * \param index UUID
   * \return 数据对象
   */
  template <typename ObjectT, typename T>
  static inline auto load(std::shared_ptr<T> prx, const std::string &index,
                          bool read_only = false) -> std::shared_ptr<ObjectT> {
    auto wrap_obj_ptr = std::make_shared<ObjectT>();
    auto obj_ptr = wrap_obj_ptr->get_obj_ptr();
    obj_ptr->set_read_only(read_only);
    get_db_manager().add(obj_ptr);
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_index";
    json_cmd["type"] = ObjectT::get_type_name();
    json_cmd["index_name"] = std::move(obj_ptr->get_index_name());
    json_cmd["index_type"] = std::move(obj_ptr->get_index_type_string());
    json_cmd["index_value"] = std::move(obj_ptr->get_index_value_string());
    json_cmd["expiration"] = std::move(obj_ptr->get_expiration());
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto json_obj_ptr = get_json_object(*json_string);
    if (!json_obj_ptr || json_obj_ptr->empty()) {
      return nullptr;
    }
    obj_ptr->load(*json_obj_ptr);
    return wrap_obj_ptr;
  }
  static inline auto arg_to_string(const std::int32_t &i) -> std::string {
    return std::to_string(i);
  }
  static inline auto arg_to_string(const std::int64_t &i) -> std::string {
    return std::to_string(i);
  }
  static inline auto arg_to_string(const std::uint32_t &i) -> std::string {
    return std::to_string(i);
  }
  static inline auto arg_to_string(const std::uint64_t &i) -> std::string {
    return std::to_string(i);
  }
  static inline auto arg_to_string(const bool &i) -> std::string {
    return i ? "true" : "false";
  }
  static inline auto arg_to_string(const float &i) -> std::string {
    return std::to_string(i);
  }
  static inline auto arg_to_string(const double &i) -> std::string {
    return std::to_string(i);
  }
  static inline auto arg_to_string(const std::string &i) -> std::string {
    return i;
  }
  /**
   * 加载指定UUID的数据对象, 数据包装类型ObjectT.
   *
   * \param prx 数据服务
   * \param index UUID
   * \return 数据对象
   */
  template <typename ObjectT, typename T, typename ArgT>
  static inline auto load(std::shared_ptr<T> prx,
                          std::initializer_list<ArgT> args,
                          bool read_only = false)
      -> std::vector<std::shared_ptr<ObjectT>> {
    if (args.size() == 0) {
      return {};
    }
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_multi_index";
    json_cmd["type"] = ObjectT::get_type_name();
    json_cmd["index_name"] = std::move(ObjectT::index_name());
    json_cmd["index_type"] = std::move(ObjectT::index_type());
    json_cmd["expiration"] = std::move(ObjectT::expiration());
    Json::Value value_array(Json::ValueType::arrayValue);
    for (const auto &arg : args) {
      value_array.append(arg_to_string(arg));
    }
    json_cmd["multi_index_value"] = std::move(value_array);
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto json_obj_ptr = get_json_object(*json_string);
    if (!json_obj_ptr || json_obj_ptr->empty()) {
      return {};
    }
    if (!json_obj_ptr->isMember("multi_values")) {
      return {};
    }
    std::vector<std::shared_ptr<ObjectT>> ptr_vec;
    for (const auto &value : (*json_obj_ptr)["multi_values"]) {
      auto wrap_obj_ptr = std::make_shared<ObjectT>();
      auto obj_ptr = wrap_obj_ptr->get_obj_ptr();
      obj_ptr->set_read_only(read_only);
      get_db_manager().add(obj_ptr);
      obj_ptr->load(value);
      ptr_vec.push_back(wrap_obj_ptr);
    }
    return ptr_vec;
  }

  /**
   * 批量加载指定UUID的数据对象, 数据包装类型ObjectT.
   *
   * \param prx 数据服务
   * \param args UUIDs
   * \return 数据对象
   */
  template <typename ObjectT, typename T, typename ArgT>
  static inline auto load(std::shared_ptr<T> prx,
    std::vector<ArgT> args,
    bool read_only = false)
    -> std::vector<std::shared_ptr<ObjectT>> {
    if (args.size() == 0) {
        return {};
    }
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_multi_index";
    json_cmd["type"] = ObjectT::get_type_name();
    json_cmd["index_name"] = std::move(ObjectT::index_name());
    json_cmd["index_type"] = std::move(ObjectT::index_type());
    json_cmd["expiration"] = std::move(ObjectT::expiration());
    Json::Value value_array(Json::ValueType::arrayValue);
    for (const auto& arg : args) {
        value_array.append(arg_to_string(arg));
    }
    json_cmd["multi_index_value"] = std::move(value_array);
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto json_obj_ptr = get_json_object(*json_string);
    if (!json_obj_ptr || json_obj_ptr->empty()) {
        return {};
    }
    if (!json_obj_ptr->isMember("multi_values")) {
        return {};
    }
    std::vector<std::shared_ptr<ObjectT>> ptr_vec;
    for (const auto& value : (*json_obj_ptr)["multi_values"]) {
        auto wrap_obj_ptr = std::make_shared<ObjectT>();
        auto obj_ptr = wrap_obj_ptr->get_obj_ptr();
        obj_ptr->set_read_only(read_only);
        get_db_manager().add(obj_ptr);
        obj_ptr->load(value);
        ptr_vec.push_back(wrap_obj_ptr);
    }
    return ptr_vec;
  }

  /**
   * 加载指定UUID的数据对象.
   *
   * \param prx 数据服务
   * \param config 数据配置文件
   * \param index UUID
   * \return 数据对象
   */
  template <typename T>
  static inline auto load(std::shared_ptr<T> prx, const std::string &config,
                          const std::string &index, bool read_only = false)
      -> DbObjectPtr {
    /*
     * 按照索引加载
     *
     * {
     *    "cmd" : "load_index",
     *    "type": "table name",
     *    "index_name" : "索引名",
     *    "index_type" : "索引类型",
     *    "index_value" : "索引值字符串"
     * }
     *
     */
    auto value_ptr = get_json_string(config);
    auto obj_ptr = std::make_shared<DbObject>();
    if (!obj_ptr->from_config(*value_ptr)) {
      return nullptr;
    }
    obj_ptr->set_read_only(read_only);
    get_db_manager().add(obj_ptr);
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_index";
    json_cmd["type"] = (*value_ptr)["type"].asString();
    json_cmd["index_name"] = std::move(obj_ptr->get_index_name());
    json_cmd["index_type"] = std::move(obj_ptr->get_index_type_string());
    json_cmd["index_value"] = std::move(obj_ptr->get_index_value_string());
    json_cmd["expiration"] = std::move(obj_ptr->get_expiration());
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto json_obj_ptr = get_json_object(*json_string);
    if (!json_obj_ptr || json_obj_ptr->empty()) {
      return nullptr;
    }
    obj_ptr->load(*json_obj_ptr);
    return obj_ptr;
  }
  /**
   * 使用过滤器加载数据对象, 数据包装类型为ObjectT.
   *
   * \param prx 数据服务
   * \param filter 过滤器
   * \return 数据对象数组
   */
  template <typename ObjectT, typename T>
  static inline auto filter(std::shared_ptr<T> prx,
                            const std::string &filter = "",
                            bool read_only = false)
      -> std::vector<std::shared_ptr<ObjectT>> {
    /*
     * 加载表
     *
     * {
     *    "cmd" : "load_all",
     *    "type": "table name",
     *    "filter" : "过滤条件语句"
     * }
     *
     */
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_all";
    json_cmd["type"] = ObjectT::get_type_name();
    if (!filter.empty()) {
      json_cmd["filter"] = filter;
    }
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto ret_json_value_ptr = get_json_object(*json_string);
    if (!ret_json_value_ptr || ret_json_value_ptr->empty()) {
      return {};
    }
    std::vector<std::shared_ptr<ObjectT>> obj_ptr_vec;
    for (const auto &obj : *ret_json_value_ptr) {
      auto proto_obj_ptr = std::make_shared<ObjectT>();
      auto obj_ptr = proto_obj_ptr->get_obj_ptr();
      obj_ptr->set_read_only(read_only);
      get_db_manager().add(obj_ptr);
      obj_ptr->load(obj);
      obj_ptr_vec.emplace_back(std::move(proto_obj_ptr));
    }
    return obj_ptr_vec;
  }
  /**
   * 使用过滤器加载数据对象, 数据包装类型为ObjectT.
   *
   * \param prx 数据服务
   * \param filter 过滤器
   * \return 数据对象数组
   */
  template <typename ObjectT, typename T>
  static inline auto filter_one(std::shared_ptr<T> prx,
                                const std::string &filter = "",
                                bool read_only = false)
      -> std::shared_ptr<ObjectT> {
    /*
     * 加载表
     *
     * {
     *    "cmd" : "load_all",
     *    "type": "table name",
     *    "filter" : "过滤条件语句"
     * }
     *
     */
    Json::Value json_cmd;
    json_cmd["cmd"] = "load_all";
    json_cmd["type"] = ObjectT::get_type_name();
    if (!filter.empty()) {
      json_cmd["filter"] = filter;
    }
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto ret_json_value_ptr = get_json_object(*json_string);
    if (!ret_json_value_ptr || ret_json_value_ptr->empty()) {
      return {};
    }
    std::shared_ptr<ObjectT> ret_obj_ptr;
    for (const auto &obj : *ret_json_value_ptr) {
      auto proto_obj_ptr = std::make_shared<ObjectT>();
      auto obj_ptr = proto_obj_ptr->get_obj_ptr();
      obj_ptr->set_read_only(read_only);
      get_db_manager().add(obj_ptr);
      obj_ptr->load(obj);
      ret_obj_ptr = std::move(proto_obj_ptr);
      // 只获取一个对象
      break;
    }
    return ret_obj_ptr;
  }
  /**
   * 使用过滤器加载数据对象.
   *
   * \param prx 数据服务
   * \param config 数据配置文件
   * \param filter 过滤器
   * \return 数据对象数组
   */
  template <typename T>
  static inline auto filter(std::shared_ptr<T> prx, const std::string &config,
                            const std::string &filter = "",
                            bool read_only = false) -> ObjPtrVector {
    /*
     * 加载表
     *
     * {
     *    "cmd" : "load_all",
     *    "type": "table name",
     *    "filter" : "过滤条件语句"
     * }
     *
     */
    Json::Value json_cmd;
    auto type_json_ptr = get_json_string(config);
    if (!type_json_ptr) {
      return {};
    }
    json_cmd["cmd"] = "load_all";
    json_cmd["type"] = (*type_json_ptr)["type"].asString();
    if (!filter.empty()) {
      json_cmd["filter"] = filter;
    }
    // CALL proxy
    auto json_string = prx->load(json_cmd.toStyledString());
    auto ret_json_value_ptr = get_json_object(*json_string);
    if (!ret_json_value_ptr || ret_json_value_ptr->empty()) {
      return {};
    }
    ObjPtrVector obj_ptr_vec;
    for (const auto &obj : *ret_json_value_ptr) {
      auto json_value_ptr = get_json_string(config);
      if (!json_value_ptr) {
        return {};
      }
      auto obj_ptr = std::make_shared<DbObject>();
      if (!obj_ptr->from_config(*json_value_ptr)) {
        return {};
      }
      obj_ptr->set_read_only(read_only);
      get_db_manager().add(obj_ptr);
      obj_ptr->load(obj);
      obj_ptr_vec.emplace_back(std::move(obj_ptr));
    }
    return obj_ptr_vec;
  }
  /**
   * 使用过滤器删除数据对象, 数据包装类型ObjectT.
   *
   * \param prx 数据服务
   * \param filter 过滤器
   * \return 删除的数量
   */
  template <typename ObjectT, typename T>
  static inline auto remove(std::shared_ptr<T> prx,
                            const std::string &filter = "") -> std::size_t {
    /*
     * 从表内删除
     *
     * {
     *    "cmd" : "remove",
     *    "type": "table name",
     *    "filter" : "过滤条件语句"
     * }
     *
     */
    Json::Value json_cmd;
    json_cmd["cmd"] = "remove";
    json_cmd["type"] = ObjectT::get_type_name();
    if (!filter.empty()) {
      json_cmd["filter"] = "";
    }
    // CALL proxy
    return prx->remove(json_cmd.toStyledString());
  }
  /**
   * 使用过滤器删除数据对象.
   *
   * \param prx 数据服务
   * \param config 数据配置文件
   * \param filter 过滤器
   * \return 删除的数量
   */
  template <typename T>
  static inline auto remove(std::shared_ptr<T> prx, const std::string &config,
                            const std::string &filter = "") -> std::size_t {
    /*
     * 从表内删除
     *
     * {
     *    "cmd" : "remove",
     *    "type": "table name",
     *    "filter" : "过滤条件语句"
     * }
     *
     */
    Json::Value json_cmd;
    auto type_json_ptr = get_json_string(config);
    if (!type_json_ptr) {
      return 0;
    }
    json_cmd["cmd"] = "remove";
    json_cmd["type"] = (*type_json_ptr)["type"].asString();
    if (!filter.empty()) {
      json_cmd["filter"] = filter;
    }
    // CALL proxy
    return prx->remove(json_cmd.toStyledString());
  }
};

inline auto DbManager::add(DbObjectPtr obj_ptr) -> void {
  auto id = id_++;
  obj_ptr->set_id(id);
  obj_ptr->set_mananger(this);
  db_obj_map_[id] = obj_ptr;
}

template <typename T>
inline auto DbManager::update(std::shared_ptr<T> prx, bool binding) -> void {
  //
  // TODO 批量新建或保存
  //

  //
  // NOTE
  //
  // binding为true时, 数据对象会使用已经绑定的代理进行存储
  //
  auto &db_obj_map = get_db_manager().get_db_obj_map();
  for (auto &[id, ptr] : db_obj_map) {
    auto need_save = false;
    auto obj_ptr = ptr;
    if (obj_ptr->is_dirty() && !obj_ptr->is_read_only()) {
      if (binding) {
        // 未绑定或绑定的代理ID相同
        if (obj_ptr->get_proxy_id() == rpc::INVALID_PROXY_ID ||
            obj_ptr->get_proxy_id() == prx->getID()) {
          need_save = true;
        }
      } else {
        // 忽略绑定
        need_save = true;
      }
      if (!need_save) {
          continue;
      }
      if (obj_ptr->is_new_object()) {
        // 新建
        co_mgr_ptr_->spawn([obj_ptr, prx]() { obj_ptr->save_new(prx); });
      } else {
        // 保存
        co_mgr_ptr_->spawn([obj_ptr, prx]() { obj_ptr->save(prx); });
      }
    }
  }
  // 检查并删除
  for (auto it = db_obj_map.begin(); it != db_obj_map.end();) {
    if (it->second.use_count() == 1) {
      auto obj_ptr = it->second;
      // 无引用且脏
      if (obj_ptr->is_dirty() && !obj_ptr->is_read_only()) {
        if (obj_ptr->is_new_object()) {
          // 新建
          co_mgr_ptr_->spawn([obj_ptr, prx]() { obj_ptr->save_new(prx); });
        } else {
          // 保存
          co_mgr_ptr_->spawn([obj_ptr, prx]() { obj_ptr->save(prx); });
        }
      }
      it = db_obj_map.erase(it);
    } else {
      ++it;
    }
  }
}

} // namespace util
} // namespace kratos
