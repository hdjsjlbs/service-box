cmake_minimum_required(VERSION 3.10)
set(CMAKE_CXX_STANDARD 17)
project (python_log)
SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin/)

find_package(Python3 COMPONENTS Development REQUIRED)

IF (MSVC)
    foreach(var
        CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO
        CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO
        )
        if(${var} MATCHES "/MD")
            string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")
        endif()
    endforeach()
ELSE()
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        set(CMAKE_CXX_COMPILER "clang++")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        set(CMAKE_CXX_COMPILER "g++")
    endif()
    find_program(CCACHE_PROGRAM ccache)
    if (CCACHE_PROGRAM)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
    endif ()
ENDIF ()

aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/jsoncpp SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/klogger/internal SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/knet SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../util/python_log SRC_LIST)

add_library(log SHARED ${SRC_LIST})

target_compile_features(log PUBLIC cxx_std_17)

IF (MSVC)
    set_target_properties(log PROPERTIES LINK_FLAGS "/ignore:4099")

    target_link_libraries(log PUBLIC
        ${Python3_LIBRARIES}
		ws2_32.lib
	)
    
else()
    link_directories(${PROJECT_SOURCE_DIR}/../thirdparty/lib/linux)
	target_link_libraries(log PUBLIC
        ${Python3_LIBRARIES}
		-lpthread
    )
endif()

#cross platfom file 
target_sources(log PRIVATE
	${PROJECT_SOURCE_DIR}/../util/string_util.cpp
    ${PROJECT_SOURCE_DIR}/../util/os_util.cpp
)


#设置包含路径以及安装时候的路径
target_include_directories(log
	PUBLIC
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/..
	${PROJECT_SOURCE_DIR}/../../thirdparty 
	${PROJECT_SOURCE_DIR}/../../thirdparty/include 
    ${PROJECT_SOURCE_DIR}/../../thirdparty/include/knet
    ${Python3_INCLUDE_DIRS}
)

target_compile_definitions(log PUBLIC
    DISABLE_TRACE
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>: _WINSOCK_DEPRECATED_NO_WARNINGS WIN32 _WINDOWS _DEBUG _CONSOLE _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN>
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Release>>: _WINSOCK_DEPRECATED_NO_WARNINGS _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN WIN32 _WINDOWS NDEBUG>
)

target_compile_options(log PUBLIC
    $<$<AND:$<PLATFORM_ID:Linux>,$<CONFIG:Debug>>: -m64 -g -O0 -Wall -Wno-parentheses -Wno-deprecated-declarations -fnon-call-exceptions -fPIC>
    $<$<AND:$<PLATFORM_ID:Linux>,$<CONFIG:Release>>: -m64 -g -O2 -Wall -Wno-parentheses -Wno-deprecated-declarations -fnon-call-exceptions -fPIC>
	$<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>: /wd4706 /wd4206 /wd4100 /wd4244 /wd4127 /wd4702 /wd4324 /wd4310 /wd4820 /wd5033 /W4 /MP /EHa /Zi>
)

target_link_options(log PUBLIC 
	$<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>: /NODEFAULTLIB:library /INCREMENTAL:NO>
)

IF (MSVC)
SET_TARGET_PROPERTIES(log PROPERTIES 
                    PREFIX ""
                    DEBUG_POSTFIX "_d"
                    SUFFIX ".pyd")
ELSE()
SET_TARGET_PROPERTIES(log PROPERTIES DEBUG_POSTFIX _d)
ENDIF()
                    
target_compile_definitions(log PUBLIC
$<$<CONFIG:Debug>: -DDEBUG -D_DEBUG>
$<$<CONFIG:Release>: -DRELEASE>
)

install(TARGETS log
	LIBRARY DESTINATION bin
)
