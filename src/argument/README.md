# 启动参数

启动参数主要分为：
1. 通用参数

   Windows, Linux通用参数
2. 专属参数
   
   Windows, Linux专用参数

# 通用参数
1. -c, --config

   启动的配置文件，如果不设置将在service box二进制路径内使用box.cfg启动，如果不存在则启动失败
2. -f, --frame

   service box主线程逻辑主循环的帧率
3. --alloc-dump

   是否在关闭service box时打印内存分配统计
4. --config-center

   配置文件远程下载地址（或WebService API）, 下载后的配置文件名为box.cfg并使用下载的配置文件启动，如果通过-c(--config)设置了配置文件则远程配置会被下载，但是如果名字与参数设置的配置文件名不同则会被忽略，如果名字相同(必须为box.cfg)则使用远程下载的配置文件
5. --system-exception

   将系统错误例如Segmentation fault转换为C++ std::exception并抛出，此功能只用于日常开发调试，不要在线上开启，即使能捕获错误也不能保证后续的运行结果是正常的。
6. --proxy

   参考[以代理模式启动](https://gitee.com/dennis-kk/service-box/blob/master/README-proxy_mode.md)

7. --proxy-host

   连接到指定（其他）的service box，被指定的service box必须以代理模式启动(启动参数--proxy)。

8. -h, --help   

# Windows专属
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/190709_80391eea_467198.png "屏幕截图.png")

专属的参数用于Windows系统服务。
1. --install

   将服务安装为Windows系统服务
2. --uninstall

   将服务从Windows系统服务列表卸载
3. --start_service

   以Windows系统服务方式启动，必须先安装
4. --stop_service

   以Windows系统服务方式关闭，必须先安装

# Linux专属
![输入图片说明](https://images.gitee.com/uploads/images/2020/1204/190859_a64fa563_467198.png "屏幕截图.png")
1. --daemon

   以守护进程方式启动