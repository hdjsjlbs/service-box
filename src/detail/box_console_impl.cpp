#include "console/box_console_impl.hh"

namespace kratos {
namespace console {

BoxConsoleImpl::BoxConsoleImpl(const std::string& name, Console* console) {
    name_    = name + "_";
    console_ = console;
}

BoxConsoleImpl::~BoxConsoleImpl() {
  for (const auto& name : switch_set_) {
    console_->remove_switch(name);
  }
  for (const auto& name : selection_set_) {
    console_->remove_selection(name);
  }
}

auto BoxConsoleImpl::add_switch(const std::string& name,
    ConsoleSwitchPlugin switch_plugin)->void {
  switch_set_.emplace(name_ + name);
  console_->add_switch(name_ + name, switch_plugin);
}

auto BoxConsoleImpl::remove_switch(const std::string& name)->void {
  switch_set_.erase(name_ + name);
  console_->remove_switch(name_ + name);
}

auto BoxConsoleImpl::add_selection(const std::string& name,
    ConsoleSelectionPlugin selection_plugin)->void {
  selection_set_.emplace(name_ + name);
  console_->add_selection(name_ + name, selection_plugin);
}

auto BoxConsoleImpl::remove_selection(const std::string& name)->void {
  selection_set_.erase(name_ + name);
  console_->remove_selection(name_ + name);
}

}
}