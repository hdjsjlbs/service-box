﻿#pragma once

#include "box/sys_logger.hh"
#include <string>

namespace kratos {
namespace service {

/**
 * 操作系统日志实现类.
 */
class SysLoggerImpl : public SysLogger {
public:
  /**
   * 构造.
   *
   * \param name 服务容器名称
   */
  SysLoggerImpl(const std::string &name);
  /**
   * 析构.
   * 
   */
  virtual ~SysLoggerImpl();
  virtual auto log(int level, const char *fmt, ...) -> void override;
};

} // namespace service
} // namespace kratos
