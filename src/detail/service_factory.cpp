#include "box/service_factory.hh"
#include "service_finder/service_finder.hh"
#include "service_register/service_register.hh"
#include "util/object_pool.hh"
#include "zookeeper/service_finder_zookeeper.hh"
#include "zookeeper/service_register_zookeeper.hh"
#include <unordered_map>

namespace kratos {
namespace service {

class ServiceMechManagerImpl : public ServiceMechManager {
  using FactoryMap =
      kratos::service::PoolUnorederedMap<std::string, ServiceMechFactoryPtr>;
  FactoryMap factory_map_; ///< 工厂表

public:
  ServiceMechManagerImpl() {}
  virtual ~ServiceMechManagerImpl() {}
  virtual auto add_factory(ServiceMechFactoryPtr factory_ptr) -> bool override {
    auto it = factory_map_.find(factory_ptr->get_type_name());
    if (it != factory_map_.end()) {
      return false;
    }
    factory_map_.emplace(factory_ptr->get_type_name(), factory_ptr);
    return true;
  }

  virtual auto get(const std::string &type_name)
      -> ServiceMechFactoryPtr override {
    auto it = factory_map_.find(type_name);
    if (it == factory_map_.end()) {
      return nullptr;
    }
    return it->second;
  }
};

/**
 * 获取全局唯一管理器.
 *
 * \return 全局唯一管理器
 */
auto get_manager() -> ServiceMechManagerPtr {
  static auto service_mech_manager_ptr =
      make_shared_pool_ptr<ServiceMechManagerImpl>();
  return service_mech_manager_ptr;
}

std::shared_ptr<ServiceRegister> getRegister(const std::string &type) {
  if ("zookeeper" == type) {
    return make_shared_pool_ptr<ServiceRegisterZookeeper>();
  } else {
    // 其他实现
    return get_manager()->get(type)->create_register();
  }
  return nullptr;
}

std::shared_ptr<ServiceFinder> getFinder(const std::string &type) {
  if ("zookeeper" == type) {
    return make_shared_pool_ptr<ServiceFinderZookeeper>();
  } else {
    // 其他实现
    return get_manager()->get(type)->create_finder();
  }
  return nullptr;
}

} // namespace service
} // namespace kratos
