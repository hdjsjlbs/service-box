#include "rpc_probe_impl.hh"
#include "box/service_box.hh"
#include "config/box_config.hh"
#include "root/rpc_transport.h"
#include "util/os_util.hh"
#include "util/string_util.hh"
#include "util/time_system.hh"
#include "util/time_util.hh"

#include <limits>

namespace kratos {
namespace service {

RpcProbeImpl::RpcProbeImpl(ServiceBox *box) : RpcProbeImpl(&box->get_config()) {
  box_ = box;
}

RpcProbeImpl::RpcProbeImpl(config::BoxConfig *config) {
  config_ = config;
  if (!config_ || !config_->is_open_trace()) {
    return;
  }
  pid_ = util::get_pid();
  pid_str_ = std::to_string(pid_);
  port_ = get_listener_port();
  call_tracer_ptr_ = get_call_tracer_factory()->create_from(*config_);
  std::vector<std::string> mac_addr_list;
  util::get_mac_addr_list(mac_addr_list);
  if (!mac_addr_list.empty()) {
    mac_addr_ = mac_addr_list[0];
    mac_hash_str_ = std::to_string(util::get_mac_hash(mac_addr_));
  }
  std::vector<std::string> ip_addr_list;
  util::get_ip_addr_list(ip_addr_list);
  if (!ip_addr_list.empty()) {
    ip_hash_ = util::get_ip_hash(ip_addr_list[0]);
  }
}

RpcProbeImpl::~RpcProbeImpl() {}

auto RpcProbeImpl::on_msg(rpc::RpcMsgType msg_type,
                          rpc::TransportPtr &transport) -> void {
  if (!call_tracer_ptr_) {
    return;
  }
}

auto RpcProbeImpl::on_tick(std::time_t now) -> void {
  if (!call_tracer_ptr_) {
    return;
  }
  call_tracer_ptr_->update(now);
}

auto RpcProbeImpl::on_error(std::uint32_t error_id,
                            const std::string &error_msg) -> void {
  if (!call_tracer_ptr_) {
    return;
  }
}

auto RpcProbeImpl::on_close() -> void {
  if (!call_tracer_ptr_) {
    return;
  }
}

auto RpcProbeImpl::on_load(const std::string &uuid,
                           const std::string &bundle_path, bool result)
    -> void {
  if (!call_tracer_ptr_) {
    return;
  }
}

auto RpcProbeImpl::on_unload(const std::string &uuid, bool result) -> void {
  if (!call_tracer_ptr_) {
    return;
  }
}

auto RpcProbeImpl::on_gen_uuid(std::string &uuid_str) -> void {
  //
  // MAC地址+PID+64位自增ID
  //
  uuid_str += mac_hash_str_ + pid_str_ + std::to_string(trace_id_++);
}

auto RpcProbeImpl::on_gen_uuid(std::uint64_t &uuid) -> void {
  //
  // 16位IP地址 | 16位端口 | 16位自增ID
  // NOTICE 确保转变为字符串为<=16个字符
  //
  // 只要保证同一个trace_id下UUID不重复即可
  // 同一个trace下最多可以同时存在65535个span
  //
  uuid = ip_hash_ | (std::uint64_t(port_) << 16) | (std::uint64_t(id_++) << 32);
}

auto RpcProbeImpl::on_get_micro_now(std::time_t &micro_now) -> void {
  micro_now = util::get_os_time_microsecond();
}

auto RpcProbeImpl::on_proxy_call_sent(const std::string &trace_id,
                                      std::uint64_t span_id,
                                      std::uint64_t parent_span_id,
                                      rpc::TransportPtr transport,
                                      const std::string &target_service_name,
                                      const std::string &target_method_name,
                                      const std::string &args_info, bool oneway)
    -> void {
  if (!call_tracer_ptr_) {
    return;
  }
  TraceInfo ti;
  if (transport) {
    ti.from_ip = transport->getLocalAddress().ip;
    ti.from_port = transport->getLocalAddress().port;
    ti.target_ip = transport->getPeerAddress().ip;
    ti.target_port = transport->getPeerAddress().port;
  }
  ti.method_name = target_method_name;
  ti.pid = pid_;
  ti.parent_span_id = parent_span_id;
  ti.service_name = target_service_name;
  ti.span_id = span_id;
  ti.timestamp_ms = util::get_os_time_microsecond();
  ti.trace_id = trace_id;
  ti.param_info = args_info;
  ti.type = TraceType::CLIENT;
  ti.detail_type = TraceDetailType::PROXY_CALL_REMOTE_SERVICE;
  if (!oneway) {
    proxy_call_trace_map_[span_id] = std::move(ti);
  } else {
    call_tracer_ptr_->trace(ti);
  }
}

auto RpcProbeImpl::on_stub_call_arrived(const std::string &trace_id,
                                        std::uint64_t span_id,
                                        std::uint64_t parent_span_id,
                                        rpc::TransportPtr transport,
                                        const std::string &target_service_name,
                                        const std::string &target_method_name,
                                        const std::string &args_info) -> void {
  if (!call_tracer_ptr_) {
    return;
  }
  TraceInfo ti;
  if (transport) {
    ti.from_ip = transport->getPeerAddress().ip;
    ti.from_port = transport->getPeerAddress().port;
    ti.target_ip = transport->getLocalAddress().ip;
    ti.target_port = transport->getLocalAddress().port;
  }
  ti.method_name = target_method_name;
  ti.pid = pid_;
  ti.parent_span_id = parent_span_id;
  ti.service_name = target_service_name;
  ti.span_id = span_id;
  ti.timestamp_ms = util::get_os_time_microsecond();
  ti.trace_id = trace_id;
  ti.param_info = args_info;
  ti.type = TraceType::SERVER;
  ti.detail_type = TraceDetailType::SERVICE_RECV_PROXY_REQUEST;
  stub_call_trace_map_[span_id] = std::move(ti);
}

auto RpcProbeImpl::on_stub_call_return_sent(
    const std::string &trace_id, std::uint64_t span_id,
    std::uint64_t parent_span_id, rpc::TransportPtr transport,
    const std::string &target_service_name,
    const std::string &target_method_name, const std::string &retval_info)
    -> void {
  if (!call_tracer_ptr_) {
    return;
  }
  auto it = stub_call_trace_map_.find(span_id);
  if (it == stub_call_trace_map_.end()) {
    return;
  }
  auto &ti = it->second;
  ti.duration = util::get_os_time_microsecond() - ti.timestamp_ms;
  call_tracer_ptr_->trace(ti);
}

auto RpcProbeImpl::on_proxy_call_return_arrived(
    const std::string &trace_id, std::uint64_t span_id,
    std::uint64_t parent_span_id, rpc::TransportPtr transport,
    const std::string &target_service_name,
    const std::string &target_method_name, const std::string &retval_info,
    std::time_t start_timestamp) -> void {
  if (!call_tracer_ptr_) {
    return;
  }
  auto it = proxy_call_trace_map_.find(span_id);
  if (it == proxy_call_trace_map_.end()) {
    return;
  }
  auto &ti = it->second;
  ti.duration = util::get_os_time_microsecond() - ti.timestamp_ms;
  call_tracer_ptr_->trace(ti);
  proxy_call_trace_map_.erase(it);
}

auto RpcProbeImpl::get_listener_port() -> std::uint16_t {
  for (const auto &listener : config_->get_listener_list()) {
    std::vector<std::string> result;
    util::split(listener, ":", result);
    if (result.size() != 2) {
      return 0;
    }
    return std::uint16_t(std::stoi(result[1]));
  }
  return 0;
}

} // namespace service
} // namespace kratos
