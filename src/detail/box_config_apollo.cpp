﻿#include "box_config_apollo.hh"
#include "util/os_util.hh"
#include "util/string_util.hh"
#include "json/json.h"

#include <fstream>
#include <string>
#include <vector>

kratos::config::BoxConfigApollo::BoxConfigApollo(
    kratos::service::ServiceBox *box)
    : BoxConfigImpl(box) {
  // 本地配置文件名为: {pid}-apollo.cfg, 建立在当前工作目录
  local_file_path_ = std::to_string(util::get_pid()) + "-apollo.cfg";
}

kratos::config::BoxConfigApollo::~BoxConfigApollo() {}

auto kratos::config::BoxConfigApollo::load(const std::string &config_file_path,
                                           std::string &error) -> bool {
  if (!download(config_file_path, error)) {
    return false;
  }
  return BoxConfigImpl::load(local_file_path_, error);
}

auto kratos::config::BoxConfigApollo::reload(
    const std::string &config_file_path, std::string &error) -> bool {
  if (!download(config_file_path, error)) {
    return false;
  }
  return BoxConfigImpl::reload(local_file_path_, error);
}

auto kratos::config::BoxConfigApollo::download(
    const std::string &config_file_path, std::string &error) -> bool {
  //
  // 命令行参数格式:
  //
  // 1. url=xxx;token=xxx
  // 2. baseurl=xxx;env=xxx;appid=xxx;cluster=xxx;namespace=xxx;token=xxx
  //
  // url
  // "http://{baseurl}/openapi/v1/envs/{env}/apps/{appid}/clusters/{cluster}/namespaces/{namespace}/releases/latest"
  // token   "Authorization:%s"
  // content "Content-Type:application/json;charset=UTF-8"
  //

  return util::apollo_download(config_file_path, error, local_file_path_);
}
