#pragma once

#include "call_tracer/call_tracer_factory.hh"

namespace kratos {
namespace service {

/**
 * CallTracer工厂实现类.
 */
class CallTracerFactoryImpl : public CallTracerFactory {
public:
  CallTracerFactoryImpl();
  virtual ~CallTracerFactoryImpl();
  virtual auto create_from(CallTracerStoreType store_type,
                           kratos::config::BoxConfig &config)
      -> CallTracerPtr override;
  virtual auto create_from(kratos::config::BoxConfig &config)
      -> CallTracerPtr override;
};

} // namespace service
} // namespace kratos
