﻿#include "co_manager_impl.h"
#include "detail/service_context_impl.hh"

namespace kratos {
namespace service {

CoManagerImpl::CoManagerImpl(ServiceContext *ctx) {
  ctx_ = dynamic_cast<ServiceContextImpl *>(ctx);
}
CoManagerImpl::~CoManagerImpl() {
  for (const auto &co_id : co_id_set_) {
    ctx_->close(co_id);
  }
}
auto CoManagerImpl::spawn(std::function<void(void)> co_func)
    -> coroutine::CoroID {
  auto co_id = ctx_->schedule_internal(co_func, this);
  if (co_id == coroutine::INVALID_CORO_ID) {
    return co_id;
  }
  co_id_set_.emplace(co_id);
  return co_id;
}
auto CoManagerImpl::close(coroutine::CoroID co_id) -> void {
  co_id_set_.erase(co_id);
  ctx_->close(co_id);
}

} // namespace service
} // namespace kratos
