﻿#include "tcp_loop.hh"

#include "box/box_network.hh"
#include "box/box_network_event.hh"
#include "detail/box_alloc.hh"
#include "util/box_debug.hh"

#include "knet/knet.h"
#include "util/box_std_allocator.hh"
#include "util/object_pool.hh"
#include "util/os_util.hh"

#include <cstring>

using namespace kratos::service;

// 每个工作线程一个独立的TcpLoop指针
thread_local kratos::loop::TcpLoop *local_tcp_loop_ = nullptr;

// 输出系统错误日志
inline static auto print_sys_error(kratos::loop::TcpLoop *loop) -> void {
  if (loop->get_network()) {
    auto *appender = loop->get_network()->get_logger_appender();
    if (appender) {
      appender->write(klogger::Logger::WARNING, "[TcpLoop]%s",
                      kratos::util::get_last_errno_message().c_str());
    }
  }
}

/**
 * 网络底层内存分配.
 *
 * \param size 字节数
 * \return 内存地址
 */
static void *knet_malloc_func(size_t size) {
  if (!size) {
    return nullptr;
  }
  return box_malloc(size);
}
/**
 * 网络底层内存释放.
 *
 * \param p 内存地址
 */
static void knet_free_func(void *p) {
  if (p) {
    box_free(p);
  }
}

kratos::loop::TcpLoop::TcpLoop(service::BoxNetwork *network) {
  network_ = network;
}

kratos::loop::TcpLoop::~TcpLoop() {}

auto kratos::loop::TcpLoop::start() -> bool {
  if (loop_) {
    return true;
  }
  // 替换网络库的内存分配器
  knet_set_malloc_func(knet_malloc_func);
  knet_set_free_func(knet_free_func);
  // 建立TCP网络循环
  loop_ = knet_loop_create();
  if (!loop_) {
    return false;
  }
  return true;
}

auto kratos::loop::TcpLoop::stop() -> bool {
  // 减少所有现存管道引用计数
  for (const auto &it : thread_channel_map_) {
    knet_channel_ref_leave(it.second);
  }
  thread_channel_map_.clear();
  // 销毁网络循环
  if (loop_) {
    knet_loop_destroy(loop_);
    loop_ = nullptr;
  }
  return true;
}

auto kratos::loop::TcpLoop::worker_update(
    kratos::service::BoxNetwork *network_ptr) -> void {
  if (!local_tcp_loop_) {
    local_tcp_loop_ = this;
  }
  // 在网络线程内运行一次主循环
  if (loop_) {
    knet_loop_run_once(loop_);
  }
}

// 网络线程内处理管道关闭事件
void do_channel_close(kchannel_ref_t *channel) {
  auto uuid = knet_channel_ref_get_uuid(channel);
  auto it = local_tcp_loop_->get_worker_channel_map().find(uuid);
  if (it != local_tcp_loop_->get_worker_channel_map().end()) {
    // 在管道查询表内找到，释放资源
    knet_channel_ref_leave(it->second);
    local_tcp_loop_->get_worker_channel_map().erase(it);
  }
  // 发送到逻辑线程
  NetEventData event_data{};
  event_data.event_id = NetEvent::close_notify;
  event_data.close_notify.channel_id = uuid;
  if (!local_tcp_loop_->get_network()->get_net_queue()->send(event_data)) {
    // 发送失败，清理资源
    event_data.clear();
  }
}

// 网络线程内处理管道读事件
void do_channel_recv(kchannel_ref_t *channel) {
  NetEventData event_data{};

  auto uuid = knet_channel_ref_get_uuid(channel);
  auto *stream = knet_channel_ref_get_stream(channel);

  int size = knet_stream_available(stream);
  if (!size) {
    // 管道内没有数据却触发了读事件，关闭这个管道
    knet_channel_ref_close(channel);
    return;
  }
  event_data.event_id = NetEvent::recv_data_notify;
  event_data.recv_data_notify.channel_id = uuid;
  event_data.recv_data_notify.data_ptr = box_malloc(size);

  event_data.recv_data_notify.length = size;
  if (error_ok !=
      knet_stream_pop(stream, event_data.recv_data_notify.data_ptr, size)) {
    // 从管道内取数据失败，清理资源
    event_data.clear();
    knet_channel_ref_close(channel);
    return;
  }
  // 发送到逻辑线程
  if (!local_tcp_loop_->get_network()->get_net_queue()->send(event_data)) {
    event_data.clear();
    // 发送失败，清理资源
    knet_channel_ref_close(channel);
  }
}

// 网络线程内，由监听器创建的管道，事件处理函数
void client_cb(kchannel_ref_t *channel, knet_channel_cb_event_e e) {
  if (e & channel_cb_event_recv) {
    do_channel_recv(channel);
  } else if (e & channel_cb_event_close) {
    do_channel_close(channel);
  }
}

// 网络线程内，监听器接受了一个连接请求
void do_accept_channel(kchannel_ref_t *channel) {
  auto uuid = knet_channel_ref_get_uuid(channel);
  auto channel_shared = knet_channel_ref_share(channel);

  local_tcp_loop_->get_worker_channel_map()[uuid] = channel_shared;
  knet_channel_ref_set_cb(channel, client_cb);

  // 发送到逻辑线程
  NetEventData event_data{};
  event_data.event_id = NetEvent::accept_notify;
  event_data.accept_notify.channel_id = uuid;
  event_data.accept_notify.local_ip = nullptr;
  event_data.accept_notify.local_port = 0;
  event_data.accept_notify.peer_ip = nullptr;
  event_data.accept_notify.peer_port = 0;
  auto *uuid_ptr = knet_channel_ref_get_ptr(channel);

  box_assert(local_tcp_loop_->get_network(), !uuid_ptr);

  if (uuid_ptr) {
    auto listen_channel_uuid = *reinterpret_cast<std::uint64_t *>(uuid_ptr);    
    auto &listener_map = local_tcp_loop_->get_network()->get_listener_name_map();
    auto it = listener_map.find(listen_channel_uuid);
    if (it != listener_map.end()) {
      const auto &listener_name = it->second.second;
      event_data.accept_notify.name = box_malloc(listener_name.size() + 1);
      box_assert(local_tcp_loop_->get_network(), !event_data.accept_notify.name);

      if (listener_name.empty()) {
        event_data.accept_notify.name[0] = '\0';
      } else {
        memcpy(event_data.accept_notify.name, listener_name.c_str(),
               listener_name.size() + 1);
      }

      auto *local_addr = knet_channel_ref_get_local_address(channel);
      auto *peer_addr = knet_channel_ref_get_peer_address(channel);
      if (local_addr && address_get_ip(local_addr)) {
        event_data.accept_notify.local_ip =
            box_malloc(std::strlen(address_get_ip(local_addr)) + 1);
        box_assert(local_tcp_loop_->get_network(), !event_data.accept_notify.local_ip);
        memcpy(event_data.accept_notify.local_ip, address_get_ip(local_addr),
               std::strlen(address_get_ip(local_addr)) + 1);
        event_data.accept_notify.local_port = address_get_port(local_addr);
      }
      if (peer_addr && address_get_ip(peer_addr)) {
        event_data.accept_notify.peer_ip =
            box_malloc(std::strlen(address_get_ip(peer_addr)) + 1);
        box_assert(local_tcp_loop_->get_network(), !event_data.accept_notify.peer_ip);
        memcpy(event_data.accept_notify.peer_ip, address_get_ip(peer_addr),
               std::strlen(address_get_ip(peer_addr)) + 1);
        event_data.accept_notify.peer_port = address_get_port(peer_addr);
      }
    }
  }
  if (!uuid_ptr || !local_tcp_loop_->get_network()->get_net_queue()->send(event_data)) {
    // 内部错误或发送失败, 销毁资源
    knet_channel_ref_leave(channel_shared);
    local_tcp_loop_->get_worker_channel_map().erase(uuid);
    event_data.clear();
  }
}

// 网络线程内，监听器事件处理函数
void acceptor_cb(kchannel_ref_t *channel, knet_channel_cb_event_e e) {
  if (e & channel_cb_event_accept) {
    do_accept_channel(channel);
  }
}

// 网络线程内，处理建立监听器事件
void do_listen_request(const NetEventData &request, kloop_t *loop,
                       kratos::loop::TcpLoop *tcp_loop) {
  auto *channel = knet_loop_create_channel(
      loop,
      0, // 已废弃
      tcp_loop->get_network()->get_channel_recv_buffer_len());
  box_assert(tcp_loop->get_network(), !channel);
  // 检查是否设置SO_REUSEPORT
  if (request.listen_request.is_reuse_port) {
    knet_channel_ref_set_reuseport(channel);
  }
  auto uuid = knet_channel_ref_get_uuid(channel);
  box_assert(tcp_loop->get_network(), !uuid);

  auto retval = knet_channel_ref_accept(channel, request.listen_request.host,
                                        request.listen_request.port,
                                        512 // TODO configurable
  );

  box_assert(tcp_loop->get_network(), !request.listen_request.name);
  std::string listener_name(request.listen_request.name);

  NetEventData response{};
  auto length = static_cast<int>(std::strlen(request.listen_request.name) + 1);
  response.listen_response.name = box_malloc(length);
  box_assert(tcp_loop->get_network(), !response.listen_response.name);

  memcpy(response.listen_response.name, request.listen_request.name, length);
  response.event_id = NetEvent::listen_response;
  if (error_ok != retval) {
    response.listen_response.channel_id = 0;
    response.listen_response.success = false;
    print_sys_error(tcp_loop);
    knet_channel_ref_close(channel);
  } else {
    auto *shared_channel = knet_channel_ref_share(channel);
    box_assert(tcp_loop->get_network(), !shared_channel);

    tcp_loop->get_worker_channel_map()[uuid] = shared_channel;
    response.listen_response.channel_id = uuid;
    response.listen_response.success = true;
    knet_channel_ref_set_cb(channel, acceptor_cb);
  }
  if (!tcp_loop->get_network()->get_net_queue()->send(response)) {
    // 发送失败，清理资源
    // 发生这种情况，会导致逻辑线程无法得知监听器是否启动成功
    response.clear();
    knet_channel_ref_close(channel);
  } else {
    if (error_ok == retval) {
      tcp_loop->get_network()->get_listener_name_map()[uuid] = {uuid,
                                                                listener_name};
      auto *uuid_ptr =
          &(tcp_loop->get_network()->get_listener_name_map()[uuid].first);
      knet_channel_ref_set_ptr(channel, reinterpret_cast<void *>(uuid_ptr));
    }
  }
}

// 网络线程内，处理连接事件（成功或失败)
void do_connect_channel(kchannel_ref_t *channel, bool success) {
  auto uuid = knet_channel_ref_get_uuid(channel);
  kchannel_ref_t *channel_shared = nullptr;
  bool update_success = true;
  if (success) {
    channel_shared = knet_channel_ref_share(channel);
    box_assert(local_tcp_loop_->get_network(), !channel_shared);
    update_success = local_tcp_loop_->add_worker_channel(uuid, channel_shared);
    box_assert(local_tcp_loop_->get_network(), !update_success);
  }
  NetEventData event_data{};
  auto *name_ptr = reinterpret_cast<char *>(knet_channel_ref_get_ptr(channel));

  box_assert(local_tcp_loop_->get_network(), !name_ptr);

  auto length = std::strlen(name_ptr) + 1;
  event_data.event_id = NetEvent::connect_response;
  event_data.connect_response.name = box_malloc(length);
  memcpy(event_data.connect_response.name, name_ptr, length);
  event_data.connect_response.channel_id = uuid;
  event_data.connect_response.success = (success && update_success);
  event_data.connect_response.local_ip = nullptr;
  event_data.connect_response.local_port = 0;
  event_data.connect_response.peer_ip = nullptr;
  event_data.connect_response.peer_port = 0;

  auto *local_addr = knet_channel_ref_get_local_address(channel);
  auto *peer_addr = knet_channel_ref_get_peer_address(channel);
  if (local_addr && address_get_ip(local_addr)) {
    event_data.connect_response.local_ip =
        box_malloc(std::strlen(address_get_ip(local_addr)) + 1);
    box_assert(local_tcp_loop_->get_network(), !event_data.connect_response.local_ip);
    memcpy(event_data.connect_response.local_ip, address_get_ip(local_addr),
           std::strlen(address_get_ip(local_addr)) + 1);
    event_data.connect_response.local_port = address_get_port(local_addr);
  }
  if (peer_addr && address_get_ip(peer_addr)) {
    event_data.connect_response.peer_ip =
        box_malloc(std::strlen(address_get_ip(peer_addr)) + 1);
    box_assert(local_tcp_loop_->get_network(), !event_data.connect_response.peer_ip);
    memcpy(event_data.connect_response.peer_ip, address_get_ip(peer_addr),
           std::strlen(address_get_ip(peer_addr)) + 1);
    event_data.connect_response.peer_port = address_get_port(peer_addr);
  }

  if (!local_tcp_loop_->get_network()->get_net_queue()->send(event_data)) {
    // 发送失败清理资源
    event_data.clear();
    if (channel_shared) {
      knet_channel_ref_leave(channel_shared);
    }
    local_tcp_loop_->get_worker_channel_map().erase(uuid);
  }
  if (!success) {
    knet_channel_ref_close(channel);
  }
}

void do_connector_channel_close(kchannel_ref_t *channel) {
  auto *name_ptr = reinterpret_cast<char *>(knet_channel_ref_get_ptr(channel));
  if (name_ptr) {
    box_free(name_ptr);
    knet_channel_ref_set_ptr(channel, nullptr);
  }
  do_channel_close(channel);
}

// 网络线程内，连接器事件处理
void connector_cb(kchannel_ref_t *channel, knet_channel_cb_event_e e) {
  if (e & channel_cb_event_connect) {
    do_connect_channel(channel, true);
  } else if (e & channel_cb_event_recv) {
    do_channel_recv(channel);
  } else if (e & channel_cb_event_connect_timeout) {
    do_connect_channel(channel, false);
  } else if (e & channel_cb_event_close) {
    do_connector_channel_close(channel);
  }
}

// 网络线程内，处理建立连接器事件
void do_connect_request(const NetEventData &request, kloop_t *loop,
                        kratos::loop::TcpLoop *tcp_loop) {
  box_assert(tcp_loop->get_network(), !request.connect_request.host);
  box_assert(tcp_loop->get_network(), !request.connect_request.name);

  auto channel = knet_loop_create_channel(
      loop,
      0, // 已废弃
      tcp_loop->get_network()->get_channel_recv_buffer_len());
  box_assert(tcp_loop->get_network(), !channel);

  auto retval = knet_channel_ref_connect(channel, request.connect_request.host,
                                         request.connect_request.port,
                                         request.connect_request.timeout);
  if (error_ok != retval) {
    NetEventData response{};
    response.event_id = NetEvent::connect_response;
    auto length = std::strlen(request.connect_request.name) + 1;
    response.connect_response.name = box_malloc(length);
    box_assert(tcp_loop->get_network(), !request.connect_response.name);

    memset(response.connect_response.name, 0, length);
    memcpy(response.connect_response.name, request.connect_request.name,
           length);
    response.connect_response.channel_id = 0;
    response.connect_response.success = false;
    if (!tcp_loop->get_network()->get_net_queue()->send(response)) {
      // 发送失败，清理资源
      response.clear();
    }
    print_sys_error(tcp_loop);
    knet_channel_ref_close(channel);
  } else {
    knet_channel_ref_set_cb(channel, connector_cb);
    auto length = std::strlen(request.connect_request.name) + 1;
    auto *name = box_malloc(length);
    box_assert(tcp_loop->get_network(), !name);

    if (name) {
      memset(name, 0, length);
      memcpy(name, request.connect_request.name, length);
      knet_channel_ref_set_ptr(channel, name);
    } else {
      knet_channel_ref_close(channel);
    }
  }
}

// 网络线程内，处理发送数据事件
void do_send_request(const NetEventData &request, kloop_t * /*loop*/,
                     kratos::loop::TcpLoop *tcp_loop) {
  box_assert(tcp_loop->get_network(), !request.send_data_notify.data_ptr);

  auto &worker_channel_map = tcp_loop->get_worker_channel_map();
  auto it = worker_channel_map.find(request.send_data_notify.channel_id);
  if (it == worker_channel_map.end()) {
    return;
  }
  auto *channel_ref = it->second;
  auto *stream = knet_channel_ref_get_stream(channel_ref);
  box_assert(tcp_loop->get_network(), !stream);

  auto retval = knet_stream_push(stream, request.send_data_notify.data_ptr,
                                 request.send_data_notify.length);

  if (error_ok != retval) {
    print_sys_error(tcp_loop);
    knet_channel_ref_close(channel_ref);
  }
}

// 网络线程内，处理管道关闭事件
void do_close_request(const NetEventData &request, kloop_t * /*loop*/,
                      kratos::loop::TcpLoop *tcp_loop) {
  auto uuid = request.close_request.channel_id;
  auto it = tcp_loop->get_worker_channel_map().find(uuid);
  if (it != tcp_loop->get_worker_channel_map().end()) {
    // 如果有用户自定义数据，这里需要清理
    auto* user_ptr =reinterpret_cast<char *>(knet_channel_ref_get_ptr(it->second));
    if (user_ptr){
      box_free(user_ptr);
      knet_channel_ref_set_ptr(it->second, nullptr);
    }
    knet_channel_ref_close(it->second);
  }
}

auto kratos::loop::TcpLoop::worker_cleanup() -> void {
  local_tcp_loop_ = nullptr;
}

auto kratos::loop::TcpLoop::do_worker_event(
    const service::NetEventData &event_data) -> void {
  if (!loop_) {
    return;
  }
  switch (event_data.event_id) {
  case NetEvent::listen_request: {
    do_listen_request(event_data, loop_, this);
  } break;
  case NetEvent::connect_request: {
    do_connect_request(event_data, loop_, this);
  } break;
  case NetEvent::send_data_notify: {
    do_send_request(event_data, loop_, this);
  } break;
  case NetEvent::close_request: {
    do_close_request(event_data, loop_, this);
  } break;
  default:
    break;
  }
}

auto kratos::loop::TcpLoop::get_network() -> service::BoxNetwork * {
  return network_;
}

kratos::loop::TcpLoop::ChannelMap &
kratos::loop::TcpLoop::get_worker_channel_map() noexcept {
  return thread_channel_map_;
}

auto kratos::loop::TcpLoop::add_worker_channel(std::uint64_t channel_id,
                                               kchannel_ref_t *channel_ref)
    -> bool {
  auto it = thread_channel_map_.emplace(channel_id, channel_ref);
  if (!it.second) {
    return false;
  }
  return true;
}
