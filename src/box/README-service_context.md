# kratos::service::ServiceContext

对service box提供功能的封装，使得用户服务可以通过ServiceContext接口便利的使用service box提供的功能支持。

`ServiceContext`接口通过`getContext()`方法获取，此方法只能在服务实现类内部调用.

[接口C++文件](service_context.hh)

# API

## register_service

```
auto register_service(const std::string &name) -> bool
```
向集群注册一个服务，名字为```name```, 注册成功后集群内其他服务就可以使用获取服务的方法来获取```name```的代理，并通过代理来直接调用方法，调用过程类似调用一个本地方法(协程版本), 或者是异步的方式进行调用。

## unregister_service
```
auto unregister_service(const std::string &name) -> bool
```
从集群内取消服务名为```name```的服务注册, 调用完成后本服务对集群内其他服务将不可视，已经获取的服务仍然可以通过代理与本服务通信

## get_argument
```
auto get_argument() const -> const argument::BoxArgument &
```
获取service box启动参数, 参数具体细节请参见[service box参数](../argument/box_argument.hh)

## write_log_line
**线程安全**

```
auto write_log_line(int log_level, const std::string &log_line) -> void
```
向service box日志系统写入日志

## get_config
```
auto get_config() const -> kratos::config::BoxConfig &
```
获取service box配置接口, 参数具体细节请参见[service box配置](../config/box_config.hh)。


## shutdown
```
auto shutdown() -> void
```
关闭service box

## sleep_co

```
auto sleep_co(std::time_t ms) -> void
```
在协程模式下让当前协程睡眠ms毫秒，如果非协程模式运行则立即返回

## try_get_proxy
```
template <typename T> auto try_get_proxy(const std::string &service_name) -> std::shared_ptr<T>
```
尝试获取名字为```name```类型为T的代理类实例，方法会在缓存内查找代理实例，如果没有找到则返回一个empty的共享指针，否则返回找到的实例。
## get_proxy_sync
```
template <typename T> auto get_proxy_sync(const std::string &service_name, std::time_t timeout) -> std::shared_ptr<T>
```
获取名字为```name```类型为T的代理类实例，方法会在缓存内查找代理实例，如果没有找到则等待timeout毫秒，在等待期间如果满足条件则尝试建立代理，如果在timeout时间内未满足条件或建立未成功则返回找到的实例。

## get_proxy_co
```
template <typename T> auto get_proxy_co(const std::string &service_name, std::time_t timeout) -> std::shared_ptr<T>
```
当协程模式下，将等待timeout毫秒，功能与get_proxy_sync类似，但get_proxy_sync会阻塞主线程，而get_proxy_co只会阻塞当前协程。

## get_proxy_from_peer
```
template <typename T> auto get_proxy_from_peer(rpc::StubCallPtr stub_call) -> std::shared_ptr<T>
```
用于通过service box proxy连接进来的客户端，当客户端有调用通过service box proxy转发到集群内服务时，通过get_proxy_from_peer可以获取对端的服务代理实例，本方法假设对端一定存在此服务，如果不存在也会成功，但后续调用会**失败**，方法立即返回。

## verbose, info, diagnose, warn, except, fail, fatal
```
auto verbose(const std::string &log) -> void
auto info(const std::string &log) -> void
auto diagnose(const std::string &log) -> void
auto warn(const std::string &log) -> void
auto except(const std::string &log) -> void
auto fail(const std::string &log) -> void
auto fatal(const std::string &log) -> void
```
以不同的日志级别，将```log```写入到service box日志系统，最低级别为verbose，最高级别为fatal

## new_scheduler

```
auto new_scheduler() -> SchedulePtr
```
获取一个定时器管理器接口，接口方法请参见[定时器接口方法](../scheduler/scheduler.hh)

## new_http
```
auto new_http() -> HttpPtr
```
获取一个HTTP管理器接口，接口方法请参见[HTTP接口方法](../http/http_base.hh), [HTTP调用接口方法](../http/http_call.hh)

## get_allocator

```
auto get_allocator() -> MemoryAllocator &
```
获取一个内存分配器，接口方法参见[内存分配器接口说明](../box/README-memory_allocator.md)，[内存分配器接口方法](../box/memory_allocator.hh)

## new_command
```
auto new_command() -> CommandPtr
```
获取一个命令处理器接口，接口方法请参见[命令处理器接口方法](../command/command.hh) 

## new_redis
```
auto new_redis() -> RedisPtr
```
获取一个Redis管理器接口，接口方法请参见[Redis管理器接口方法](../redis/redis.hh)

## get_module_logger
```
auto get_module_logger(const std::string &name) -> LoggerPtr
```
获取一个模块日志接口，模块日志接口请参见[模块日志接口方法](../box/service_logger.hh)

## get_local_time
```
auto get_local_time() -> LocalTimePtr
```
获取本地时间系统，时间系统接口请参见[时间系统接口方法](../time/local_system_time.hh)

## new_csv_manager
```
auto new_csv_manager() -> CsvManagerPtr
```
获取CSV配置管理器，CSV配置管理器接口请参见[CSV配置管理器接口方法](../csv/csv_reader.hh)

## new_box_console
```
auto new_box_console(const std::string &name) -> BoxConsolePtr
```
获取Web控制台接口，Web控制台接口请参见[Web控制台接口方法](../console)

## new_util
```
auto new_util() -> UtilPtr;
```
获取工具库接口，工具库接口请参见[工具库接口方法](../util/util.hh)