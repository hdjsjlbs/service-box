﻿#pragma once

#include "root/rpc_root.h"
#include "util/box_std_allocator.hh"
#include "util/object_pool.hh"
#include "util/timer_wheel.hh"
#include <memory>
#include <unordered_map>

namespace kratos {
namespace service {

class ServiceBox;

/**
 * 定时器实现类.
 */
class TimerImpl : public TimerBase {
  unique_pool_ptr<util::TimerWheel> timer_wheel_{nullptr}; ///< 时间轮
  /**
   * 定时器信息.
   */
  struct TimerInfo {
    TimerFunc func;             ///< 定时器函数
    std::uint64_t user_data{0}; ///< 用户数据
    util::TimerID wheel_handle; ///< 定时器句柄
    bool cancelled{ false };    ///< 是否被取消
  };
  using TimerInfoMap = PoolUnorederedMap<std::uint64_t, TimerInfo>;
  TimerInfoMap timer_info_map_;  ///< 定时器表
  std::uint64_t internal_id_{1}; ///< 内部ID，填入定时器的用户数据
  ServiceBox *box_{nullptr};     ///< box

public:
  /// ctor
  TimerImpl(ServiceBox *box);
  /// dtor
  virtual ~TimerImpl();
  /// Start a timer
  /// @param timer_func timer callback
  /// @param duration timeout duration
  /// @param user_data user data
  /// @return timer handle
  virtual TimerHandle addTimerOnce(TimerFunc timer_func, std::time_t duration,
                                   std::uint64_t user_data) override;
  /// Start a loop timer, reserved
  /// @param timer_func timer callback
  /// @param duration timeout duration
  /// @param user_data user data
  /// @return timer handle
  virtual TimerHandle addTimer(TimerFunc timer_func, std::time_t duration,
                               std::uint64_t user_data) override;
  /// Cancel a timer, reserved
  virtual void cancelTimer(TimerHandle handle) override;
  /// Returns a expired timer
  /// @param now current timestamp in millionsecond
  virtual TimerHandle runOnce(std::time_t now = 0) override;
  /// Clear all timer
  virtual void clear() override;
  virtual std::uint32_t count() override;
};

/**
 * 定时器工厂实现类.
 */
class TimerFactoryImpl : public TimerFactory {
  ServiceBox *box_{nullptr}; ///< box

public:
  TimerFactoryImpl(ServiceBox *box);
  virtual ~TimerFactoryImpl();
  virtual TimerBase *create() override;
  virtual void destory(TimerBase *timer_base) override;
};

} // namespace service
} // namespace kratos
