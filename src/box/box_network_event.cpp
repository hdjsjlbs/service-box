#include "box_network_event.hh"
#include "box/fixed_mem_pool.hh"
#include "detail/box_alloc.hh"

void kratos::service::ListenRequest::clear() const {
  if (name) {
    box_free(name);
  }
  if (host) {
    box_free(host);
  }
}

void kratos::service::ListenResponse::clear() const {
  if (name) {
    box_free(name);
  }
}

void kratos::service::ConnectRequest::clear() const {
  if (host) {
    box_free(host);
  }
  if (name) {
    box_free(name);
  }
}

void kratos::service::ConnectResponse::clear() const {
  if (name) {
    box_free(name);
  }
  if (local_ip) {
    box_free(local_ip);
  }
  if (peer_ip) {
    box_free(peer_ip);
  }
}

void kratos::service::RecvDataNotify::clear() const {
  if (data_ptr) {
    box_free(data_ptr);
  }
}

void kratos::service::SendDataNotify::clear() const {
  if (data_ptr) {
    box_free(data_ptr);
  }
}

void kratos::service::AcceptNotify::clear() const {
  if (name) {
    box_free(name);
  }
  if (local_ip) {
    box_free(local_ip);
  }
  if (peer_ip) {
    box_free(peer_ip);
  }
}

void kratos::service::NetEventData::clear() const {
  switch (event_id) {
  case NetEvent::listen_request:
    listen_request.clear();
    break;
  case NetEvent::listen_response:
    listen_response.clear();
    break;
  case NetEvent::accept_notify:
    accept_notify.clear();
    break;
  case NetEvent::connect_request:
    connect_request.clear();
    break;
  case NetEvent::connect_response:
    connect_response.clear();
    break;
  case NetEvent::recv_data_notify:
    recv_data_notify.clear();
    break;
  case NetEvent::send_data_notify:
    send_data_notify.clear();
    break;
  case NetEvent::close_notify:
    close_notify.clear();
    break;
  case NetEvent::close_request:
    close_request.clear();
    break;
  default:
    break;
  }
}
