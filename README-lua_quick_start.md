# Quick Start: Lua Demo

> 这篇文章主要介绍 如何基于Lua语言开始 定义、编写以及发布服务。这边会简单的创建一个应用程序，来帮助快速理解。 主要包含：
>
> 1. 定义idl文件。
> 2. 使用repo进行代码生成。
> 4. 服务如何发布。
> 5. 服务如何测试。
>
> 具体lua详细的指南请参考



## 背景

> 这是一个基于游戏场景管理的服务，它可以进行管理简单的角色管理，记录角色的信息，角色进入场景，角色移动和角色位置广播。
> 完整的工程，具体可以参考[LuaDemo的使用文档](README-cpp_demo.md)，里面有简单的，生成与编译，发布与证明的过程。



## 定义你的IDL文件

> service-box有一套自己的接口描述语言，具体可以参考接:[IDL服务定义](README.md) ， 如何编写idl文件。

```
// demodata.idl
// 业务通讯相关的错误码
enum ErrorCode{
    SUCCESS,
    INVALIDSCENE,
    INVALIDPOSITION,
    ROLENOTEXIST
}
// 坐标信息
struct Position {
    float x
    float y
    float z
}
// 玩家的基础信息
struct Role {
    ui64 role_uid
    i32 scene_id
    Position pos
}
// 玩家消息同步所使用的结构体
struct SyncData {
    seq<Role> role_info
}
```

```
// demoscenelua.idl
#import "demodata.idl"

service dynamic Scene generic {
    [script_type:lua]
    void EnterScene(ui64, i32, Position) timeout = 5000
    void Move(ui64, Position) timeout = 5000
}
```

```
// demosynchronize.idl
#import "demodata.idl"
// 同步服务的接口定义文件
service Synchronize reentrant {
    oneway void SyncRoleData(SyncData)
    oneway void EnterScene(ErrorCode, ui64, i32, Position)
    oneway void Move(ErrorCode, ui64, Position)
}
```

服务器的编写需要从idl开始，idl文件当中会有我们需要的接口以及接口通信所使用的结构体。 Demo需要用到以上三个idl文件。这边对于idl文件存放的目录有一个需求，需要存放于src/repo/example目录内:

```
example
├── demodata.idl
├── demoscenelua.idl
└── demosynchronize.idl
```



## 使用repo进行代码生成

> repo是....... 具体相信信息可以查看:[rpc-repo](https://gitee.com/dennis-kk/rpc-repo/blob/master/README.md)

```
cd src/repo												// 进入到repo目录

python init.py											// 进行repo库的初始化
python repo.py -t cpp -i								// 进行cpp依赖库初始化
python repo.py -t lua -i								// 进行lua依赖库初始化

python repo.py -t cpp -a example\demodata.idl			// 添加三个demo相关的idl 为了生成proxy 和 stub
python repo.py -t cpp -a example\demoscenelua.idl		
python repo.py -t cpp -a example\demosynchronize.idl

python repo.py -t lua -a example\demoscenelua.idl		// 需要生成lua相关的脚本

python repo.py -t cpp -b example\demodata.idl			// 生成工程并且编译服务
python repo.py -t lua -b example\demodata.idl
```



## 服务如何发布

> 在服务发布之前，根据ServiceBoxd的架构，需要编译c++版本的proxy和stub服务，这里可以可以参考[Quick Start: C++ Demo](README-cpp_quick_start.md#如果使用生成代码编译服务).
>
> 服务的发布是将我们需要的服务打包进service-box这个载体当中，最后通过配置去进行运行。

```
service-box/
└── publish
    ├── bin
    │   ├── config.default.cfg				// 启动服务器的基础配置
    │   ├── pub.py							// 发布的脚本
    │   └── root.html						// console的基础页面
    └── config
        ├── box.cfg					
        └── box.json						// 发布的配置
```

这边需要按照编写的服务，来定制`box.json`，打开`box.json` 并且修改里面的内容为：

```
{
	"binary_name" : "box",
	"bundle" : 
	[
		{
			"name" : "demoscenelua",
			"service" : "Scene"
		}
	],
	"proxy" : 
	[
		{
			"name": "demoscenelua",
			"proxy": "Scene"
		},
		{
			"name": "demosynchronize",
			"proxy": "Synchronize"
		}
	],
	"stage" : "alpha",
	"version" : "1.0.10"
}
```

进入到bin目录执行：这一步会自动进行编译与启动配置的生成。

```
cd ../bin								// 进入到发布脚本目录
python pub.py -c ../config/box.json		// 按照box.json的配置，进行发布

publish/
└── bin
    └── box								// box的工作目录， 如果是windos下面这里还会有一层目录Debug和Release
        ├── box							// 二进制
        ├── box.cfg						// 启动需要的配置
        └── root.html
```

最后，使用`tools`目录下面的脚本来启动服务：

```
cd ../../tools
Windos:
1.start start_win_local_env.bat
2.start start_win_box.bat
Linux:
bash start_linux_local_env.sh
bash start_linux_box.sh
```

当服务器正常启动之后，在box的工作目录下面会产生相对应的日志，日志格式`box_yyyymmdd.log.0`



## 服务如何测试

> repo当中提供了快速生成测试用mock的功能。详细关于mock的使用原理请参考：[mock](README-mock.md)

