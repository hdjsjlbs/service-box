#!/bin/bash

#MIT License
#
#Copyright (c) 2020 cloudguan rcloudguan@163.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

OSNAME="Unknown"
ISN="Unknown"

function getLinuxDistribution() {
    #最简单的判断，是否有os-release文件
    if type lsb_release > /dev/null 2>&1; then
        #是否有lsb-release文件
        OSNAME=$(lsb_release -si)
        ISN=apt-get
    elif [ -f /etc/os-release ]; then
	    . /etc/os-release
	    OSNAME=$NAME
	    ISN=apt-get
    elif [ -f /etc/debian_version ]; then
        #是否是debian
        OSNAME=Deepin
        ISN=apt-get
    elif [ -f /etc/rehat_version ]; then
        #是否是redhat
        OSNAME=RedHat
        ISN=yum
    else
        echo "unkown linux distribution!"
    fi
}

function initRedHat(){
    # if [ -x "$(command -v java)" ]; then
    #     # $ISN install java -y 
	    
    # fi 
    $ISN install redis -y
}

function initDebain(){
    $ISN update
    # if [ ! -x "$(command -v java)" ]; then
    #     $ISN install default-jdk -y
    # fi 
    $ISN install redis-server -y
	chown redis:redis /var/log/redis/redis-server.log
}

#检查操作系统版本 如果是Unknown 就直接报错
getLinuxDistribution


if [[ $OSNAME == "Debian GNU/Linux" ]];then
    OSNAME="Debain"
    echo "osname set to debain"
fi


if [ $OSNAME = "Unknown" ]; then
    echo "unkwon linux version!!"
    exit 1
fi


if [ ! -d /data ]; then
    echo "data 目录不存在"
    mkdir /data
    chmod 777 data
fi

if [ ! -d /data/thirdparty ]
then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
    chmod 777 /data/thirdparty 
fi

$ISN install -y wget bzip2 dos2unix nginx
#call platform init function 
init$OSNAME

if [ $? -gt 0 ]; then
    echo "init environment error while executing ${ISN} install wget  bzip2 dos2unix"
    exit 1
fi

$ISN install -y git


#如果文件存在但是没有生效的话 需要再这里source 一下 把环境刷新到这个bash里面
if [ -f /etc/profile.d/gcc.sh ]; then
    source /etc/profile.d/gcc.sh
fi 

bash ./initgcc.sh
if [ -f /etc/profile.d/gcc.sh ]; then
    source /etc/profile.d/gcc.sh
fi


if [ $? -gt 0 ]; then
    echo "install gcc init error"
    exit 1
fi 

