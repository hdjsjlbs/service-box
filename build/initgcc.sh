#!/bin/bash

#MIT License
#
#Copyright (c) 2021 cloudguan rcloudguan@163.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.


OSNAME="Unknown"
ISN="Unknown"

function getLinuxDistribution() {
    #采用最简单和愚蠢的做法，不搞花活
    #最简单的判断，是否有os-release文件
    if type lsb_release > /dev/null 2>&1; then
        #是否有lsb-release文件
        OSNAME=$(lsb_release -si)
        ISN=apt-get
    elif [ -f /etc/os-release ]; then
	    . /etc/os-release
	    OSNAME=$NAME
	    ISN=apt-get
    elif [ -f /etc/debian_version ]; then
        #是否是debian
        OSNAME=Deepin
        ISN=apt-get
    elif [ -f /etc/rehat_version ]; then
        #是否是redhat
        OSNAME=RedHat
        ISN=yum
    else
        echo "unkown linux distribution!"
    fi
}


function initRedHat(){
    $ISN install curl-devel -y
    $ISN install zlib-devel -y
}

function initDebain(){
	initDeepin
}

function initDeepin(){ 
    $ISN install libcurl4-openssl-dev -y
    $ISN install build-essential -y
    $ISN install manpages-dev -y 
}

#前置环境安装，编译需要旧环境
function initenv() {
    echo ===========start install gcc=================
    #安装跨平台依赖
    init$OSNAME
    $ISN install automake autoconf libtool make -y
    $ISN install diffutils -y
    $ISN install bzip2 -y
    $ISN install -y git

    if [ ! -x "$(command -v gcc)" ]; then
        $ISN install -y gcc
        $ISN install -y gcc-c++
    fi
}

function installcmake() {
    wget https://cmake.org/files/v3.10/cmake-3.10.0.tar.gz
    tar zxvf cmake-3.10.0.tar.gz
    cd cmake-3.10.0
    ./bootstrap
    gmake -j8
    make install
}

function checkcmake() {
    CMAKE_VERSION=`cmake --version | sed -n '1p' | awk '{print $3}'`

    #如果版本不符合要求就执行安装程序
    if [ $CMAKE_VERSION \< "3.10.0" ];then
        #保存CMAKE路径
        CMAKE_PATH=`which cmake`
        CMAKE_PATH=${CMAKE_PATH%/cmake}

        #尝试使用apt卸载
        apt-get remove cmake

        #初始化并安装我们自己的版本
        initenv
        installcmake
        if [ $? -gt 0 ];then
            echo "cmake 安装执行失败！！！，请检查相关报错"
        fi

        #如果是我们脚本的默认路径，说明我们会覆盖安装掉对应的camke，直接结束
        if [ ${CMAKE_PATH} = "/usr/local/bin" ];then
            return
        fi 

        #如果文件还存在，说明是用户源码安装的，直接备份并且软连接到我们自己的
        if [ -f ${CMAKE_PATH}/cmake ];then
            echo "开始备份用户的cmake"
            if [ -f ${CMAKE_PATH}/cmake.back ];then
                rm ${CMAKE_PATH}/cmake.back
            fi 
            mv ${CMAKE_PATH}/cmake ${CMAKE_PATH}/cmake.back
            #创建软链
            ln -s /usr/local/bin/cmake  ${CMAKE_PATH}/cmake
        fi 
    else
        echo "cmake已安装，版本满足要求, 当前版本: ${CMAKE_VERSION}"
        echo `cmake --version`
    fi
}

#root 权限检测
function root_need() {
    if [[ $EUID -ne 0 ]]; then
        echo "Error:This script must be run as root!" 1>&2
        exit 1
    fi
}


#下载gcc 9.2 源码
function down_loadgcc() {
    echo "gcc 93 不存在 开始从ftp下载"
    wget https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.gz
    if [ $? -gt 0 ]
    then
        echo "下载失败请手动下载gcc 在执行脚本"
        exit -1
    fi
}

#下载gcc 依赖 
function down_gcc_dep() {
    if [ -f gmp-6.1.0.tar.bz2 ]
    then
        rm -f gmp-6.1.0.tar.bz2
    fi
    wget https://mirrors.sjtug.sjtu.edu.cn/gnu/gmp/gmp-6.1.0.tar.bz2

    if [ -f mpfr-3.1.4.tar.bz2 ]
    then
        rm -f mpfr-3.1.4.tar.bz2
    fi
    wget http://mirror.hust.edu.cn/gnu/mpfr/mpfr-3.1.4.tar.bz2 

    if [ -f mpc-1.0.3.tar.gz ]
    then
        rm -f mpc-1.0.3.tar.gz
    fi 
    wget http://mirror.hust.edu.cn/gnu/mpc/mpc-1.0.3.tar.gz

    if [ -f isl-0.18.tar.bz2 ]
    then
        rm -f isl-0.18.tar.bz2
    fi 

    #前三个是国内的源 不会失败最后一个是外国的源可能会失败所以 判断了一下返回值
    wget https://gcc.gnu.org/pub/gcc/infrastructure/isl-0.18.tar.bz2
    if [ $? -gt 0 ]
    then
        echo "下载 isl-0.18.tar 失败 请手动下载"
        exit 1
    fi 

}


#安装脚本
function install_gcc(){
    echo "开始解压"

    if [ ! -d ./gcc-9.3.0 ]
    then
        tar zxvf gcc-9.3.0.tar.gz
        if [ $? -gt -0 ]
        then
            echo "解压失败 请检查文件是否正确"
            exit 1
        fi 
    fi

    cd gcc-9.3.0

    #下载依赖文件
    #down_gcc_dep
    echo "./contrib/download_prerequisites "
    ./contrib/download_prerequisites --no-verify
    if [ $? -gt 0 ]
    then
        echo "./contrib/download_prerequisites 执行失败"
        exit 1
    fi 

    if [ ! -d ./build ]
    then
        mkdir build 
    fi 

    cd ./build
    echo "开始配置 configure --prefix=/usr/local/gcc --enable-checking=release --disable-multilib --enable-languages=c,c++"
    ../configure --prefix=/usr/local/gcc --enable-checking=release --disable-multilib --enable-languages=c,c++

    if [ $? -gt 0 ]
    then
        echo "./configure --prefix=/usr/local/gcc --enable-checking=release --disable-multilib --enable-languages=c,c++ 执行失败，检查log文件"
        exit 1
    fi 

    make -j4
    make install 
    
    if [ $? -gt 0 ]
    then
        echo "安装失败"
        exit 1
    fi 
    
    # 环境变量path
    echo  "export PATH=/usr/local/gcc/bin:/usr/local/bin:\$PATH" >> /etc/profile.d/gcc.sh
    source /etc/profile.d/gcc.sh

    # 头文件
    ln -sv /usr/local/gcc/include/ /usr/include/gcc

    # 库文件
    echo "/usr/local/gcc/lib64" >> /etc/ld.so.conf.d/gcc.conf
    ldconfig -v
    ldconfig -p |grep gcc

    echo "卸载原版gcc" 
    $ISN remove -y gcc 
}


#检查目录
echo "请使用root用户运行此脚本"
root_need

#检查操作系统版本 如果是Unknown 就直接报错
getLinuxDistribution

if [ $OSNAME = "Unknown" ]; then
    echo "unkwon linux version!!"
    exit 1
fi

#刷新一下环境
if [ -f /etc/profile.d/gcc.sh ];then
    source /etc/profile.d/gcc.sh
fi

# 先检查cmake, 不论是gcc的安装还是cmake都依赖于旧版本gcc
if [ -x "$(command -v cmake)" ]; then
# 有安装, 按照如下流程执行： 1.保存cmake路径 2. 判断cmake版本是否符合要求 3.安装覆盖用户原始安装版本
checkcmake
else
#没有安装直接执行安装脚本
installcmake
fi

#检查gcc 如果安装了就不装
if [ -x "$(command -v gcc)" ]; then
### get version code
MAJOR=$(echo __GNUC__ | gcc -E -xc - | tail -n 1)
MINOR=$(echo __GNUC_MINOR__ | gcc -E -xc - | tail -n 1)
PATCHLEVEL=$(echo __GNUC_PATCHLEVEL__ | gcc -E -xc - | tail -n 1)
if [ $MAJOR -ge 9 ]
then
    echo "gcc 已经安装 合适 规格版本" 
    echo `gcc --version`
    exit 0
fi
fi

initenv



echo "开始下载gcc 编译版本 请保证data 目录下有有分配足够空间"
if [ ! -d /data ]
then
    echo "data 目录不存在"
    mkdir /data
    chmod 777 data
fi

if [ ! -d /data/thirdparty ]
then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
fi

# download gcc from thirparty
cd /data/thirdparty 

if [ ! -f gcc-9.3.0.tar.gz ]
then
    down_loadgcc
fi

echo "开始md5 校验: "
MD5NUM=`md5sum gcc-9.3.0.tar.gz | awk '{print $1}'`

if [  "$MD5NUM" != "9b7e8f6cfad96114e726c752935af58a" ]
then
    echo "md5 不匹配 尝试重新下载"
    rm gcc-9.3.0.tar.gz
    down_loadgcc
fi

#编译安装
install_gcc


#gcc 安装完毕 
echo "gcc 安装完毕请手动执行 source /etc/profile.d/gcc.sh  刷新 gcc 环境 "
if [ -f /usr/bin/cc ];then
    rm -f /usr/bin/cc
fi 
ln -s /usr/local/gcc/bin/gcc /usr/bin/cc
if [ -f /usr/bin/c++ ];then
    rm -f /usr/bin/c++
fi 
ln -s /usr/local/gcc/bin/g++ /usr/bin/c++
