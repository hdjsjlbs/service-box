﻿#include "load_balancer/load_balance.h"
#include <random>

namespace kratos {
namespace loadbalance {

std::int32_t get_random_int32(std::int32_t a, std::int32_t b) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<std::int32_t> dist(a, b);
  return dist(mt);
}

auto LASBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  nodes_.emplace_back(lbnode);
  return true;
}

auto LASBalancer::get_next(const std::string &/*ip*/) -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  bool same_weight = false;
  std::int32_t total_weight = 0;
  std::int32_t first_weight = 0;
  std::int32_t least_active_count = -1;
  std::int32_t least_count = 0;
  std::vector<std::int32_t> least_indexes;
  std::vector<std::int32_t> weights;
  least_indexes.resize(server_count_);
  weights.resize(server_count_);

  for (std::int32_t i = 0; i < server_count_; i++) {
    if (nodes_[i].expired()) {
      continue;
    }
    auto weight = nodes_[i].lock()->get_weight();
    auto active = nodes_[i].lock()->get_active_count();
    weights[i] = weight;
    if (least_active_count == -1 || active < least_active_count) {
      least_active_count = active;
      least_count = 1;
      least_indexes[0] = i;
      total_weight = weight;
      first_weight = weight;
      same_weight = true;
    } else if (active == least_active_count) {
      least_indexes[least_count++] = i;
      total_weight += weight;
      if (same_weight && weight != first_weight) {
        same_weight = false;
      }
    }
  }

  if (least_count == 0) {
    return LoadBalanceNodeWeakPtr();
  }

  if (least_count == 1) {
    return nodes_[least_indexes[0]];
  }

  if (!same_weight && total_weight > 0) {
    std::int32_t offset_weight = get_random_int32(0, total_weight);
    for (std::int32_t i = 0; i < least_count; i++) {
      offset_weight -= weights[i];
      if (offset_weight < 0) {
        return nodes_[least_indexes[i]];
      }
    }
  }
  auto rand_index = get_random_int32(0, least_count - 1);
  return nodes_[least_indexes[rand_index]];
}

auto LASBalancer::clear() -> void {
  nodes_.clear();
  server_count_ = 0;
}

} // namespace loadbalance
} // namespace kratos
