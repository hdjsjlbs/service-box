﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

auto IPHASHSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  nodes_.emplace_back(lbnode);
  return true;
}

auto IPHASHSBalancer::get_next(const std::string &ip)
    -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  std::size_t hash_code = std::hash<std::string>{}(ip);
  auto index = hash_code % server_count_;
  return nodes_[index];
}

auto IPHASHSBalancer::clear() -> void {
  nodes_.clear();
  server_count_ = 0;
  return;
}

} // namespace loadbalance
} // namespace kratos
