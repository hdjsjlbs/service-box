﻿#include "load_balancer/load_balance.h"
#include <atomic>
#include <iostream>
#include <mutex>

namespace kratos {

namespace loadbalance {

auto BalancerFactory::get_balancer_map() -> BalancersMap & {
  static BalancersMap balancers_{};
  return balancers_;
}

auto BalancerFactory::register_balancer(BalancerMod mod, ICreateMethod t)
    -> bool {
  auto &balancers = get_balancer_map();
  auto it = balancers.find(std::underlying_type<BalancerMod>::type(mod));
  if (it == balancers.end()) {
    balancers[std::underlying_type<BalancerMod>::type(mod)] = t;
    return true;
  }
  return false;
}

auto BalancerFactory::create_balancer(BalancerMod mod)
    -> std::unique_ptr<ILoadBalancer> {
  static std::once_flag create_flag;
  std::call_once(create_flag, [] {
    Registe_Balancer(BalancerMod::Ip_Hash_Scheduling, IPHASHSBalancer::creator);
    Registe_Balancer(BalancerMod::Least_Active_Scheduling,
                     LASBalancer::creator);
    Registe_Balancer(BalancerMod::Round_Robin_Scheduling, RRSBalancer::creator);
    Registe_Balancer(BalancerMod::Weighted_Round_Robin_Scheduling,
                     WRRSBalancer::creator);
    Registe_Balancer(BalancerMod::Smooth_Weighted_Round_Robin_Scheduling,
                     SWRRSBalancer::creator);
    Registe_Balancer(
        BalancerMod::Virtual_Node_Smooth_Weighted_Round_Robin_Scheduling,
        VNSWRRSBalancer::creator);
  });
  auto &balancers = get_balancer_map();
  auto it = balancers.find(std::underlying_type<BalancerMod>::type(mod));
  if (it != balancers.end()) {
    return balancers[std::underlying_type<BalancerMod>::type(mod)]();
  }
  return nullptr;
}

} // namespace loadbalance
} // namespace kratos
