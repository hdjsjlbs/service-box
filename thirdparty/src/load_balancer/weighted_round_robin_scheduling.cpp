﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

std::int32_t gcd(std::int32_t a, std::int32_t b) {
  while (b != 0) {
    std::int32_t r = b;
    b = a % b;
    a = r;
  }
  return a;
};

auto WRRSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  nodes_.emplace_back(lbnode);
  std::int32_t weight = lbnode.lock()->get_weight();
  tot_weight_ += weight;
  max_weight_ = std::max(max_weight_, weight);
  gcd_weight_ = gcd_weight_ ? gcd(weight, gcd_weight_) : weight;
  server_count_++;
  return true;
}

auto WRRSBalancer::get_next(const std::string &/*ip*/) -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  if (server_count_ == 1) {
    return nodes_[0];
  }
  while (true) {
    last_select_index_ = (last_select_index_ + 1) % server_count_;
    if (last_select_index_ == 0) {
      current_weight_ = current_weight_ - gcd_weight_;
      if (current_weight_ < 0) {
        current_weight_ = max_weight_;
        if (current_weight_ == 0) {
          return LoadBalanceNodeWeakPtr();
        }
      }
    }
    if (nodes_[last_select_index_].expired()) {
      return LoadBalanceNodeWeakPtr();
    }
    if (nodes_[last_select_index_].lock()->get_weight() >= current_weight_) {
      return nodes_[last_select_index_];
    }
  }
  return LoadBalanceNodeWeakPtr();
}

auto WRRSBalancer::clear() -> void {
  last_select_index_ = -1;
  tot_weight_ = 0;
  max_weight_ = 0;
  current_weight_ = 0;
  gcd_weight_ = 0;
  server_count_ = 0;
  nodes_.clear();
}

auto WRRSBalancer::reset_balancer() -> void {
  last_select_index_ = -1;
  tot_weight_ = 0;
  max_weight_ = 0;
  current_weight_ = 0;
  gcd_weight_ = 0;
  for (auto &it : nodes_) {
    if (it.expired()) {
      continue;
    }
    std::int32_t weight = it.lock()->get_weight();
    tot_weight_ += weight;
    max_weight_ = std::max(max_weight_, weight);
    gcd_weight_ = gcd_weight_ ? gcd(weight, gcd_weight_) : weight;
  }
}

} // namespace loadbalance
} // namespace kratos
