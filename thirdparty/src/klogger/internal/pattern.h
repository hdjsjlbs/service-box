/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PATTERN_H
#define PATTERN_H

#include <list>
#include <string>

/**
 * @brief 日志前缀
 */
class Pattern {
public:
  static const char TRHEAD_ID = 't'; ///< 线程ID
  static const char PROCESS = 'p';   ///< 进程ID
  static const char DELIM = '%';     ///< 分隔符
  static const char YEAR = 'y';      ///< 年
  static const char MONTH = 'm';     ///< 月
  static const char DAY = 'd';       ///< 日
  static const char HOUR = 'H';      ///< 小时
  static const char MINUTE = 'M';    ///< 分
  static const char SECOND = 'S';    ///< 秒
  static const char MILLI = 'U';     ///< 毫秒

public:
  /**
   * 构造函数
   */
  Pattern();

  /**
   * 构造函数
   * @param pattern 前缀
   */
  Pattern(const std::string &pattern);

  /**
   * 析构函数
   */
  ~Pattern();

  /**
   * 解析
   * @param pattern 前缀
   */
  void parse(const std::string &pattern);

  /**
   * 获取前缀
   * @return 前缀
   */
  std::string getPrefix();
  /**
   * 获取时间
   * @return 时间
   */
  std::string getTime();

private:
  bool valid(char p);
  bool hasTime(char p);
  bool hasThread(char p);
  bool hasProcess(char p);

private:
  std::list<std::string> _pattern;
  bool _needTime;
  bool _needThreadId;
  bool _needProcessId;
};

#endif // PATTERN_H
