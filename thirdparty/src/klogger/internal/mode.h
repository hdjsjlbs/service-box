/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MODE_H
#define MODE_H

#include <string>

#include "file_name.h"

/**
 * @brief 日志模式
 */
class Mode {
public:
  static const int HOUR = 1;   ///< 以小时为单位分割日志文件
  static const int DAY = 2;    ///< 以天为单位分割日志文件
  static const int WEEK = 3;   ///< 以星期为单位分割日志文件
  static const int SINGLE = 4; ///< 单个日志文件

public:
  /**
   * 构造函数
   */
  Mode();

  /**
   * 构造函数
   * @param mode 模式(hour/day/week/single)，默认为single
   * @param fileName 文件名
   */
  Mode(const std::string &mode, std::string fileName);

  /**
   * 析构函数
   */
  ~Mode();

  /**
   * 解析模式和文件名
   * @param mode 模式(hour/day/week/single)，默认为single
   * @param fileName 文件名
   */
  void parse(const std::string &mode, std::string fileName);

  /**
   * 是否需要切换日志文件
   * @retval true 是
   * @retval false 否
   */
  bool reach();

  /**
   * 取得当前新日志文件的名字
   * @return 新日志文件的名字
   */
  std::string getFileName();

private:
  /**
   * 计算下次切换日志文件的时间（秒）
   * @return 下次切换日志文件的时间（秒）
   */
  time_t calcDeadline();

private:
  static const time_t HOUR_TIME = 3600;
  static const time_t DAY_TIME = HOUR_TIME * 24;
  static const time_t WEEK_TIME = DAY_TIME * 7;

private:
  int _mode;          ///< 当前模式
  time_t _deadline;   ///< 下次切换日志文件的时间（秒）
  FileName _fileName; ///< 文件名
};

#endif // MODE_H
