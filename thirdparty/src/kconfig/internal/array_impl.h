/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ARRAY_IMPL_H
#define ARRAY_IMPL_H

#include <string>
#include <vector>

#include "attribute_impl.hpp"
#include "config.h"

using namespace kconfig;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
#elif __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverloaded-virtual"
#endif

/**
 * @brief array implementation
 */
class ArrayImpl : public AttributeImpl<ArrayAttribute, Attribute::ARRAY> {
public:
  using Attribute::number;
  using Attribute::string;
  using Attribute::array;
  using Attribute::table;
  using Attribute::boolean;

  ArrayImpl();
  virtual ~ArrayImpl();

  virtual int getSize() override;
  virtual NumberAttribute *number(int index) override;
  virtual StringAttribute *string(int index) override;
  virtual ArrayAttribute *array(int index) override;
  virtual TableAttribute *table(int index) override;
  virtual BoolAttribute *boolean(int index) override;
  virtual Attribute *get(int index) override;
  virtual AttributePtr doOperate(int op, AttributePtr rhs) override;
  virtual AttributePtr get_ptr(int index) override;
  virtual auto to_string() -> std::string override;

  void push(AttributePtr attribute);

private:
  std::vector<AttributePtr> _attributes;
};

#ifdef __clang__
#pragma clang diagnostic pop
#elif __GUNC__
#pragma GCC diagnostic pop
#endif

#endif // ARRAY_IMPL_H
