﻿/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <stdexcept>

/**
 * <pre>
 * <b>日志类型</b>
 *
 * 1. 文件日志
 * 2. 屏幕日志
 * 3. HTTP日志
 *
 * <b>通配符(%)</b>：
 *
 * 1. t  线程ID
 * 2. p  进程ID
 * 3. y  年
 * 4. m  月
 * 5. d  日
 * 6. H  小时
 * 7. M  分
 * 8. S  秒
 * 9. U  毫秒
 *
 * <b>日志属性配置</b>
 *
 * 1. 文件日志
 *    1) file://[日志文件根目录]
 *    2) mode=[hour/day/week/single]
 *    3) file=[日志文件名称通配符]
 *    4) async=[true/false] 是否使用异步日志
 *    5) line=[日志行前缀通配符]
 *    6) flush=[true/false]是否立刻刷新日志到硬盘
 *    7) level=[数字]日志等级1~7级逐渐增大,越大等级越高
 *    8) maxSize = [数字]单个日志文件最大容量(字节)
 *
 * 2. 屏幕日志
 *    1) console://
 *    2) line=[日志行前缀通配符]
 *    3) flush=[true/false]是否立刻刷新日志到屏幕
 *    4) level=[数字]日志等级1~7级逐渐增大,越大等级越高
 *
 * 3. HTTP日志
 *    1) line=[日志行前缀通配符]
 *    2) http://[主机地址/主机域名]
 *    3) port=[主机端口]
 *    4) pool=[HTTP客户端最大数量]
 *    5) uri=[HTTP主机访问路径]
 *    6) method=[POST/PUT/GET] HTTP方法
 *    7) level=[数字]日志等级1~7级逐渐增大,越大等级越高
 *
 * 4. TCP日志
 *    1) line=[日志行前缀通配符]
 *    2) tcp://[远程日志服务IP地址:主机端口]
 *    3) port=[主机端口]
 *    7) level=[数字]日志等级1~7级逐渐增大,越大等级越高
 * </pre>
 */

namespace klogger {

class Logger;
class Appender;

class LoggerException : public std::logic_error {
public:
    explicit LoggerException(const std::string& reason)
    : logic_error(reason.c_str()) { // construct from message string
    }

	explicit LoggerException(const char* reason)
    : logic_error(reason) {	// construct from message string
    }
};

/**
 * @brief logger
 */
class Logger {
public:
    static const int VERBOSE     = 1;       ///< verbose
    static const int INFORMATION = 2;       ///< information
    static const int DIAGNOSE    = 3;       ///< debug for programmer
    static const int WARNING     = 4;       ///< something wrong but can recover
    static const int EXCEPTION   = 5;       ///< exception thrown
    static const int FAILURE     = 6;       ///< system failure
    static const int FATAL       = 7;       ///< fatal error cannot recover
	static const int STAT        = 8;       ///< statistic
    static const int MAX         = STAT;    ///< maximum log level
    static const int MIN         = VERBOSE; ///< minimum log level

public:
    virtual ~Logger() {}

    /**
     * @brief start logger
     */
    virtual void destroy() = 0;

    /**
     * @brief get appender by name
     * @param name appender name
     */
    virtual Appender* getAppender(const std::string& name) = 0;

    /**
     * @brief fork a new appender
     * @param name appender name
     * @param attribute appender attribute string
     */
    virtual Appender* newAppender(const std::string& name, const std::string& attribute) = 0;
    /**
     * @brief remove a appender
     * @param name appender name
     */
    virtual void remove(const std::string& name) = 0;
};

/**
 * @brief log line appender
 */
class Appender {
public:
    virtual ~Appender() {}

    /**
     * @brief write a log message
     * @param level log level
     * @param format log message format
     */
    virtual void write(int level, const char* format, ...) = 0;

    /**
     * set log level
     * @param level log level
     */
    virtual void setLevel(int level) = 0;

    /**
     * get log level
     * @return log level
     */
    virtual int getLevel() = 0;

    /**
     * destroy self
     */
    virtual void destroy() = 0;

    /**
     * reload log attribute
     */
    virtual void reload(const std::string& attribute) = 0;

    /**
     * Is show log level?
     */
    virtual void show_level(bool flag) = 0;

    /**
     * Is show appender name?
     */
    virtual void show_name(bool flag) = 0;

    /**
     * @brief detach from Logger
     * 
     */
    virtual void detach() = 0;
};

}

#endif // LOGGER_H
