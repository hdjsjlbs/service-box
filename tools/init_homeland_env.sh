#!/bin/bash

#安装需要的4个组件
docker pull rabbitmq:3.8.31-management
docker pull zookeeper:3.8.0
docker pull redis:6.2.7
docker pull mysql:5.7.26
docker pull bitnami/kafka:1.1.1

docker rm -f hl_rabbitmq
docker rm -f hl_mysql
docker rm -f hl_zookeeper
docker rm -f hl_redis
docker rm -f hl_kafka

docker run -itd --name hl_rabbitmq -p 5672:5672 -p 15672:15672  --hostname HomelandRabbit -e RABBITMQ_DEFAULT_VHOST=/  -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin rabbitmq:3.8.31-management
docker run -itd --name hl_mysql -p 3306:3306   -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7.26
docker run -itd --name hl_zookeeper -p 2181:2181  --restart always  zookeeper:3.8.0
docker run -itd --name hl_redis -p 6379:6379  redis:6.2.7
docker run -d --name hl_kafka -p 9093:9093 -e KAFKA_BROKER_ID=1 -e KAFKA_CFG_ZOOKEEPER_CONNECT=172.17.0.1:2181 -e KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CLIENT:PLAINTEXT,EXTERNAL:PLAINTEXT -e KAFKA_CFG_ADVERTISED_LISTENERS=CLIENT://kafka:9092,EXTERNAL://172.17.0.1:9093 -e KAFKA_CFG_LISTENERS=CLIENT://:9092,EXTERNAL://:9093 -e KAFKA_CFG_INTER_BROKER_LISTENER_NAME=CLIENT -e ALLOW_PLAINTEXT_LISTENER=yes bitnami/kafka:1.1.1

sleep 10

docker exec hl_mysql bash -c "mysql -uroot -p123456 -e \"grant all privileges on *.* to mog_develop@'%' identified by 'mog_develop' with grant option;  \""
docker exec hl_mysql bash -c "mysql -uroot -p123456 -e \"grant all privileges on *.* to mog_develop@'localhost' identified by 'mog_develop' with grant option;   \""
docker exec hl_mysql bash -c "mysql -uroot -p123456 -e \"flush privileges; \""

sleep 10

docker exec hl_rabbitmq bash -c "rabbitmqctl add_user mog_develop mog_develop"
docker exec hl_rabbitmq bash -c "rabbitmqctl set_user_tags mog_develop administrator"
docker exec hl_rabbitmq bash -c "rabbitmqctl set_permissions -p / mog_develop '.*' '.*' '.*'"
