cd ..\build\service-box-windows-dependency\zookeeper\bin
start /MIN taskkill /F /IM java.exe
cd ..\..\
cd Redis
start /MIN redis-cli.exe -h 127.0.0.1 -p 6379 shutdown & taskkill /F /IM redis-cli.exe
cd ..
cd nginx
start /MIN nginx.exe -s stop
